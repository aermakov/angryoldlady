﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BoilingPoint.Portable;
using BoilingPoint.Portable.Data;
using Newtonsoft.Json.Linq;
using Parse;

namespace BoilingPoint.Console
{
    internal class Program
    {
        private static void Main()
        {
            ParseClient.Initialize("vLM3l6oOTcTNpxtlWcXhXFG6BdItGPbyCMAykM6T",
                                   "rme4lQs27PFu3uTo9dbEqoTtMsvWHt4kur7PNOpQ");

            //CleanupBanks().Wait();
            //SyncBanksAsync().Wait();
            SearchTwitterUsernamesAsync().Wait();
        }

        private static async Task SearchTwitterUsernamesAsync()
        {
            IList<Bank> parseBanks = await ParseHelper.GetBanksAsync();

            string json = File.ReadAllText("banks.json");
            JArray array = JArray.Parse(json);
            foreach (JToken jToken in array)
            {
                var name = jToken.Value<string>("name");
                var account = jToken.Value<string>("account");
                string lowerCaseHashTag = Utils.GetLowerCaseHashtag(name);
                Bank bank = parseBanks.FirstOrDefault(x => x.LowercaseHashtagTitle == lowerCaseHashTag);
                if (bank != null && string.IsNullOrEmpty(bank.TwitterUsername))
                {
                    bank.TwitterUsername = account.Substring(1).ToLowerInvariant().Trim();

                    await ParseHelper.UpdateBanksAsync(bank);
                }
            }

            //await BanksImporter.SearchTwitterUsernamesAsync(parseBanks);
        }

        private static async Task SyncBanksAsync()
        {
            var allBanksParser = new AllBanksParser();
            List<Bank> banks = allBanksParser.Parse().ToList();
            IList<Bank> parseBanks = await ParseHelper.GetBanksAsync();
            var newBanks = new List<Bank>();
            foreach (Bank bank in banks)
            {
                if (!parseBanks.Any(x => Equals(x.License, bank.License)))
                {
                    newBanks.Add(bank);
                }
            }

            await ParseHelper.ImportBanksAsync(newBanks);
        }

        private static async Task CleanupBanks()
        {
            IList<Bank> banks = await ParseHelper.GetBanksAsync();
            foreach (Bank bank in banks)
            {
            }

            await ParseHelper.UpdateBanksAsync(banks.ToArray());
        }
    }
}