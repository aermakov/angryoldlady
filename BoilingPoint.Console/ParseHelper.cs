﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using BoilingPoint.Portable.Attributes;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Geo;
using Parse;

namespace BoilingPoint.Console
{
    public static class ParseHelper
    {
        public static async Task ImportBanksAsync(IEnumerable<Bank> banks)
        {
            IList<ParseObject> parseObjects = new List<ParseObject>();
            IDictionary<Bank, ParseObject> dictionary = new Dictionary<Bank, ParseObject>();
            foreach (Bank bank in banks)
            {
                ParseObject parseObject = GetParseObject(bank);
                parseObjects.Add(parseObject);
                dictionary.Add(bank, parseObject);
            }

            await ParseObject.SaveAllAsync(parseObjects);

            foreach (var pair in dictionary)
            {
                pair.Key.objectId = pair.Value.ObjectId;
            }
        }

        public static async Task<IList<Bank>> GetBanksAsync()
        {
            const int limit = 1000;

            IList<Bank> banks = new List<Bank>();
            int count = await ParseObject.GetQuery("Bank").CountAsync();
            if (count > 0)
            {
                int pagesCount = count > 0 ? (int) Math.Ceiling(count/(double) limit) : 0;
                for (int page = 0; page < pagesCount; page++)
                {
                    IEnumerable<ParseObject> parseObjects =
                        await ParseObject.GetQuery("Bank").Skip(limit*page).Limit(limit).FindAsync();
                    foreach (ParseObject parseObject in parseObjects)
                    {
                        banks.Add(GetObject<Bank>(parseObject));
                    }
                }
            }
            return banks;
        }

        public static async Task UpdateBanksAsync(params Bank[] banks)
        {
            IEnumerable<IGrouping<int, Bank>> chunks =
                Enumerable.Range(0, banks.Length).GroupBy(index => index/100, index => banks[index]);
            IList<ParseObject> parseObjects = new List<ParseObject>();
            foreach (var chunk in chunks)
            {
                parseObjects.Clear();
                foreach (Bank bank in chunk)
                {
                    ParseObject parseObject = GetParseObject(bank);
                    parseObjects.Add(parseObject);
                }

                await ParseObject.SaveAllAsync(parseObjects);
            }
        }

        public static async Task ImportBanksAsync(IList<Bank> banks)
        {
            IEnumerable<IGrouping<int, Bank>> chunks =
                Enumerable.Range(0, banks.Count).GroupBy(index => index/100, index => banks[index]);
            foreach (var chunk in chunks)
            {
                await ImportBanksAsync(chunk);
            }
        }

        public static T GetObject<T>(ParseObject parseObject) where T : BaseEntity
        {
            if (parseObject != null)
            {
                Type type = typeof (T);
                var t = (T) Activator.CreateInstance(type);

                t.objectId = parseObject.ObjectId;
                t.updatedAt = parseObject.UpdatedAt;
                t.createdAt = parseObject.CreatedAt;

                UpdateObject(t, parseObject);

                return t;
            }

            return default(T);
        }

        public static ParseObject GetParseObject<T>(T data) where T : BaseEntity
        {
            Type type = typeof (T);
            string className = type.Name;
            var parseObject = new ParseObject(className) {ObjectId = data.objectId};

            UpdateParseObject(parseObject, data);

            return parseObject;
        }

        private static void UpdateObject<T>(T t, IEnumerable<KeyValuePair<string, object>> parseObject)
        {
            Type type = typeof (T);
            PropertyInfo[] properties = type.GetProperties();
            foreach (var pair in parseObject)
            {
                PropertyInfo property = properties.SingleOrDefault(x => x.Name == pair.Key);
                if (property == null || property.GetSetMethod() == null)
                    continue;

                object value = pair.Value;
                if (value != null && property.PropertyType.IsEnum)
                {
                    value = Enum.Parse(property.PropertyType, value.ToString());
                }

                if (value is ParseGeoPoint)
                {
                    value = new GeoPoint(((ParseGeoPoint) value).Latitude, ((ParseGeoPoint) value).Longitude);
                }

                SetValue(t, property.Name, value);
            }
        }

        public static void SetValue(object inputObject, string propertyName, object propertyVal)
        {
            // Find out the type
            Type type = inputObject.GetType();

            // Get the property information based on the type
            PropertyInfo propertyInfo = type.GetProperty(propertyName);

            // Find the property type
            Type propertyType = propertyInfo.PropertyType;

            // Convert.ChangeType does not handle conversion to nullable types
            // if the property type is nullable, we need to get the underlying type of the property
            Type targetType = IsNullableType(propertyType) ? Nullable.GetUnderlyingType(propertyType) : propertyType;

            if (propertyVal is List<object>)
            {
                var list = (List<object>) propertyVal;
                object target = Activator.CreateInstance(targetType);
                for (int i = 0; i < list.Count; i++)
                {
                    object value = list[i];
                    value = Convert.ChangeType(value, targetType.GenericTypeArguments[0]);

                    propertyType.GetMethod("Add").Invoke(target, new[] {value});
                }

                SetValue(inputObject, propertyName, target);
            }
            else
            {
                // Returns an System.Object with the specified System.Type and whose value is
                // equivalent to the specified object.
                propertyVal = Convert.ChangeType(propertyVal, targetType);

                // Set the value of the property
                propertyInfo.SetValue(inputObject, propertyVal, null);
            }
        }

        private static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof (Nullable<>);
        }

        public static void UpdateParseObject<T>(ParseObject parseObject, T data)
        {
            Type type = typeof (T);
            PropertyInfo[] properties = type.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                var attribute = property.GetCustomAttribute<IgnoreAttribute>();
                if (attribute != null) continue;
                object value = property.GetValue(data);

                if (property.PropertyType.IsEnum)
                    value = value.ToString();

                parseObject[property.Name] = value;
            }
        }
    }
}