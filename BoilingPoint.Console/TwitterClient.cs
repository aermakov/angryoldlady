﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using HtmlAgilityPack;

namespace BoilingPoint.Console
{
    public class TwitterClient
    {
        private readonly Encoding _encoding = Encoding.UTF8;

        public IEnumerable<string> SearchUsers(string queryText)
        {
            string address = string.Format("https://mobile.twitter.com/search/users?q={0}",
                                           HttpUtility.UrlEncode(queryText));

            var usernames = new List<string>();
            do
            {
                List<string> pageUsernames = ParseSearchUsersPage(ref address).ToList();
                if (pageUsernames.Count > 0)
                {
                    usernames.AddRange(pageUsernames);
                    break; // Grab users only from first page
                }
            } while (!string.IsNullOrEmpty(address));
            return usernames;
        }

        public TwitterUser GetUser(string username)
        {
            var user = new TwitterUser {Username = username};

            if (username.StartsWith("@"))
                username = username.Substring(1);

            string address = string.Format("https://mobile.twitter.com/{0}", username);
            string s = HttpHelper.DownloadString(address, _encoding);

            var document = new HtmlDocument();
            if (s != null)
            {
                document.LoadHtml(s);

                HtmlNode fullnameNode = document.DocumentNode.SelectSingleNode("//div[@class='fullname']");
                if (fullnameNode != null)
                {
                    user.Fullname = fullnameNode.InnerText.Trim();
                }

                HtmlNode locationNode = document.DocumentNode.SelectSingleNode("//div[@class='location']");
                if (locationNode != null)
                {
                    user.Location = locationNode.InnerText.Trim();
                }

                HtmlNode bioNode = document.DocumentNode.SelectSingleNode("//div[@class='bio']");
                if (bioNode != null)
                {
                    user.Bio = bioNode.InnerText.Trim();
                }

                HtmlNode urlNode = document.DocumentNode.SelectSingleNode("//a[string-length(@data-url) > 0]");
                if (urlNode != null)
                {
                    user.Url = urlNode.GetAttributeValue("data-url", string.Empty);
                }
            }
            return user;
        }

        private IEnumerable<string> ParseSearchUsersPage(ref string address)
        {
            IList<string> usernames = new List<string>();

            string s = HttpHelper.DownloadString(address, _encoding);

            address = null;
            if (!string.IsNullOrEmpty(s))
            {
                var document = new HtmlDocument();
                document.LoadHtml(s);
                HtmlNodeCollection nodes = document.DocumentNode.SelectNodes("//span[@class='username']");
                if (nodes != null)
                {
                    foreach (HtmlNode node in nodes)
                    {
                        usernames.Add(node.InnerText.Trim());
                    }
                }

                HtmlNode showMoreNode = document.DocumentNode.SelectSingleNode("//div[@class='w-button-more']/a");
                if (showMoreNode != null)
                {
                    string href = showMoreNode.GetAttributeValue("href", string.Empty);
                    address = string.Format("https://mobile.twitter.com{0}", href);
                }
            }
            return usernames;
        }
    }
}