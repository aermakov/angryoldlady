﻿using System;
using System.Net;
using System.Text;
using System.Threading;

namespace BoilingPoint.Console
{
    public static class HttpHelper
    {
        public static string DownloadString(string address, Encoding encoding, int maxAttempts = 5)
        {
            int attempts = 0;
            string s;
            do
            {
                Thread.Sleep(attempts*5000);

                using (var webClient = new WebClient {Encoding = encoding})
                {
                    try
                    {
                        s = webClient.DownloadString(address);
                    }
                    catch (Exception)
                    {
                        s = null;
                    }
                }
            } while (string.IsNullOrEmpty(s) && attempts++ < maxAttempts);
            return s;
        }
    }
}