﻿using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace BoilingPoint.Console
{
    public static class Translator
    {
        #region Fields

        private static readonly Dictionary<char, string> TranslitDictionary
            = new Dictionary<char, string>
                  {
                      {'а', "a"},
                      {'б', "b"},
                      {'в', "v"},
                      {'г', "g"},
                      {'д', "d"},
                      {'е', "e"},
                      {'ж', "zh"},
                      {'з', "z"},
                      {'и', "i"},
                      {'й', "y"},
                      {'к', "k"},
                      {'л', "l"},
                      {'м', "m"},
                      {'н', "n"},
                      {'о', "o"},
                      {'п', "p"},
                      {'р', "r"},
                      {'с', "s"},
                      {'т', "t"},
                      {'у', "u"},
                      {'ф', "f"},
                      {'х', "h"},
                      {'ц', "c"},
                      {'ч', "ch"},
                      {'ш', "sh"},
                      {'щ', "shh"},
                      {'ъ', ""},
                      {'ы', "i"},
                      {'ь', ""},
                      {'э', "e"},
                      {'ю', "u"},
                      {'я', "ya"}
                  };

        #endregion

        public static string ToTranslit(string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;

            var builder = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                bool isUpperCase = (str[i] >= 'А') && (str[i] <= 'Я');
                bool isLowerCase = (str[i] >= 'а') && (str[i] <= 'я');
                if (isUpperCase || isLowerCase)
                {
                    char lowerCase = str[i].ToString(CultureInfo.InvariantCulture).ToLower()[0];
                    string letter = TranslitDictionary[lowerCase];
                    if (isUpperCase && letter.Length >= 1)
                        letter = letter.Substring(0, 1).ToUpper() + letter.Substring(1);
                    builder.Append(letter);
                }
                else
                {
                    builder.Append(str[i]);
                }
            }
            return builder.ToString();
        }
    }
}