﻿namespace BoilingPoint.Console
{
    public class TwitterUser
    {
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Location { get; set; }
        public string Bio { get; set; }
        public string Url { get; set; }
    }
}