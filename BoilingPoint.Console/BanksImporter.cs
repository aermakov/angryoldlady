﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoilingPoint.Portable;
using BoilingPoint.Portable.Data;

namespace BoilingPoint.Console
{
    public static class BanksImporter
    {
        public static async Task ImportBanksAsync()
        {
            var parser = new AllBanksParser();

            IList<Bank> banks = await ParseHelper.GetBanksAsync();
            if (banks.Count == 0)
            {
                banks = parser.Parse().ToList();
                await ParseHelper.ImportBanksAsync(banks);
            }

            await SearchTwitterUsernamesAsync(banks);
        }

        public static async Task SearchTwitterUsernamesAsync(IList<Bank> banks)
        {
            for (int i = 0; i < banks.Count; i++)
            {
                Bank bank = banks[i];
                if (!string.IsNullOrEmpty(bank.TwitterUsername)) continue;
                string twitterUsername = SearchTwitterUsername(bank.WebSite);
                if (twitterUsername == null)
                {
                    var list =
                        new List<string>
                            {
                                bank.Title,
                                string.Format("{0} {1}", bank.Title, bank.Country)
                            };

                    if (!Translator.ToTranslit(bank.Title.ToLower()).Contains("bank"))
                    {
                        list.Add(string.Format("Банк {0}", bank.Title));
                    }

                    for (int j = 0; j < list.Count && twitterUsername == null; j++)
                    {
                        twitterUsername = SearchTwitterUsername(bank.WebSite, list[j]);
                    }
                }

                if (twitterUsername == null) continue;
                bank.TwitterUsername = twitterUsername.Substring(1).ToLower().Trim();
                await ParseHelper.UpdateBanksAsync(bank);
            }
        }

        private static string SearchTwitterUsername(string webSite)
        {
            var twitterClient = new TwitterClient();
            if (string.IsNullOrEmpty(webSite))
                return null;

            string domain = Utils.NormalizeWebSite(webSite);

            string twitterUsername = null;
            string queryText = string.Format("{0}", domain);
            List<string> usernames = twitterClient.SearchUsers(queryText).ToList();
            for (int i = 0; i < usernames.Count && twitterUsername == null; i++)
            {
                string username = usernames[i];
                TwitterUser twitterUser = twitterClient.GetUser(username);
                if (twitterUser.Url != null &&
                    Equals(Utils.NormalizeWebSite(twitterUser.Url), domain))
                {
                    twitterUsername = twitterUser.Username;
                }
            }
            return twitterUsername;
        }

        private static string SearchTwitterUsername(string webSite, string fullname)
        {
            const int MAX_DISTANCE = 2;

            var twitterClient = new TwitterClient();
            if (string.IsNullOrEmpty(fullname))
                return null;

            string twitterUsername = null;
            string queryText = string.Format("{0}", fullname);

            string domain = Utils.NormalizeWebSite(webSite);
            fullname = Translator.ToTranslit(fullname.ToLower());

            List<string> usernames = twitterClient.SearchUsers(queryText).ToList();
            for (int i = 0; i < usernames.Count && twitterUsername == null; i++)
            {
                string username = usernames[i];
                TwitterUser twitterUser = twitterClient.GetUser(username);
                if (twitterUser.Url != null && Equals(Utils.NormalizeWebSite(twitterUser.Url), domain))
                {
                    twitterUsername = twitterUser.Username;
                }
                else if (twitterUser.Fullname != null)
                {
                    int distance = LevenshteinDistance.Compute(
                        fullname,
                        Translator.ToTranslit(twitterUser.Fullname.ToLower()));
                    if (distance < MAX_DISTANCE)
                    {
                        string bio = Translator.ToTranslit(twitterUser.Bio ?? string.Empty);
                        if (bio.IndexOf("bank", StringComparison.InvariantCultureIgnoreCase) == -1)
                        {
                            try
                            {
                                if (!Utils.NormalizeWebSite(twitterUser.Url).Contains("bank"))
                                {
                                    if (twitterUser.Username.IndexOf("bank",
                                                                     StringComparison.InvariantCultureIgnoreCase) != -1)
                                    {
                                        twitterUsername = twitterUser.Username;
                                    }
                                }
                                else
                                {
                                    twitterUsername = twitterUser.Username;
                                }
                            }
                            catch (NullReferenceException)
                            {
                            }
                        }
                        else
                        {
                            twitterUsername = twitterUser.Username;
                        }
                    }
                }
            }

            return twitterUsername;
        }
    }
}