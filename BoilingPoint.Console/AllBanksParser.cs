﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using BoilingPoint.Portable;
using BoilingPoint.Portable.Data;
using HtmlAgilityPack;
using PhoneNumbers;

namespace BoilingPoint.Console
{
    public class AllBanksParser
    {
        private readonly Encoding _encoding = Encoding.GetEncoding(1251);
        private NumberFormat _numberFormat;
        private PhoneNumberUtil _phoneUtil;

        public IEnumerable<Bank> Parse(int pagesCount = -1)
        {
            NumberFormat.Builder builder = NumberFormat.CreateBuilder();
            builder.Pattern = "(\\d{3})(\\d{3})(\\d{4})";
            builder.Format = "($1) $2-$3";
            _numberFormat = builder.Build();
            _phoneUtil = PhoneNumberUtil.GetInstance();

            var allBanks = new HashSet<Bank>();
            if (pagesCount == -1) pagesCount = GetPagesCount();
            for (int page = 1; page <= pagesCount; page++)
            {
                IList<Bank> pageBanks = ParsePage(page).ToList();
                foreach (Bank bank in pageBanks.Where(bank => !allBanks.Contains(bank)))
                {
                    allBanks.Add(bank);
                }
            }
            return allBanks;
        }

        private int GetPagesCount()
        {
            int pagesCount = 0;
            string s = HttpHelper.DownloadString("http://www.allbanks.ru/banks/", _encoding);
            var document = new HtmlDocument();
            if (s != null)
            {
                document.LoadHtml(s);

                HtmlNode node = document.DocumentNode.SelectSingleNode("//img[contains(@src, 'last')]").ParentNode;
                if (node != null)
                {
                    string href = node.GetAttributeValue("href", string.Empty);

                    int result;
                    if (int.TryParse(StripNonDigits(href), out result))
                    {
                        pagesCount = result;
                    }
                }
            }
            return pagesCount;
        }

        private string StripNonDigits(string s)
        {
            var builder = new StringBuilder();
            foreach (char c in s.Where(char.IsDigit))
            {
                builder.Append(c);
            }
            return builder.ToString();
        }

        private IEnumerable<Bank> ParsePage(int page)
        {
            string address = string.Format("http://www.allbanks.ru/banks/page{0}/", page);
            string s = HttpHelper.DownloadString(address, _encoding);

            if (string.IsNullOrEmpty(s)) yield break;

            var document = new HtmlDocument();
            document.LoadHtml(s);
            HtmlNodeCollection tableNodes = document.DocumentNode.SelectNodes("//table[@class='bankBlock']");
            if (tableNodes == null) yield break;

            foreach (Bank bank in tableNodes.Select(ParseBank))
            {
                yield return bank;
            }
        }

        private Bank ParseBank(HtmlNode tableNode)
        {
            var bank = new Bank();

            HtmlNode titleNode = tableNode.SelectSingleNode(".//h3/a[1]");
            if (titleNode != null)
            {
                bank.Title =
                    Utils.RemoveParenthesis(HttpUtility.HtmlDecode(titleNode.InnerText.Trim().Replace("&nbsp;", " ")));
                bank.LowercaseTitle = bank.Title.ToLowerInvariant();
                bank.LowercaseHashtagTitle = Utils.GetLowerCaseHashtag(bank.Title);
            }

            HtmlNode countryNode = tableNode.SelectSingleNode(".//h3/a[2]");
            if (countryNode != null)
            {
                bank.Country = HttpUtility.HtmlDecode(countryNode.InnerText.Trim());
            }

            HtmlNode licenseNode = tableNode.SelectSingleNode(".//div[@class='license']");
            if (licenseNode != null)
            {
                bank.License = HttpUtility.HtmlDecode(licenseNode.InnerText.Trim().Replace("&nbsp;", " "));

                HtmlNode addressNode = licenseNode.NextSibling.NextSibling;
                if (addressNode != null)
                {
                    bank.Address = HttpUtility.HtmlDecode(addressNode.InnerText.Trim().Replace("&nbsp;", " "));
                }
            }

            HtmlNode phoneNumberNode = tableNode.SelectSingleNode(".//img[contains(@src, 'ico_phone')]");
            if (phoneNumberNode != null)
            {
                bank.PhoneNumber =
                    Portable.PhoneNumbers.PhoneNumberUtil.Normalize(
                        HttpUtility.HtmlDecode(phoneNumberNode.ParentNode.InnerText.Trim().Replace("&nbsp;", " ")));

                if (bank.CountryCode != null)
                {
                    PhoneNumber phoneNumber =
                        _phoneUtil.Parse(bank.PhoneNumber, bank.CountryCode.ToUpper());

                    bank.InternationalPhoneNumber =
                        _phoneUtil.FormatByPattern(phoneNumber, PhoneNumberFormat.INTERNATIONAL,
                                                   new List<NumberFormat> {_numberFormat});
                }

                bank.PhoneNumber = PhoneNumberUtil.NormalizeDigitsOnly(bank.InternationalPhoneNumber);
            }

            HtmlNode webSiteNode = tableNode.SelectSingleNode(".//img[contains(@src, 'ico_url')]");
            if (webSiteNode != null)
            {
                bank.WebSite = Utils.NormalizeWebSite(webSiteNode.ParentNode.GetAttributeValue("href", string.Empty));
            }

            return bank;
        }
    }
}