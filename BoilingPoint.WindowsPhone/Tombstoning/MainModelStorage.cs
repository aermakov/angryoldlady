﻿using BoilingPoint.WindowsPhone.ViewModels;
using Caliburn.Micro;

namespace BoilingPoint.WindowsPhone.Tombstoning
{
    public class MainModelStorage : StorageHandler<MainViewModel>
    {
        public override void Configure()
        {
            Property(x => x.BankId).InPhoneState();
            Property(x => x.BankTitle).InPhoneState();
            Property(x => x.CashBoxAvailable).InPhoneState();
            Property(x => x.CashBoxClosed).InPhoneState();
            Property(x => x.Comment).InPhoneState();
            Property(x => x.Latitude).InPhoneState();
            Property(x => x.Longitude).InPhoneState();
            Property(x => x.PeopleInline).InPhoneState();
            Property(x => x.WaitTime).InPhoneState();
        }
    }
}