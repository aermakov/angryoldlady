﻿using System;
using System.Reflection;
using Microsoft.Phone.Info;

namespace BoilingPoint.WindowsPhone.Helpers
{
    public enum DeviceKey
    {
        DeviceName,
        DeviceUniqueId,
        DeviceManufacturer,
        ApplicationCurrentMemoryUsage,
        ApplicationPeakMemoryUsage,
        DeviceFirmwareVersion,
        DeviceHardwareVersion,
        DeviceTotalMemory
    }

    public static class DeviceInfoHelper
    {
        private const int ANIDLength = 32;
        private const int ANIDOffset = 2;

        public static T GetDeviceInfo<T>(DeviceKey key)
        {
            T result = default(T);
            object deviceInfo;
            if (DeviceExtendedProperties.TryGetValue(key.ToString(), out deviceInfo))
            {
                result = (T) deviceInfo;
            }
            return result;
        }

        public static string GetVersionNumber()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            string[] parts = assembly.FullName.Split(',');
            return parts[1].Split('=')[1];
        }

        public static string GetDeviceId()
        {
            return Convert.ToBase64String(GetDeviceInfo<byte[]>(DeviceKey.DeviceUniqueId));
        }

        public static string GetAnonymousId()
        {
            string result = string.Empty;
            object anid;
            if (UserExtendedProperties.TryGetValue("ANID", out anid))
            {
                if (anid != null && anid.ToString().Length >= (ANIDLength + ANIDOffset))
                {
                    result = anid.ToString().Substring(ANIDOffset, ANIDLength);
                }
            }

            return result;
        }
    }
}