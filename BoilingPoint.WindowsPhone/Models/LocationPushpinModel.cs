﻿namespace BoilingPoint.WindowsPhone.Models
{
    public class LocationPushpinModel : PushpinModelBase
    {
        public override PushpinType Type
        {
            get { return PushpinType.Location; }
        }
    }
}