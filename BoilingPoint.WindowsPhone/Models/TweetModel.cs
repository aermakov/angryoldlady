﻿using BoilingPoint.Portable.Data;

namespace BoilingPoint.WindowsPhone.Models
{
    public class TweetModel : Portable.Models.TweetModel
    {
        #region Events
        #endregion

        #region Ctors

        public TweetModel()
        {
        }

        public TweetModel(Tweet tweet) 
            : base(tweet)
        {
        }

        #endregion
    }
}