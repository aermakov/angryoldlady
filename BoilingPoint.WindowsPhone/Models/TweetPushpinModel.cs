﻿using System.Device.Location;
using System.Linq;
using System.Windows;
using BoilingPoint.Portable.Data;

namespace BoilingPoint.WindowsPhone.Models
{
    public class TweetPushpinModel : PushpinModelBase
    {
        #region Fields

        private readonly string _address;

        #endregion

        #region Ctors

        public TweetPushpinModel(TweetGrouping tweetGrouping)
        {
            TweetGrouping = tweetGrouping;
            Visibility = Visibility.Visible;
            ContentVisibility = Visibility.Collapsed;

            Tweet tweet = tweetGrouping.First();

            _location = new GeoCoordinate(tweet.Latitude, tweet.Longitude);
            _address = tweet.Address;
        }

        #endregion

        #region Properties

        public string Address
        {
            get { return _address; }
        }

        public override PushpinType Type
        {
            get { return PushpinType.Tweet; }
        }

        public TweetGrouping TweetGrouping { get; private set; }

        #endregion

        #region Methods

        public void AddTweetToGrouping(Tweet tweet)
        {
            TweetGrouping.Add(tweet);
        }

        #endregion
    }
}