﻿using System;
using System.ComponentModel;
using System.IO.IsolatedStorage;

namespace BoilingPoint.WindowsPhone
{
    public class Settings
    {
        #region Fields

        public const string BUGSENSE_API_KEY = "0ba19cd8";

        public const string YANDEX_MAPKIT_KEY =
            "iQiK14QymJ-QawynFUThDRBFUQxzFUzfzOTNn09DmZGfElk1Kc0u7uz~5BbTyp-0tIMW1qMFeARSUwHTx7aH8BJMPNRkctRQH3khlqm8esw=";

        public const uint YANDEX_METRICA_API_KEY = 12652;

        public const string PARSE_APPLICATION_ID = "vLM3l6oOTcTNpxtlWcXhXFG6BdItGPbyCMAykM6T";

        public const string PARSE_REST_API_KEY = "OxzeKviVZIgENXu50Frc5pCNshH8d2sjzS5hQNwQ";

        public const string TWITTER_CONSUMER_KEY = "4ay9XbSbFnQAHjloSJ7JpA";

        public const string TWITTER_CONSUMER_SECRET = "ABOJcPrrRdP6gdTdbLKJJvwZ6Ul9bhl9RX3PjGC8hk";

        protected readonly IsolatedStorageSettings _settings;

        #endregion

        #region Ctors

        /// <summary>
        ///     Constructor that gets the application settings.
        /// </summary>
        protected internal Settings()
        {
            if (!DesignerProperties.IsInDesignTool)
            {
                // Get the settings for this application.
                _settings = IsolatedStorageSettings.ApplicationSettings;
            }
        }

        #endregion

        #region Properties

        public bool? GeolocationEnabled
        {
            get { return GetValueOrDefault<bool?>("GeolocationEnabled", null); }
            set { if (AddOrUpdateValue("GeolocationEnabled", value)) Save(); }
        }

        public bool SignatureEnabled
        {
            get { return GetValueOrDefault("SignatureEnabled", true); }
            set { if (AddOrUpdateValue("SignatureEnabled", value)) Save(); }
        }

        public string Signature
        {
            get
            {
                var signature = GetValueOrDefault<string>("Signature", null);
                if (signature == null)
                {
                    signature = AppResources.DefaultSignature;
                }
                return signature;
            }
            set { if (AddOrUpdateValue("Signature", value)) Save(); }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Update a setting value for our application. If the setting does not
        ///     exist, then add the setting.
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected bool AddOrUpdateValue(string Key, Object value)
        {
            bool valueChanged = false;

            // If the key exists
            if (_settings.Contains(Key))
            {
                // If the value has changed
                if (_settings[Key] != value)
                {
                    // Store the new value
                    _settings[Key] = value;
                    valueChanged = true;
                }
            }
                // Otherwise create the key.
            else
            {
                _settings.Add(Key, value);
                valueChanged = true;
            }
            return valueChanged;
        }

        /// <summary>
        ///     Get the current value of the setting, or if it is not found, set the
        ///     setting to the default setting.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        protected T GetValueOrDefault<T>(string Key, T defaultValue)
        {
            T value;

            // If the key exists, retrieve the value.
            if (_settings.Contains(Key))
            {
                value = (T) _settings[Key];
            }
                // Otherwise, use the default value.
            else
            {
                value = defaultValue;
            }
            return value;
        }

        /// <summary>
        ///     Save the settings.
        /// </summary>
        protected void Save()
        {
            _settings.Save();
        }

        #endregion
    }
}