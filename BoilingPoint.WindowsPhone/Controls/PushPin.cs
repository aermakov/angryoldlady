﻿using System.Windows;
using System.Windows.Controls;

namespace BoilingPoint.WindowsPhone.Controls
{
    public class PushPin : Yandex.Maps.PushPin
    {
        protected override void OnContentVisibilityChanged()
        {
            base.OnContentVisibilityChanged();

            if (ContentVisibility == Visibility.Visible)
            {
                Canvas.SetZIndex(this, 999);
            }
            else
            {
                Canvas.SetZIndex(this, 0);
            }
        }
    }
}