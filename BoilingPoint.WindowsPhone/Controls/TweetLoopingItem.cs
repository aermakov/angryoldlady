﻿using System;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Models;
using Telerik.Windows.Controls;

namespace BoilingPoint.WindowsPhone.Controls
{
    public class TweetLoopingItem : LoopingListDataItem
    {
        #region Events

        #endregion

        #region Fields

        private readonly TweetModel _tweetModel;

        #endregion

        #region Ctors

        public TweetLoopingItem(Tweet tweet)
        {
            _tweetModel = new TweetModel(tweet);
        }

        #endregion

        #region Properties

        public TimeSpan WaitTime
        {
            get { return _tweetModel.WaitTime; }
        }

        public ulong ID
        {
            get { return _tweetModel.ID; }
        }

        public string ProfileImageUrl
        {
            get { return _tweetModel.ProfileImageUrl; }
        }

        public string UserName
        {
            get { return _tweetModel.UserName; }
        }

        public string UserScreenName
        {
            get { return _tweetModel.UserScreenName; }
        }

        public string Status
        {
            get { return _tweetModel.Status; }
        }

        public bool StatusVisible
        {
            get { return !string.IsNullOrEmpty(Status); }
        }

        public int PeopleInline
        {
            get { return _tweetModel.PeopleInline; }
        }

        public string WaitTimeFormatted
        {
            get { return _tweetModel.WaitTimeFormatted; }
        }

        public int CashBoxAvailable
        {
            get { return _tweetModel.CashBoxAvailable; }
        }

        public int CashBoxClosed
        {
            get { return _tweetModel.CashBoxClosed; }
        }

        public double Latitude
        {
            get { return _tweetModel.Latitude; }
        }

        public double Longitude
        {
            get { return _tweetModel.Longitude; }
        }

        public string BankId
        {
            get { return _tweetModel.BankId; }
        }

        public bool BankVisible
        {
            get { return BankId != null; }
        }

        public string ProfileId
        {
            get { return _tweetModel.ProfileId; }
        }

        public bool ServiceTagsAvailable
        {
            get { return PeopleInline > 0 || !Equals(WaitTime, TimeSpan.Zero) || CashBoxAvailable > 0 || CashBoxClosed > 0; }
        }

        public DateTime CreatedAtLocal
        {
            get { return _tweetModel.CreatedAtLocal; }
        }

        public DateTime CreatedAtUtc
        {
            get { return _tweetModel.CreatedAtUtc; }
        }

        public string CreatedAtTimeago
        {
            get { return _tweetModel.CreatedAtTimeago; }
        }

        public string CreatedAtShortTimeago
        {
            get { return _tweetModel.CreatedAtShortTimeago; }
        }

        public string CreatedAtUtcFormatted
        {
            get { return _tweetModel.CreatedAtUtcFormatted; }
        }

        public string Address
        {
            get { return _tweetModel.Address; }
        }

        public bool PeopleInlineVisible
        {
            get { return PeopleInline != 0; }
        }

        public bool WaitTimeVisible
        {
            get { return !Equals(WaitTime, TimeSpan.Zero); }
        }

        public bool CashBoxAvailableVisible
        {
            get { return CashBoxAvailable != 0; }
        }

        public bool CashBoxClosedVisible
        {
            get { return CashBoxClosed != 0; }
        }

        public bool AddressVisible
        {
            get { return !string.IsNullOrEmpty(Address); }
        }

        public string BankTitle
        {
            get { return _tweetModel.BankTitle; }
        }

        public bool BankTitleVisible
        {
            get { return !string.IsNullOrEmpty(BankTitle); }
        }

        #endregion

        #region Methods

        #endregion
    }
}