﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.Yandex;
using BoilingPoint.Portable.Services;
using BoilingPoint.WindowsPhone.Services;
using BoilingPoint.WindowsPhone.ViewModels;
using Caliburn.Micro;
using Caliburn.Micro.BindableAppBar;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Parse;
using Yandex.Metrica;
using DataService = BoilingPoint.WindowsPhone.Services.DataService;
using ITwitterService = BoilingPoint.WindowsPhone.Services.ITwitterService;
using TwitterService = BoilingPoint.WindowsPhone.Services.TwitterService;

namespace BoilingPoint.WindowsPhone
{
    public class AppBootstrapper : PhoneBootstrapper
    {
        private PhoneContainer _container;

        protected override PhoneApplicationFrame CreatePhoneApplicationFrame()
        {
            return new TransitionFrame();
        }

        protected override void Configure()
        {
            _container = new PhoneContainer();

            _container.RegisterPhoneServices(RootFrame);

            _container.PerRequest<MainViewModel>();
            _container.PerRequest<GoogleMapViewModel>();
            _container.PerRequest<YandexMapViewModel>();
            _container.PerRequest<SettingsViewModel>();
            _container.PerRequest<AboutViewModel>();
            _container.PerRequest<OAuthViewModel>();
            _container.PerRequest<ProfileViewModel>();
            _container.PerRequest<BankViewModel>();

            _container.RegisterSingleton(typeof (IGeocodeService), null, typeof (GeocodeService));
            _container.RegisterSingleton(typeof (IGeocoder), null, typeof (YandexGeocoder));
            _container.RegisterSingleton(typeof (IAnalyticalService), null, typeof (YandexMetricaAnalyticalService));
            _container.RegisterSingleton(typeof (IDataService), null, typeof (DataService));
            _container.RegisterSingleton(typeof (IGeolocationService), null, typeof (GeolocationService));
            _container.RegisterSingleton(typeof (ISettingsService), null, typeof (SettingsService));
            _container.RegisterSingleton(typeof (INotificationService), null, typeof (NotificationService));
            _container.RegisterSingleton(typeof (ITwitterService), null, typeof (TwitterService));
            _container.RegisterSingleton(typeof (IAnonymousTwitterService), null, typeof (AnonymousTwitterService));

            ParseConfiguration.Configure(Settings.PARSE_APPLICATION_ID, Settings.PARSE_REST_API_KEY);

            AddCustomConventions();

            EnableLocationService();
        }

        private void EnableLocationService()
        {
            var settings = new Settings();
            bool? geolocationEnabled = settings.GeolocationEnabled;
            if (!geolocationEnabled.HasValue)
            {
                settings.GeolocationEnabled = true;
            }
        }

        private static void AddCustomConventions()
        {
            ConventionManager.AddElementConvention<BindableAppBarButton>(
                Control.IsEnabledProperty, "DataContext", "Click");

            ConventionManager.AddElementConvention<BindableAppBarMenuItem>(
                Control.IsEnabledProperty, "DataContext", "Click");
        }

        protected override void OnLaunch(object sender, LaunchingEventArgs e)
        {
        }

        protected override void OnClose(object sender, ClosingEventArgs e)
        {
            Counter.ReportExit();

            base.OnClose(sender, e);
        }

        protected override void OnUnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            var logService = (IAnalyticalService) _container.GetInstance(typeof (IAnalyticalService), null);
            if (logService != null)
            {
                logService.ReportError(e.ExceptionObject);
            }

            if (Debugger.IsAttached)
            {
                Debugger.Break();
                e.Handled = true;
            }
            else
            {
                MessageBox.Show(AppResources.UnexpectedError, "Oops...", MessageBoxButton.OK);
                e.Handled = true;
            }

            base.OnUnhandledException(sender, e);
        }

        protected override object GetInstance(Type service, string key)
        {
            return _container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }
    }
}