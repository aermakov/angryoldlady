﻿using BoilingPoint.Portable.Geo;
using BoilingPoint.WindowsPhone.ViewModels;
using Caliburn.Micro;

namespace BoilingPoint.WindowsPhone.Extensions
{
    public static class NavigationServiceExtensions
    {
        public static void NavigateToMapView(this INavigationService navigationService, GeoAddress geoAddress)
        {
            if (geoAddress != null)
            {
                navigationService.NavigateToMapView(geoAddress.Coordinates.Latitude,
                                                    geoAddress.Coordinates.Longitude,
                                                    null,
                                                    0ul);
            }
        }

        public static void NavigateToMapView(
            this INavigationService navigationService,
            double latitude,
            double longitude,
            string profileId,
            ulong tweetID)
        {
            navigationService.UriFor<YandexMapViewModel>()
                             .WithParam(x => x.Latitude, latitude)
                             .WithParam(x => x.Longitude, longitude)
                             .WithParam(x => x.ProfileId, profileId)
                             .WithParam(x => x.TweetID, tweetID)
                             .Navigate();
        }
    }
}