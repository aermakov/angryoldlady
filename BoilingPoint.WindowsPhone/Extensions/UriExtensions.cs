﻿using System;

namespace BoilingPoint.WindowsPhone.Extensions
{
    public static class UriExtensions
    {
        private const string ExternalAddress = "app://external/";

        /// <summary>
        ///     Returns whether the uri is from an external source.
        /// </summary>
        /// <param name="uri">The uri</param>
        public static bool IsExternalNavigation(this Uri uri)
        {
            return (ExternalAddress == uri.ToString());
        }
    }
}