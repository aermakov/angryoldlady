﻿using System.Collections.Generic;
using System.Device.Location;
using BoilingPoint.Portable.Geo;
using Microsoft.Phone.Controls.Maps;
using Microsoft.Phone.Controls.Maps.Platform;

namespace BoilingPoint.WindowsPhone.Extensions
{
    public static class LocationExtensions
    {
        public static GeoCoordinate ToCoordinate(this Location routeLocation)
        {
            return new GeoCoordinate(routeLocation.Latitude, routeLocation.Longitude);
        }

        public static Location ToLocation(this GeoCoordinate coordinate)
        {
            return new Location {Latitude = coordinate.Latitude, Longitude = coordinate.Longitude};
        }

        public static Location ToLocation(this GeoAddress geoAddress)
        {
            return new Location
                       {
                           Latitude = geoAddress.Coordinates.Latitude,
                           Longitude = geoAddress.Coordinates.Longitude
                       };
        }

        public static LocationCollection ToCoordinates(this IEnumerable<Location> points)
        {
            var locations = new LocationCollection();

            if (points != null)
            {
                foreach (Location point in points)
                {
                    locations.Add(point.ToCoordinate());
                }
            }

            return locations;
        }
    }
}