﻿using System.Device.Location;
using BoilingPoint.Portable.Geo;

namespace BoilingPoint.WindowsPhone.Extensions
{
    public static class GeoPointExtensions
    {
        public static GeoCoordinate ToGeoCoordinate(this GeoPoint geoPoint)
        {
            return new GeoCoordinate(geoPoint.Latitude, geoPoint.Longitude);
        }

        public static GeoPoint ToGeoPoint(this GeoCoordinate geoCoordinate)
        {
            return new GeoPoint {Latitude = geoCoordinate.Latitude, Longitude = geoCoordinate.Longitude};
        }

        public static GeoPoint ToGeoPoint(this GeoAddress geoAddress)
        {
            return new GeoPoint(geoAddress.Coordinates.Latitude, geoAddress.Coordinates.Longitude);
        }
    }
}