﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Events;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.DoubleGis;
using BoilingPoint.Portable.Services;
using BoilingPoint.WindowsPhone.Models;
using BoilingPoint.WindowsPhone.Services;
using Caliburn.Micro;
using Telerik.Windows.Controls;
using DataService = BoilingPoint.Portable.Services.DataService;
using ITwitterService = BoilingPoint.WindowsPhone.Services.ITwitterService;

namespace BoilingPoint.WindowsPhone.ViewModels
{
    public abstract class MapViewModel : ViewModelBase
    {
        #region Fields

        private const int SEARCH_RADIUS_IN_METERS = 1000;

        protected readonly IAnonymousTwitterService _anonymousTwitterService;
        protected readonly IDataService _dataService;
        private readonly DispatcherTimer _timer = new DispatcherTimer();
        protected readonly ITwitterService _twitterService;
        protected bool _isTweetsLoading;
        protected LocationPushpinModel _locationPushpinModel;

        protected DataTemplate _profilePushpinTemplate;
        protected BindableCollection<PushpinModelBase> _pushpins;
        protected TweetModel _selectedTweet;
        protected BindableCollection<TweetGrouping> _tweetGroupings;
        protected DataTemplate _tweetPushpinTemplate;
        protected BindableCollection<TweetModel> _tweets;

        #endregion

        #region Ctors

        protected MapViewModel(
            INavigationService navigationService,
            IGeolocationService geolocationService,
            IAnonymousTwitterService anonymousTwitterService,
            ITwitterService twitterService,
            ISettingsService settingsService,
            IDataService dataService)
            : base(navigationService, settingsService, geolocationService)
        {
            _anonymousTwitterService = anonymousTwitterService;
            _anonymousTwitterService.TweetsAdded += OnAnonymousTwitterServiceTweetsAdded;
            _twitterService = twitterService;
            _dataService = dataService;

            _tweets = new BindableCollection<TweetModel>();
            _tweetGroupings = new BindableCollection<TweetGrouping>();
            _pushpins = new BindableCollection<PushpinModelBase>();
        }

        #endregion

        #region Properties

        public BindableCollection<TweetGrouping> TweetGroupings
        {
            get { return _tweetGroupings; }
        }

        public BindableCollection<TweetModel> Tweets
        {
            get { return _tweets; }
            set
            {
                if (Equals(value, _tweets)) return;
                _tweets = value;
                NotifyOfPropertyChange(() => Tweets);
            }
        }

        public BindableCollection<PushpinModelBase> Pushpins
        {
            get { return _pushpins; }
            set
            {
                if (Equals(value, _pushpins)) return;
                _pushpins = value;
                NotifyOfPropertyChange(() => Pushpins);
            }
        }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public ulong TweetID { get; set; }
        public string ProfileId { get; set; }

        #endregion

        #region Methods

        protected abstract void SetSelectedTweet(TweetModel tweet);

        protected abstract TweetModel GetSelectedTweet();

        protected void OnAnonymousTwitterServiceTweetsAdded(object sender, TweetsAddedEventArgs e)
        {
            foreach (Tweet tweet in e.Tweets)
            {
                Tweets.Add(new TweetModel(tweet));

                TweetPushpinModel pushpinModel = GetPushpinModel(tweet);
                if (pushpinModel != null)
                {
                    pushpinModel.AddTweetToGrouping(tweet);
                }
                else
                {
                    if (!Equals(tweet.Latitude, 0d) && !Equals(tweet.Longitude, 0d))
                    {
                        var tweetGrouping = new TweetGrouping(DataService.DEFAULT_RADIUS_IN_METERS) { tweet };
                        TweetGroupings.Add(tweetGrouping);

                        pushpinModel = new TweetPushpinModel(tweetGrouping);
                        Pushpins.Add(pushpinModel);

                        AddPushpins(new[] { tweetGrouping });        
                    }
                }
            }
        }

        protected TweetPushpinModel GetPushpinModel(Tweet tweet)
        {
            for (int i = 0; i < Pushpins.Count; i++)
            {
                PushpinModelBase pushpinModel = Pushpins[i];
                if (!(pushpinModel is TweetPushpinModel)) continue;
                TweetGrouping grouping = ((TweetPushpinModel) pushpinModel).TweetGrouping;
                if (grouping.CanAdd(tweet))
                {
                    return (TweetPushpinModel) pushpinModel;
                }
            }

            return null;
        }

        protected async Task LoadProfilesAsync()
        {
            if (IsGeolocationEnabled)
            {
                GeoPoint geoPoint = await GeolocationService.GetGeoPointAsync();
                double latitude = geoPoint.Latitude;
                double longitude = geoPoint.Longitude;

                var profilePushpins = new List<ProfilePushpinModel>();
                List<Profile> profiles =
                    await _dataService.SearchProfilesAsync(latitude, longitude, SEARCH_RADIUS_IN_METERS);
                foreach (Profile profile in profiles)
                {
                    var pushpinModel = new ProfilePushpinModel(profile);
                    if (Equals(profile.Id, ProfileId))
                    {
                        pushpinModel.ContentVisibility = Visibility.Visible;
                    }

                    AddPushpin(pushpinModel, _profilePushpinTemplate);

                    profilePushpins.Add(pushpinModel);
                    Pushpins.Add(pushpinModel);
                }

                List<Bank> banks = await _dataService.SearchBanksAsync(profiles);

                List<Profile> profilesWithImages = await GetProfilesWithImagesAsync(banks);
                foreach (Profile profile in profilesWithImages)
                {
                    ProfilePushpinModel pushpinModel =
                        profilePushpins.First(x => x.Id == profile.Id);
                    if (pushpinModel != null)
                    {
                        pushpinModel.ImageUri = profile.ImageUri;
                    }
                }
            }
        }

        protected async Task<List<Profile>> GetProfilesWithImagesAsync(List<Bank> banks)
        {
            var profilesWithImages = new List<Profile>();
            var userScreenNames = new List<string>();
            if (banks != null)
            {
                foreach (Bank bank in banks.Where(x => !string.IsNullOrEmpty(x.TwitterUsername) &&
                                                       !userScreenNames.Contains(x.TwitterUsername)))
                {
                    userScreenNames.Add(bank.TwitterUsername);
                }

                List<User> users = await _anonymousTwitterService.GetUsersAsync(string.Join(",", userScreenNames));

                foreach (User user in users)
                {
                    string fileName = Path.GetFileName(user.ProfileImageUrl);
                    if (fileName == null || fileName.StartsWith("default_profile")) continue;
                    Bank bank = banks.First(x => Equals(x.TwitterUsername, user.ScreenName));
                    Profile profile = bank.Profile;
                    profile.ImageUri = new Uri(user.ProfileImageUrl);
                    profilesWithImages.Add(profile);
                }
            }

            return profilesWithImages;
        }

        protected void ReloadTweets()
        {
            Tweets.IsNotifying = false;

            Tweets.Clear();
            Tweets.AddRange(_anonymousTwitterService.Tweets.Select(x => new TweetModel(x)));

            Tweets.IsNotifying = true;
            Tweets.Refresh();
        }

        protected void LoadTweets()
        {
            _isTweetsLoading = true;

            Tweets.AddRange(_anonymousTwitterService.Tweets.Select(x => new TweetModel(x)));

            _tweetGroupings =
                new BindableCollection<TweetGrouping>(
                    _twitterService.GroupTweetsByDistance(
                        _anonymousTwitterService.Tweets.Where(
                            tweet => !Equals(tweet.Latitude, 0d) && !Equals(tweet.Longitude, 0d))));

            AddPushpins(_tweetGroupings);

            _isTweetsLoading = false;
        }

        private void AddPushpins(IEnumerable<TweetGrouping> tweetGroupings)
        {
            IEnumerable<TweetPushpinModel> pushpins =
                tweetGroupings.Select(tweetGrouping => new TweetPushpinModel(tweetGrouping)).ToList();

            foreach (TweetPushpinModel pushpinModel in pushpins)
            {
                if (pushpinModel.TweetGrouping.FirstOrDefault(x => x.ID == TweetID) != null)
                {
                    pushpinModel.ContentVisibility = Visibility.Visible;
                }

                AddPushpin(pushpinModel, _tweetPushpinTemplate);
            }

            Pushpins.Clear();
            Pushpins.AddRange(pushpins.Cast<PushpinModelBase>());
        }

        protected override async void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);

            _tweetPushpinTemplate = (DataTemplate) Application.Current.Resources["TweetPushpinTemplate"];
            _profilePushpinTemplate = (DataTemplate) Application.Current.Resources["ProfilePushpinTemplate"];

            if (!Equals(Latitude, 0d) && !Equals(Longitude, 0d))
            {
                JumpToPosition(Latitude, Longitude);
            }
            else
            {
                if (IsGeolocationEnabled)
                {
                    GeoPoint geoPoint = await GeolocationService.GetGeoPointAsync();

                    JumpToPosition(geoPoint);
                }
            }

            LoadTweets();

            _timer.Interval = TimeSpan.FromSeconds(60);
            _timer.Tick += OnTimerTick;
            _timer.Start();

            if (TweetID != 0UL)
            {
                TweetModel tweet = Tweets.FirstOrDefault(x => x.ID == TweetID);
                if (tweet != null)
                {
                    SetSelectedTweet(tweet);
                }
            }

            await LoadProfilesAsync();

            if (TweetID == 0UL && (!Equals(Latitude, 0d) && !Equals(Longitude, 0d)))
            {
                JumpToPosition(Latitude, Longitude);
            }
        }

        private void OnTimerTick(object sender, EventArgs e)
        {
            foreach (TweetModel tweetModel in Tweets)
            {
                tweetModel.UpdateCreatedAtTimeago();
            }
        }

        protected abstract void AddImagePushpin(PushpinModelBase pushpinModel, BitmapImage bitmapImage);

        protected abstract void AddPushpin(PushpinModelBase pushpinModel, DataTemplate template);

        /// <summary>
        ///     Zoom in to closest integer zoom level.
        /// </summary>
        public abstract void ZoomIn();

        /// <summary>
        ///     Zoom out to closest integer zoom level.
        /// </summary>
        public abstract void ZoomOut();

        protected virtual void SetLocation(PushpinModelBase pushpinModel, double latitude, double longitude)
        {
            pushpinModel.Location = new GeoCoordinate(latitude, longitude);
        }

        /// <summary>
        ///     Jump to user's location.
        /// </summary>
        /// <remarks>Map's UseLocation property should be true.</remarks>
        public virtual async Task<GeoPoint> FindMe()
        {
            GeoPoint geoPoint = null;
            if (IsGeolocationEnabled)
            {
                geoPoint = await GeolocationService.GetGeoPointAsync();

                TweetModel nearestTweet = FindNearestTweet(geoPoint);
                if (nearestTweet != null)
                {
                    SetSelectedTweet(nearestTweet);
                }

                JumpToPosition(geoPoint);

                UpdateLocationMarker(geoPoint);
            }
            else
            {
                switch (GeolocationService.Status)
                {
                    case GeolocationStatus.Disabled:
                        MessageBox.Show(AppResources.GeolocationStatusDisabled, "Где я?", MessageBoxButton.OK);
                        break;

                    case GeolocationStatus.NoData:
                        MessageBox.Show(AppResources.GeolocationStatusNoData, "Где я?", MessageBoxButton.OK);
                        break;

                    default:
                        if (!SettingsService.GeolocationEnabled)
                        {
                            RadMessageBox.Show(new[] {"да", "нет"}, "Где я?",
                                               "Для определения местоположения необходимо разрешить приложению использовать службу геолокации. " +
                                               "Разрешить использование службы?",
                                               null, false, false, HorizontalAlignment.Stretch,
                                               VerticalAlignment.Top,
                                               async delegate(MessageBoxClosedEventArgs args)
                                                         {
                                                             if (args.ButtonIndex != 0) return;
                                                             SettingsService.GeolocationEnabled = true;
                                                             await FindMe();
                                                         });
                        }
                        break;
                }
            }
            return geoPoint;
        }

        private TweetModel FindNearestTweet(GeoPoint geoPoint)
        {
            foreach (TweetGrouping tweetGrouping in TweetGroupings)
            {
                if (!tweetGrouping.CanAdd(geoPoint.Latitude, geoPoint.Longitude))
                    continue;

                Tweet tweet = tweetGrouping.OrderByDescending(x => x.CreatedAt).First();
                return Tweets.FirstOrDefault(x => x.ID == tweet.ID);
            }

            return null;
        }

        private void UpdateLocationMarker(GeoPoint geoPoint)
        {
            if (_locationPushpinModel == null)
            {
                _locationPushpinModel =
                    new LocationPushpinModel
                        {
                            Location = new GeoCoordinate(geoPoint.Latitude, geoPoint.Longitude)
                        };

                Stream stream =
                    Application.GetResourceStream(new Uri(@"Assets/LocationMarker.png", UriKind.Relative)).Stream;

                var bitmapImage = new BitmapImage();
                bitmapImage.SetSource(stream);

                AddImagePushpin(_locationPushpinModel, bitmapImage);
            }
            else
            {
                SetLocation(_locationPushpinModel, geoPoint.Latitude, geoPoint.Longitude);
            }
        }

        protected abstract void JumpToPosition(GeoPoint geoPoint);

        protected abstract void JumpToPosition(double latitude, double longitude);

        #endregion
    }
}