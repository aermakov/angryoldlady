﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using BoilingPoint.Portable;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Services;
using BoilingPoint.WindowsPhone.Services;
using Caliburn.Micro;
using Microsoft.Phone.Shell;

namespace BoilingPoint.WindowsPhone.ViewModels
{
    public abstract class ViewModelBase : Screen
    {
        #region Fields

        protected readonly IGeolocationService GeolocationService;
        private readonly IAnalyticalService _analyticalService;
        protected readonly INavigationService NavigationService;
        protected readonly ISettingsService SettingsService;

        #endregion

        #region Ctors

        protected ViewModelBase(INavigationService navigationService)
        {
            NavigationService = navigationService;
        }

        protected ViewModelBase(INavigationService navigationService,
            ISettingsService settingsService,
            IGeolocationService geolocationService,
            IAnalyticalService analyticalService = null)
            : this(navigationService)
        {
            SettingsService = settingsService;
            GeolocationService = geolocationService;
            _analyticalService = analyticalService;
        }

        #endregion

        #region Properties

        protected Dispatcher Dispatcher
        {
            get { return ((Page) GetView()).Dispatcher; }
        }

        public bool CanGoBack
        {
            get { return NavigationService.CanGoBack; }
        }

        protected bool IsGeolocationEnabled
        {
            get { return SettingsService.GeolocationEnabled && GeolocationService.Status == GeolocationStatus.Ready; }
        }

        protected async Task<bool> GetIsNetworkAvailableAsync()
        {
            return await Utils.GetIsNetworkAvailableAsync();
        }

        #endregion

        #region Methods

        protected T FindName<T>(string name)
        {
            var view = (FrameworkElement) GetView();
            if (view != null) return (T) view.FindName(name);
            return default(T);
        }

        public void GoBack()
        {
            NavigationService.GoBack();
        }

        protected void ShowSystemTray(string text)
        {
            if (SystemTray.ProgressIndicator != null)
            {
                SystemTray.ProgressIndicator.IsVisible = true;
                SystemTray.ProgressIndicator.Text = text;
            }
        }

        protected void HideSystemTray()
        {
            if (SystemTray.ProgressIndicator != null) SystemTray.ProgressIndicator.IsVisible = false;
        }

        protected override void OnViewLoaded(object view)
        {
            Type type = GetType();
            if (_analyticalService != null)
            {
                string viewName = type.Name.Replace("ViewModel", " View");
                _analyticalService.ReportEvent(string.Format("{0} Loaded", viewName));
            }

            base.OnViewLoaded(view);
        }

        #endregion
    }
}