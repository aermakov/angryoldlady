﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Events;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.DoubleGis;
using BoilingPoint.Portable.Models;
using BoilingPoint.Portable.Services;
using BoilingPoint.WindowsPhone.Extensions;
using BoilingPoint.WindowsPhone.Services;
using Caliburn.Micro;
using Coding4Fun.Toolkit.Controls;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Telerik.Windows.Controls;
using ITwitterService = BoilingPoint.WindowsPhone.Services.ITwitterService;

namespace BoilingPoint.WindowsPhone.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        #region Fields

        private const string QUERY = "#ihatetowait";

        private const int SEARCH_RADIUS_IN_METERS = 1000;

        private readonly IAnonymousTwitterService _anonymousTwitterService;
        private readonly IDataService _dataService;
        private readonly IGeocodeService _geocodeService;
        private readonly DispatcherTimer _timer = new DispatcherTimer();
        private readonly ITwitterService _twitterService;

        private Bank _bank;
        private string _bankId;
        private string _bankTitle;
        private int _cashBoxAvailable;
        private int _cashBoxClosed;
        private string _comment = string.Empty;
        private string _countryCode;
        private bool _isProfilesLoading;
        private bool _isSearching;
        private bool _isTweetsLoading;
        private double _latitude;
        private double _longitude;
        private bool _mapButtonVisible;
        private Uri _mapIconUri;
        private int _peopleInline;
        private BindableCollection<ProfileModel> _profiles;
        private string _profilesLoadingErrorMessage;
        private Uri _refreshIconUri;
        private bool _sendTweetButtonVisible;
        private Uri _sendTweetIconUri;
        private BindableCollection<TagModel> _tags;
        private Tweet _tweet;
        private BindableCollection<TweetModel> _tweets;
        private string _tweetsLoadingErrorMessage;
        private TimeSpan _waitTime;
        private WebServiceAutoCompleteProvider _webServiceProvider;

        #endregion

        #region Ctors

        public MainViewModel(INavigationService navigationService,
                             IDataService dataService,
                             IGeocodeService geocodeService,
                             IGeolocationService geolocationService,
                             IAnonymousTwitterService anonymousTwitterService,
                             ITwitterService twitterService,
                             ISettingsService settingsService)
            : base(navigationService, settingsService, geolocationService)
        {
            _dataService = dataService;
            _geocodeService = geocodeService;
            _anonymousTwitterService = anonymousTwitterService;
            _twitterService = twitterService;

            _tweets = new BindableCollection<TweetModel>();
            _tags = new BindableCollection<TagModel>();
            _profiles = new BindableCollection<ProfileModel>();

            if (!GeolocationService.Started)
            {
                GeolocationService.Start();
            }

            GeolocationService.StatusChanged += OnGeolocationServiceStatusChanged;

#if DEBUG
            LatitudeVisible = true;
            LongitudeVisible = true;
#endif
        }

        #endregion

        #region Properties

        public TimeSpan MaxWaitTime
        {
            get { return TimeSpan.FromHours(8.0); }
        }

        public string Comment
        {
            get { return _comment; }
            set
            {
                if (value == _comment) return;
                _comment = value;
                NotifyOfPropertyChange(() => Comment);
                UpdateTweet();
            }
        }

        public double Latitude
        {
            get { return _latitude; }
            set
            {
                if (value.Equals(_latitude)) return;
                _latitude = value;
                NotifyOfPropertyChange(() => Latitude);
            }
        }

        public bool LatitudeVisible { get; set; }

        public double Longitude
        {
            get { return _longitude; }
            set
            {
                if (value.Equals(_longitude)) return;
                _longitude = value;
                NotifyOfPropertyChange(() => Longitude);
            }
        }

        public bool LongitudeVisible { get; set; }

        public int PeopleInline
        {
            get { return _peopleInline; }
            set
            {
                if (value == _peopleInline) return;
                _peopleInline = value;
                NotifyOfPropertyChange(() => PeopleInline);
                UpdateTweet();
            }
        }

        public TimeSpan WaitTime
        {
            get { return _waitTime; }
            set
            {
                if (value == _waitTime) return;
                _waitTime = value;
                NotifyOfPropertyChange(() => WaitTime);
                UpdateTweet();
            }
        }

        public int CashBoxAvailable
        {
            get { return _cashBoxAvailable; }
            set
            {
                if (value == _cashBoxAvailable) return;
                _cashBoxAvailable = value;
                NotifyOfPropertyChange(() => CashBoxAvailable);
                UpdateTweet();
            }
        }

        public int CashBoxClosed
        {
            get { return _cashBoxClosed; }
            set
            {
                if (value == _cashBoxClosed) return;
                _cashBoxClosed = value;
                NotifyOfPropertyChange(() => CashBoxClosed);
                UpdateTweet();
            }
        }

        public string BankTitle
        {
            get { return _bankTitle; }
            set
            {
                if (value == _bankTitle) return;
                _bankTitle = value;
                NotifyOfPropertyChange(() => BankTitle);
                UpdateTweet();
            }
        }

        public string BankId
        {
            get { return _bankId; }
            set
            {
                if (value == _bankId) return;
                _bankId = value;
                NotifyOfPropertyChange(() => BankId);
            }
        }

        public Bank Bank
        {
            get { return _bank; }
            set
            {
                if (Equals(value, _bank)) return;
                _bank = value;
                if (_bank != null)
                    BankId = _bank.objectId;
                NotifyOfPropertyChange(() => Bank);
                UpdateTweet();
            }
        }

        public bool DoubleGisHyperlinkVisible
        {
            get { return Profiles.Any(); }
        }

        public bool SendTweetButtonVisible
        {
            get { return _sendTweetButtonVisible; }
            set
            {
                if (value.Equals(_sendTweetButtonVisible)) return;
                _sendTweetButtonVisible = value;
                NotifyOfPropertyChange(() => SendTweetButtonVisible);
            }
        }

        public bool MapButtonVisible
        {
            get { return _mapButtonVisible; }
            set
            {
                if (value.Equals(_mapButtonVisible)) return;
                _mapButtonVisible = value;
                NotifyOfPropertyChange(() => MapButtonVisible);
            }
        }

        public Uri SendTweetIconUri
        {
            get { return _sendTweetIconUri; }
            set
            {
                if (Equals(value, _sendTweetIconUri)) return;
                _sendTweetIconUri = value;
                NotifyOfPropertyChange(() => SendTweetIconUri);
            }
        }

        public Uri MapIconUri
        {
            get { return _mapIconUri; }
            set
            {
                if (Equals(value, _mapIconUri)) return;
                _mapIconUri = value;
                NotifyOfPropertyChange(() => MapIconUri);
            }
        }

        public Uri RefreshIconUri
        {
            get { return _refreshIconUri; }
            set
            {
                if (Equals(value, _refreshIconUri)) return;
                _refreshIconUri = value;
                NotifyOfPropertyChange(() => RefreshIconUri);
            }
        }

        public BindableCollection<TweetModel> Tweets
        {
            get { return _tweets; }
            set
            {
                if (Equals(value, _tweets)) return;
                _tweets = value;
                NotifyOfPropertyChange(() => Tweets);
            }
        }

        public BindableCollection<ProfileModel> Profiles
        {
            get { return _profiles; }
            set
            {
                if (Equals(value, _profiles)) return;
                _profiles = value;
                NotifyOfPropertyChange(() => Profiles);
                NotifyOfPropertyChange(() => DoubleGisHyperlinkVisible);
            }
        }


        public BindableCollection<TagModel> Tags
        {
            get { return _tags; }
            set
            {
                if (Equals(value, _tags)) return;
                _tags = value;
                NotifyOfPropertyChange(() => Tags);
            }
        }

        private RadBusyIndicator ProfilesLoadingBusyIndicator
        {
            get { return FindName<RadBusyIndicator>("ProfilesLoadingBusyIndicator"); }
        }

        private RadBusyIndicator TweetsLoadingBusyIndicator
        {
            get { return FindName<RadBusyIndicator>("TweetsLoadingBusyIndicator"); }
        }

        private RadAutoCompleteBox BankAutoCompleteBox
        {
            get { return FindName<RadAutoCompleteBox>("BankAutoCompleteBox"); }
        }

        private RadListPicker TagsListPicker
        {
            get { return FindName<RadListPicker>("TagsListPicker"); }
        }

        private IEnumerable<TagModel> SelectedTags
        {
            get
            {
                if (TagsListPicker != null)
                    return TagsListPicker.SelectedItems.Cast<TagModel>();
                return new List<TagModel>();
            }
        }

        public Tweet Tweet
        {
            get { return _tweet; }
            set
            {
                if (value == _tweet) return;
                _tweet = value;
                NotifyOfPropertyChange(() => Tweet);
            }
        }

        public bool IsProfilesLoading
        {
            get { return _isProfilesLoading; }
            set
            {
                if (value.Equals(_isProfilesLoading)) return;
                _isProfilesLoading = value;
                NotifyOfPropertyChange(() => IsProfilesLoading);
            }
        }

        public bool IsTweetsLoading
        {
            get { return _isTweetsLoading; }
            set
            {
                if (value.Equals(_isTweetsLoading)) return;
                _isTweetsLoading = value;
                NotifyOfPropertyChange(() => IsTweetsLoading);
            }
        }

        public string TweetsLoadingErrorMessage
        {
            get { return _tweetsLoadingErrorMessage; }
            set
            {
                if (value == _tweetsLoadingErrorMessage) return;
                _tweetsLoadingErrorMessage = value;
                NotifyOfPropertyChange(() => TweetsLoadingErrorMessage);
                NotifyOfPropertyChange(() => IsTweetsLoadingErrorMessageVisible);
            }
        }

        public bool IsTweetsLoadingErrorMessageVisible
        {
            get { return !string.IsNullOrEmpty(TweetsLoadingErrorMessage); }
        }

        public string ProfilesLoadingErrorMessage
        {
            get { return _profilesLoadingErrorMessage; }
            set
            {
                if (value == _profilesLoadingErrorMessage) return;
                _profilesLoadingErrorMessage = value;
                NotifyOfPropertyChange(() => ProfilesLoadingErrorMessage);
                NotifyOfPropertyChange(() => IsProfilesLoadingErrorMessageVisible);
            }
        }

        public bool IsProfilesLoadingErrorMessageVisible
        {
            get { return !string.IsNullOrEmpty(ProfilesLoadingErrorMessage); }
        }

        #endregion

        #region Methods

        public async void OnTweetsRefreshRequested(RadDataBoundListBox dataBoundListBox)
        {
            await RefreshAsync();

            dataBoundListBox.StopPullToRefreshLoading(true);
        }

        private async void OnTimerTick(object sender, EventArgs e)
        {
            foreach (TweetModel tweetModel in Tweets)
            {
                tweetModel.UpdateCreatedAtTimeago();
            }

            await RefreshAsync();
        }

        private async void OnGeolocationServiceStatusChanged(object sender, GeolocationStatusChangedEventArgs e)
        {
            switch (e.Status)
            {
                case GeolocationStatus.Ready:
                    {
                        if (Bank == null)
                        {
                            var banks = new List<Bank>();
                            if (Equals(Latitude, 0d) && Equals(Longitude, 0d))
                            {
                                GeoPoint geoPoint = null;
                                if (IsGeolocationEnabled)
                                {
                                    geoPoint = await GeolocationService.GetGeoPointAsync();
                                }

                                if (geoPoint != null)
                                {
                                    Latitude = geoPoint.Latitude;
                                    Longitude = geoPoint.Longitude;

                                    if (await GetIsNetworkAvailableAsync())
                                    {
                                        banks = await _dataService.SearchBanksAsync(Latitude, Longitude);
                                    }
                                }
                            }

                            Bank = banks.FirstOrDefault();
                            if (Bank != null)
                            {
                                BankTitle = Bank.Title;
                            }
                        }

                        await LoadProfilesAsync();
                    }
                    break;
            }
        }

        public void OnTagsListPickerSelectionChanged(SelectionChangedEventArgs e)
        {
            UpdateTweet();
        }

        public void OnBankAutoCompleteBoxSuggestionSelected(SuggestionSelectedEventArgs e)
        {
            if (e.SelectedSuggestion is BankModel)
            {
                Bank = ((BankModel) e.SelectedSuggestion).Bank;
            }
        }

        public void OnBankAutoCompleteBoxTextChanged(TextChangedEventArgs e)
        {
            if (BankAutoCompleteBox == null) return;
            string title = BankAutoCompleteBox.Text;
            if (Bank != null && !string.Equals(title, Bank.Title, StringComparison.OrdinalIgnoreCase))
            {
                Bank = null;
            }
        }

        public void OnDoubleGisHyperlinkButtonTap()
        {
            var webBrowserTask = new WebBrowserTask {Uri = new Uri("http://api.2gis.ru/", UriKind.Absolute)};
            webBrowserTask.Show();
        }

        private async void OnWebServiceProviderInputChanged(object sender, EventArgs e)
        {
            if (await GetIsNetworkAvailableAsync())
            {
                ShowSystemTray("Поиск банков...");

                string title = _webServiceProvider.InputString;

                var banks = new List<Bank>();
                if (title.Length > 2)
                {
                    banks = await _dataService.SearchBanksByTitleAsync(title);
                }

                _webServiceProvider.LoadSuggestions(banks.Select(bank => new BankModel(bank)));

                HideSystemTray();
            }
        }

        public void OnPivotSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (((Pivot) sender).SelectedIndex)
            {
                case 0:
                    ShowBoilingAppBar();
                    break;

                case 1:
                    ShowBoiledAppBar();
                    break;
            }
        }

        private void Clear()
        {
            PeopleInline = 0;
            WaitTime = TimeSpan.Zero;
            CashBoxAvailable = 0;
            CashBoxClosed = 0;
            Comment = string.Empty;

            if (TagsListPicker != null)
            {
                TagsListPicker.SelectedItems.Clear();
            }

            UpdateTweet();
        }

        private Tweet GetTweet()
        {
            Bank bank = Bank;
            if (BankAutoCompleteBox != null && (bank == null && !string.IsNullOrEmpty(BankAutoCompleteBox.Text)))
            {
                bank = new Bank {Title = BankAutoCompleteBox.Text.Trim()};
            }

            var tweet =
                new Tweet
                    {
                        Bank = bank,
                        PeopleInline = PeopleInline,
                        WaitTime = WaitTime.TotalMinutes,
                        CashBoxAvailable = CashBoxAvailable,
                        CashBoxClosed = CashBoxClosed,
                        Comment = Comment
                    };

            tweet.Tags.AddRange(SelectedTags.Select(x => x.Name.Substring(1)));

            return tweet;
        }

        private async Task InitializeDoubleGisAsync()
        {
            if (await GetIsNetworkAvailableAsync())
            {
                Config config = await _dataService.GetConfigAsync();

                if (config.DoubleGisApiKey != null)
                {
                    DoubleGis.Initialize(config.DoubleGisApiKey);
                }
            }
        }

        protected override async void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);

            var progressIndicator = new ProgressIndicator {IsIndeterminate = true};
            SystemTray.SetProgressIndicator((DependencyObject) view, progressIndicator);

            ShowBoilingAppBar();

            SendTweetIconUri = new Uri("/Assets/AppBar/appbar.tweet.png", UriKind.Relative);
            MapIconUri = new Uri("/Assets/AppBar/appbar.map.png", UriKind.Relative);
            RefreshIconUri = new Uri("/Assets/AppBar/appbar.refresh.png", UriKind.Relative);

            _webServiceProvider = new WebServiceAutoCompleteProvider();
            _webServiceProvider.InputChanged += OnWebServiceProviderInputChanged;
            
            BankAutoCompleteBox.InitSuggestionsProvider(_webServiceProvider);

            _timer.Interval = TimeSpan.FromSeconds(60);
            _timer.Tick += OnTimerTick;
            _timer.Start();

            if (BankId != null)
            {
                if (await GetIsNetworkAvailableAsync())
                {
                    Bank = await _dataService.GetBankByIdAsync(BankId);
                }
            }

            await InitializeDoubleGisAsync();

            UpdateTweet();

            IsTweetsLoading = true;

            IsProfilesLoading = true;

            await LoadTagsAsync();

            await LoadTweetsAsync();
        }

        private async Task LoadTweetsAsync()
        {
            if (await GetIsNetworkAvailableAsync())
            {
                List<Tweet> tweets = await SearchTweetsAsync();
                if (tweets.Count == 0)
                {
                    ShowTweetsLoadingErrorMessage(AppResources.NoTweets);
                }
                else
                {
                    HideTweetsLoadingErrorMessage();

                    InsertTweets(tweets);
                }

                HideTweetsLoadingIndicator();

                await UpdateProfileImageUrlsAsync(tweets);
            }
            else
            {
                HideTweetsLoadingIndicator();

                ShowTweetsLoadingErrorMessage(AppResources.NetworkUnavailable);
            }
        }

        private void HideTweetsLoadingIndicator()
        {
            IsTweetsLoading = false;
            TweetsLoadingBusyIndicator.Visibility = Visibility.Collapsed;
        }

        private void ShowTweetsLoadingErrorMessage(string errorMessage)
        {
            TweetsLoadingErrorMessage = errorMessage;
        }

        private void HideTweetsLoadingErrorMessage()
        {
            TweetsLoadingErrorMessage = string.Empty;
        }

        private void ShowProfilesLoadingErrorMessage(string errorMessage)
        {
            ProfilesLoadingErrorMessage = errorMessage;
        }

        private void HideProfilesLoadingErrorMessage()
        {
            ProfilesLoadingErrorMessage = string.Empty;
        }

        private async Task LoadProfilesAsync(int radiusInMeters = SEARCH_RADIUS_IN_METERS)
        {
            if (IsGeolocationEnabled)
            {
                GeoPoint geoPoint = await GeolocationService.GetGeoPointAsync();

                double latitude = geoPoint.Latitude;
                double longitude = geoPoint.Longitude;

                if (await GetIsNetworkAvailableAsync())
                {
                    List<Profile> profiles;
                    try
                    {
                        profiles = (await _dataService.SearchProfilesAsync(latitude, longitude, radiusInMeters))
                            .OrderBy(x => x.Distance)
                            .ToList();
                    }
                    catch (Exception)
                    {
                        profiles = new List<Profile>();
                    }

                    if (profiles.Count == 0)
                    {
                        ShowProfilesLoadingErrorMessage(AppResources.NoProfiles);
                    }
                    else
                    {
                        foreach (Profile profile in profiles)
                        {
                            ProfileModel profileModel = Profiles.FirstOrDefault(x => Equals(x.Id, profile.Id));
                            if (profileModel != null || profile.Contacts.Count <= 0) continue;

                            profileModel = new ProfileModel(profile);
                            profileModel.ShowOnMapTap += OnProfileModelShowOnMapTap;
                            profileModel.ShowTweetsInBankTap += OnProfileModelShowTweetsInBankTap;
                            profileModel.ShowTweetsInFilialTap += OnProfileModelShowTweetsInFilialTap;

                            Profiles.Add(profileModel);
                        }

                        HideProfilesLoadingErrorMessage();
                    }
                }
                else
                {
                    ShowProfilesLoadingErrorMessage(AppResources.NetworkUnavailable);
                }
            }

            IsProfilesLoading = false;
            ProfilesLoadingBusyIndicator.Visibility = Visibility.Collapsed;
        }

        private async Task LoadTagsAsync()
        {
            List<Tag> tags;
            if (await GetIsNetworkAvailableAsync())
            {
                tags = await _dataService.GetPredefinedTagsAsync();
            }
            else
            {
                tags = new List<Tag>
                           {
                               new Tag {Name = "angryoldlady"},
                               new Tag {Name = "nochairs"},
                               new Tag {Name = "retardedteller"},
                               new Tag {Name = "ussrstyle"}
                           };
            }

            Tags = new BindableCollection<TagModel>(tags.Select(tag => new TagModel(tag)).OrderBy(x => x.Name.Length));
        }

        private void UpdateTweet()
        {
            Tweet = GetTweet();
        }

        private async Task<List<Tweet>> SearchTweetsAsync(ulong sinceID = 0, ulong maxID = 0)
        {
            var tweets = new List<Tweet>();
            try
            {
                if (!_isSearching)
                {
                    _isSearching = true;

                    if (await GetIsNetworkAvailableAsync())
                    {
                        tweets = await _anonymousTwitterService.SearchAsync(QUERY, 0, 0, 0, sinceID, maxID);
                    }

                    _isSearching = false;
                }
            }
            catch (Exception)
            {
                tweets = new List<Tweet>();
            }
            return tweets;
        }

        private void InsertTweets(List<Tweet> tweets, ulong sinceID = 0, ulong maxID = 0)
        {
            if (sinceID != 0 || (sinceID == 0 && maxID == 0))
            {
                for (int i = tweets.Count - 1; i >= 0; i--)
                {
                    Tweet tweet = tweets[i];
                    InsertTweet(tweet);
                }
            }
            else
            {
                tweets = tweets.Where(tweet => Convert.ToUInt64(tweet.ID) != maxID).ToList();
                if (tweets.Count > 0)
                {
                    var tweetModels = new List<TweetModel>();
                    foreach (Tweet tweet in tweets)
                    {
                        var tweetModel = new TweetModel(tweet);
                        if (!Tweets.Contains(tweetModel))
                        {
                            SubscribeToEvents(tweetModel);

                            tweetModels.Add(tweetModel);
                        }
                    }

                    Tweets.AddRange(tweetModels);
                }
            }
        }

        private void SubscribeToEvents(TweetModel tweetModel)
        {
            tweetModel.ContextMenuOpening += OnTweetModelContextMenuOpening;
            tweetModel.ShowOnMapTap += OnTweetModelShowOnMapTap;
            tweetModel.ShowTweetsInFilialTap += OnTweetModelShowTweetsInFilialTap;
            tweetModel.ShowTweetsInBankTap += OnTweetModelShowTweetsInBankTap;
        }

        private void InsertTweet(Tweet tweet)
        {
            var tweetModel = new TweetModel(tweet);
            if (!Tweets.Contains(tweetModel))
            {
                SubscribeToEvents(tweetModel);

                Tweets.Insert(0, tweetModel);
            }
        }

        private void OnProfileModelShowTweetsInFilialTap(object sender, EventArgs e)
        {
            var profileModel = (ProfileModel) sender;

            NavigationService.UriFor<ProfileViewModel>()
                             .WithParam(x => x.ProfileId, profileModel.Id)
                             .Navigate();
        }

        private void OnProfileModelShowTweetsInBankTap(object sender, EventArgs e)
        {
            var profileModel = (ProfileModel) sender;

            NavigationService.UriFor<BankViewModel>().WithParam(x => x.BankId, profileModel.BankId).Navigate();
        }

        private void OnProfileModelShowOnMapTap(object sender, EventArgs e)
        {
            var profileModel = (ProfileModel) sender;

            NavigationService.NavigateToMapView(profileModel.Latitude,
                                                profileModel.Longitude,
                                                profileModel.Id,
                                                tweetID: 0ul);
        }

        private void OnTweetModelShowTweetsInBankTap(object sender, EventArgs e)
        {
            var tweetModel = (TweetModel) sender;

            NavigationService.UriFor<BankViewModel>().WithParam(x => x.BankId, tweetModel.BankId).Navigate();
        }

        private void OnTweetModelShowTweetsInFilialTap(object sender, EventArgs e)
        {
            var tweetModel = (TweetModel) sender;

            NavigationService.UriFor<ProfileViewModel>()
                             .WithParam(x => x.ProfileId, tweetModel.ProfileId)
                             .WithParam(x => x.Latitude, tweetModel.Latitude)
                             .WithParam(x => x.Longitude, tweetModel.Longitude)
                             .Navigate();
        }

        private void OnTweetModelContextMenuOpening(object sender, EventArgs e)
        {
            if (e is ContextMenuOpeningEventArgs)
            {
                var args = (ContextMenuOpeningEventArgs) e;
                var tweetModel = (TweetModel) ((FrameworkElement) args.FocusedElement).DataContext;
                if (!tweetModel.ShowOnMapVisible && !tweetModel.ShowTweetsInBankVisible &&
                    !tweetModel.ShowTweetsInFilialVisible)
                {
                    args.Cancel = true;
                }
            }
        }

        private void OnTweetModelShowOnMapTap(object sender, EventArgs e)
        {
            var tweetModel = (TweetModel) sender;

            NavigationService.NavigateToMapView(tweetModel.Latitude, tweetModel.Longitude, string.Empty, tweetModel.ID);
        }

        public async Task<string> GetCountryCodeAsync()
        {
            if (_countryCode == null)
            {
                GeoPoint geoPoint = null;

                if (IsGeolocationEnabled)
                {
                    geoPoint = await GeolocationService.GetGeoPointAsync();
                }

                if (geoPoint != null)
                {
                    if (await GetIsNetworkAvailableAsync())
                    {
                        _countryCode =
                            await _geocodeService.GetCountryCodeAsync(geoPoint.Latitude, geoPoint.Longitude);
                    }
                }
            }
            return _countryCode;
        }

        public void Settings()
        {
            NavigationService.UriFor<SettingsViewModel>().Navigate();
        }

        public void About()
        {
            NavigationService.UriFor<AboutViewModel>().Navigate();
        }

        public void Map()
        {
            NavigationService.NavigateToMapView(0d, 0d, string.Empty, 0ul);
        }

        public async void SendTweet()
        {
            await SendTweetInternalAsync(_twitterService);
        }

        public async Task SendTweetInternalAsync(Portable.Services.ITwitterService twitterService)
        {
            if (await GetIsNetworkAvailableAsync())
            {
                if (!twitterService.IsAuthorized)
                {
                    await twitterService.AuthorizeAsync();
                }

                if (!twitterService.IsAuthorized)
                {
                    switch (twitterService.AuthorizationStatus)
                    {
                        case AuthorizationStatus.Denied:
                            {
                                if (!_isTweetsLoading && !_isProfilesLoading)
                                {
                                    Dispatcher.BeginInvoke(ShowAuthorizationDeniedMessageBox);
                                }
                            }
                            break;
                        case AuthorizationStatus.Failure:
                            {
                                if (!_isTweetsLoading && !_isProfilesLoading)
                                {
                                    Dispatcher.BeginInvoke(ShowAuthorizationFailureMessageBox);
                                }
                            }
                            break;
                    }
                    return;
                }

                Tweet tweet = GetTweet();

#if DEBUG
                var geoPoint = new GeoPoint(Latitude, Longitude);
#else
                GeoPoint geoPoint = null;
                if (IsGeolocationEnabled)
                {
                    geoPoint = await GeolocationService.GetGeoPointAsync();
                }
#endif
                if (geoPoint != null)
                {
                    tweet.Location = geoPoint;
                }

                if (SettingsService.SignatureEnabled)
                {
                    tweet.Signature = SettingsService.Signature;
                }

                var tweets = new List<Tweet>();
                List<string> list = tweet.Split();
                for (int i = 0; i < list.Count; i++)
                {
                    tweets.AddRange(
                        await twitterService.SendTweetAsync(new Tweet
                                                                {
                                                                    Text = list[i],
                                                                    ID = (ulong) DateTime.UtcNow.Ticks,
                                                                    Latitude = tweet.Latitude,
                                                                    Longitude = tweet.Longitude
                                                                }));
                }

                if (tweets != null && tweets.Any())
                {
                    Dispatcher.BeginInvoke(ShowSuccessToast);
                }

                Clear();
            }
        }

        private static void ShowSuccessToast()
        {
            var prompt =
                new ToastPrompt
                    {
                        TextWrapping = TextWrapping.Wrap,
                        Message = "Выплеск опубликован. Совсем скоро он появится в ленте."
                    };

            prompt.Show();
        }

        private void ShowAuthorizationFailureMessageBox()
        {
            RadMessageBox.Show(new[] {"да", "нет"},
                               "Публикация выплеска",
                               "Произошла ошибка во время публикации выплеска. Повторить публикацию?",
                               null,
                               false,
                               false,
                               HorizontalAlignment.Stretch,
                               VerticalAlignment.Top,
                               delegate(MessageBoxClosedEventArgs args)
                                   {
                                       if (args.ButtonIndex == 0)
                                       {
                                           SendTweet();
                                       }
                                   });
        }

        private void ShowAuthorizationDeniedMessageBox()
        {
            RadMessageBox.Show(new[] {"да", "нет"},
                               "Публикация выплеска",
                               "Выплеск не опубликован. Для публикации выплеска необходимо открыть приложению доступ к вашей учетной записи. " +
                               "Опубликовать выплеск анонимно?",
                               null,
                               false,
                               false,
                               HorizontalAlignment.Stretch,
                               VerticalAlignment.Top,
                               async delegate(
                                   MessageBoxClosedEventArgs args)
                                         {
                                             if (args.ButtonIndex == 0)
                                             {
                                                 await SendTweetInternalAsync(_anonymousTwitterService);
                                             }
                                         });
        }

        public static void Sleep(TimeSpan duration)
        {
            using (var sleeper = new ManualResetEvent(false))
            {
                sleeper.WaitOne(duration);
            }
        }

        public async Task<List<Tweet>> RefreshAsync()
        {
            ulong sinceID = 0;
            TweetModel firstTweetModel = Tweets.FirstOrDefault();
            if (firstTweetModel != null)
            {
                sinceID = Convert.ToUInt64(firstTweetModel.ID);
            }

            var tweets = new List<Tweet>();
            if (!_isSearching)
            {
                tweets = await SearchTweetsAsync(sinceID);

                InsertTweets(tweets);

                await UpdateProfileImageUrlsAsync(tweets);
            }
            return tweets;
        }

        private async Task UpdateProfileImageUrlsAsync(List<Tweet> tweets)
        {
            await _anonymousTwitterService.UpdateProfileImageUrlsAsync(tweets);

            foreach (Tweet tweet in tweets)
            {
                TweetModel tweetModel = Tweets.First(x => x.ID == tweet.ID);
                tweetModel.ProfileImageUrl = tweet.ProfileImageUrl;
            }
        }

        private void ShowBoilingAppBar()
        {
            SendTweetButtonVisible = true;
            MapButtonVisible = true;
        }

        private void ShowBoiledAppBar()
        {
            SendTweetButtonVisible = false;
            MapButtonVisible = true;
        }

        #endregion
    }
}