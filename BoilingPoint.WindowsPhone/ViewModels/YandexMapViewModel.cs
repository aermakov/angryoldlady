﻿using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Services;
using BoilingPoint.WindowsPhone.Models;
using BoilingPoint.WindowsPhone.Services;
using Caliburn.Micro;
using Telerik.Windows.Controls;
using Yandex.Maps;
using Yandex.Positioning;
using ITwitterService = BoilingPoint.WindowsPhone.Services.ITwitterService;
using PushPin = BoilingPoint.WindowsPhone.Controls.PushPin;

namespace BoilingPoint.WindowsPhone.ViewModels
{
    public class YandexMapViewModel : MapViewModel
    {
        #region Fields

        private const double DEFAULT_ZOOM_LEVEL = 17;

        private Map _map;
        private LayerBase _pushpinsLayer;

        #endregion

        #region Ctors

        public YandexMapViewModel(
            INavigationService navigationService,
            IGeolocationService geolocationService,
            IAnonymousTwitterService anonymousTwitterService,
            ITwitterService twitterService,
            ISettingsService settingsService,
            IDataService dataService)
            : base(navigationService,
                   geolocationService,
                   anonymousTwitterService,
                   twitterService,
                   settingsService,
                   dataService)
        {
        }

        #endregion

        #region Properties

        private Map Map
        {
            get
            {
                if (_map == null)
                    _map = (Map) ((FrameworkElement) GetView()).FindName("map");
                return _map;
            }
        }

        #endregion

        #region Methods

        public void OnTweetsSelectionChanged(SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count <= 0)
                return;

            if (_isTweetsLoading && (!_isTweetsLoading || !Equals(Latitude, 0d) || !Equals(Longitude, 0d))) return;
            var tweetModel = (TweetModel) e.AddedItems[0];
            if (tweetModel != null && !Equals(tweetModel.Latitude, 0d) && !Equals(tweetModel.Longitude, 0d))
            {
                JumpToPosition(tweetModel.Latitude, tweetModel.Longitude);
            }
        }

        public void OnTweetLoopingItemTapped(object sender)
        {
            var tweetModel = (TweetModel) ((FrameworkElement) sender).DataContext;
            if (tweetModel != null)
            {
                JumpToPosition(tweetModel.Latitude, tweetModel.Longitude);
            }
        }

        public void OnMapTapped()
        {
            CollapseAllPushpins();

            TweetModel selectedTweet = GetSelectedTweet();

            ReloadTweets();

            if (selectedTweet != null)
            {
                SetSelectedTweet(selectedTweet);
            }
        }

        private void CollapseAllPushpins()
        {
            foreach (UIElement child in _pushpinsLayer.Children)
            {
                if (child is PushPin)
                {
                    var pushPin = (PushPin) child;
                    pushPin.ContentVisibility = Visibility.Collapsed;
                }
            }
        }

        protected override void SetSelectedTweet(TweetModel tweet)
        {
            var slideView = FindName<RadSlideView>("TweetsSlideView");
            slideView.SelectedItem = tweet;
        }

        protected override TweetModel GetSelectedTweet()
        {
            var slideView = FindName<RadSlideView>("TweetsSlideView");
            return (TweetModel) slideView.SelectedItem;
        }

        protected override void OnViewLoaded(object view)
        {
            _pushpinsLayer = ((LayerBase) Map.FindName("PushpinsLayer"));

            base.OnViewLoaded(view);
        }

        protected override void OnActivate()
        {
            base.OnActivate();

            if (!Equals(Map.UseLocation, SettingsService.GeolocationEnabled))
            {
                Map.UseLocation = SettingsService.GeolocationEnabled;
            }
        }

        public override async Task<GeoPoint> FindMe()
        {
            ReloadTweets();

            return await base.FindMe();
        }

        protected override void JumpToPosition(double latitude, double longitude)
        {
            Map.Center = new GeoCoordinate(latitude, longitude);
            Map.ZoomLevel = DEFAULT_ZOOM_LEVEL;
        }

        protected override void JumpToPosition(GeoPoint geoPoint)
        {
            if (geoPoint == null) return;
            Map.Center = new GeoCoordinate(geoPoint.Latitude, geoPoint.Longitude);
            Map.ZoomLevel = DEFAULT_ZOOM_LEVEL;
        }

        protected override void SetLocation(PushpinModelBase pushpinModel, double latitude, double longitude)
        {
            base.SetLocation(pushpinModel, latitude, longitude);

            foreach (FrameworkElement element in _pushpinsLayer.Children)
            {
                if (!Equals(element.DataContext, pushpinModel)) continue;
                MapLayer.SetLocation(element, new GeoCoordinate(latitude, longitude));
                break;
            }
        }

        protected override void AddImagePushpin(PushpinModelBase pushpinModel, BitmapImage bitmapImage)
        {
            var image = new Image {Source = bitmapImage, DataContext = pushpinModel};

            var coordinate = new GeoCoordinate(pushpinModel.Location.Latitude, pushpinModel.Location.Longitude);
            MapLayer.SetLocation(image, coordinate);
            MapLayer.SetAlignment(image, Alignment.BottomCenter);

            _pushpinsLayer.Children.Add(image);
        }

        protected override void AddPushpin(PushpinModelBase pushpinModel, DataTemplate template)
        {
            var pushPin =
                new PushPin
                    {
                        DataContext = pushpinModel,
                        ContentTemplate = template,
                        ContentVisibility = pushpinModel.ContentVisibility,
                        Visibility = pushpinModel.Visibility,
                        State = pushpinModel.State,
                    };

            MapLayer.SetLocation(pushPin,
                                 new GeoCoordinate(pushpinModel.Location.Latitude, pushpinModel.Location.Longitude));
            MapLayer.SetAlignment(pushPin, Alignment.BottomCenter);

            Canvas.SetZIndex(pushPin, pushpinModel.ZIndex);

            if (pushpinModel is TweetPushpinModel)
            {
                pushPin.PreviewTap += OnPushPinPreviewTap;
            }

            _pushpinsLayer.Children.Add(pushPin);
        }

        private void OnPushPinPreviewTap(object sender, CancelEventArgs e)
        {
            var pushpin = (PushPin) sender;
            var pushpinModel = ((PushpinModelBase) pushpin.DataContext);
            if (pushpinModel is TweetPushpinModel)
            {
                var tweetPushpinModel = (TweetPushpinModel) pushpinModel;

                Tweets.Clear();
                Tweets.AddRange(tweetPushpinModel.TweetGrouping.Select(x => new TweetModel(x)));
            }
        }

        /// <summary>
        ///     Zoom in to closest integer zoom level.
        /// </summary>
        public override void ZoomIn()
        {
            Map.ZoomIn();
        }

        /// <summary>
        ///     Zoom out to closest integer zoom level.
        /// </summary>
        public override void ZoomOut()
        {
            Map.ZoomOut();
        }

        #endregion
    }
}