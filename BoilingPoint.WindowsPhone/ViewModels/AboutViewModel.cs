﻿using System;
using System.Windows;
using BoilingPoint.WindowsPhone.Helpers;
using Caliburn.Micro;
using Microsoft.Phone.Tasks;

namespace BoilingPoint.WindowsPhone.ViewModels
{
    public class AboutViewModel : ViewModelBase
    {
        public AboutViewModel(INavigationService navigationService)
            : base(navigationService)
        {
        }

        public string VersionNumber
        {
            get { return DeviceInfoHelper.GetVersionNumber(); }
        }

        public void MarketplaceReview()
        {
            var marketplaceReviewTask = new MarketplaceReviewTask();
            marketplaceReviewTask.Show();
        }

        public void OnTwitterUserNameTapped(FrameworkElement sender)
        {
            var webBrowserTask =
                new WebBrowserTask
                    {
                        Uri = new Uri(string.Format("https://twitter.com/{0}", sender.Tag), UriKind.Absolute)
                    };
            webBrowserTask.Show();
        }
    }
}