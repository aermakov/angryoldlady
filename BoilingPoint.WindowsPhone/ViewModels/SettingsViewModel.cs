﻿using BoilingPoint.Portable.Services;
using BoilingPoint.WindowsPhone.Services;
using Caliburn.Micro;

namespace BoilingPoint.WindowsPhone.ViewModels
{
    public class SettingsViewModel : ViewModelBase
    {
        public SettingsViewModel(INavigationService navigationService,
            ISettingsService settingsService,
            IGeolocationService geolocationService,
            IAnalyticalService analyticalService)
            : base(navigationService, settingsService, geolocationService, analyticalService)
        {
        }

        public bool GeolocationEnabled
        {
            get { return SettingsService.GeolocationEnabled; }
            set { SettingsService.GeolocationEnabled = value; }
        }

        public bool SignatureEnabled
        {
            get { return SettingsService.SignatureEnabled; }
            set { SettingsService.SignatureEnabled = value; }
        }

        public string Signature
        {
            get { return SettingsService.Signature; }
            set { SettingsService.Signature = value; }
        }
    }
}