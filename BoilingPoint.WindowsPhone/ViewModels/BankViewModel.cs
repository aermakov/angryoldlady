﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Events;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Models;
using BoilingPoint.Portable.Services;
using BoilingPoint.WindowsPhone.Extensions;
using Caliburn.Micro;
using Microsoft.Phone.Tasks;
using Telerik.Windows.Controls;

namespace BoilingPoint.WindowsPhone.ViewModels
{
    public class BankViewModel : ViewModelBase
    {
        #region Fields

        private readonly IAnonymousTwitterService _anonymousTwitterService;
        private readonly IDataService _dataService;
        private readonly IGeocodeService _geocodeService;
        private Bank _bank;
        private bool _isTweetsLoading;
        private BindableCollection<TweetModel> _tweets;
        private string _tweetsLoadingErrorMessage;

        #endregion

        #region Ctors

        public BankViewModel(INavigationService navigationService,
                             IDataService dataService,
                             IAnonymousTwitterService anonymousTwitterService,
                             IGeocodeService geocodeService)
            : base(navigationService)
        {
            _dataService = dataService;
            _anonymousTwitterService = anonymousTwitterService;
            _geocodeService = geocodeService;
            _anonymousTwitterService.TweetsAdded += OnAnonymousTwitterServiceTweetsAdded;

            _tweets = new BindableCollection<TweetModel>();
        }

        #endregion

        #region Properties

        public string BankId { get; set; }

        public Bank Bank
        {
            get { return _bank; }
            set
            {
                if (Equals(value, _bank)) return;
                _bank = value;
                NotifyOfPropertyChange(() => Bank);
                NotifyOfPropertyChange(() => Address);
                NotifyOfPropertyChange(() => AddressVisible);
                NotifyOfPropertyChange(() => Title);
                NotifyOfPropertyChange(() => TitleVisible);
                NotifyOfPropertyChange(() => WebSite);
                NotifyOfPropertyChange(() => WebSiteVisible);
                NotifyOfPropertyChange(() => PhoneNumber);
                NotifyOfPropertyChange(() => PhoneNumberVisible);
                NotifyOfPropertyChange(() => TwitterUserName);
                NotifyOfPropertyChange(() => TwitterUserNameVisible);
            }
        }

        public string Address
        {
            get { return Bank != null ? Bank.Address : null; }
        }

        public bool AddressVisible
        {
            get { return Address != null; }
        }

        public string Title
        {
            get { return Bank != null ? Bank.Title : null; }
        }

        public bool TitleVisible
        {
            get { return Title != null; }
        }

        public string PhoneNumber
        {
            get { return Bank != null ? Bank.InternationalPhoneNumber : null; }
        }

        public bool PhoneNumberVisible
        {
            get { return PhoneNumber != null; }
        }

        public string WebSite
        {
            get { return Bank != null ? Bank.WebSite : null; }
        }

        public bool WebSiteVisible
        {
            get { return WebSite != null; }
        }

        public string TwitterUserName
        {
            get
            {
                if (Bank != null && Bank.TwitterUsername != null)
                {
                    return string.Format("@{0}", Bank.TwitterUsername);
                }
                return null;
            }
        }

        public bool TwitterUserNameVisible
        {
            get { return TwitterUserName != null; }
        }

        public BindableCollection<TweetModel> Tweets
        {
            get { return _tweets; }
            set
            {
                if (Equals(value, _tweets)) return;
                _tweets = value;
                NotifyOfPropertyChange(() => Tweets);
            }
        }

        private RadBusyIndicator TweetsLoadingBusyIndicator
        {
            get
            {
                var view = (FrameworkElement) GetView();
                if (view != null) return (RadBusyIndicator) view.FindName("TweetsLoadingBusyIndicator");
                return null;
            }
        }

        public bool IsTweetsLoading
        {
            get { return _isTweetsLoading; }
            set
            {
                if (value.Equals(_isTweetsLoading)) return;
                _isTweetsLoading = value;
                NotifyOfPropertyChange(() => IsTweetsLoading);
            }
        }

        public string TweetsLoadingErrorMessage
        {
            get { return _tweetsLoadingErrorMessage; }
            set
            {
                if (value == _tweetsLoadingErrorMessage) return;
                _tweetsLoadingErrorMessage = value;
                NotifyOfPropertyChange(() => TweetsLoadingErrorMessage);
                NotifyOfPropertyChange(() => IsTweetsLoadingErrorMessageVisible);
            }
        }

        public bool IsTweetsLoadingErrorMessageVisible
        {
            get { return !string.IsNullOrEmpty(TweetsLoadingErrorMessage); }
        }

        #endregion

        #region Methods

        private void ShowTweetsLoadingErrorMessage(string errorMessage)
        {
            TweetsLoadingErrorMessage = errorMessage;
        }

        private void HideTweetsLoadingErrorMessage()
        {
            TweetsLoadingErrorMessage = string.Empty;
        }

        public async void OnAddressTapped()
        {
            GeoAddress geoAddress = (await _geocodeService.GeocodeAsync(Address)).FirstOrDefault();
            NavigationService.NavigateToMapView(geoAddress);
        }

        public void OnPhoneNumberTapped()
        {
            var phoneCallTask =
                new PhoneCallTask
                    {
                        PhoneNumber = PhoneNumber,
                        DisplayName = Title
                    };
            phoneCallTask.Show();
        }

        public void OnWebSiteTapped()
        {
            var webBrowserTask =
                new WebBrowserTask
                    {
                        Uri = new Uri(string.Format("http://{0}", WebSite), UriKind.Absolute)
                    };
            webBrowserTask.Show();
        }

        public void OnTwitterUserNameTapped()
        {
            var webBrowserTask =
                new WebBrowserTask
                    {
                        Uri = new Uri(string.Format("https://twitter.com/{0}", Bank.TwitterUsername), UriKind.Absolute)
                    };
            webBrowserTask.Show();
        }

        private void OnAnonymousTwitterServiceTweetsAdded(object sender, TweetsAddedEventArgs e)
        {
            IOrderedEnumerable<Tweet> tweets = e.Tweets.Where(tweet => Equals(tweet.BankId, BankId))
                                                .OrderByDescending(tweet => tweet.CreatedAtTimestamp);

            foreach (Tweet tweet in tweets)
            {
                var tweetModel = new TweetModel(tweet);
                if (!Tweets.Contains(tweetModel))
                {
                    tweetModel.ContextMenuOpening += OnTweetContextMenuOpening;
                    Tweets.Insert(0, tweetModel);
                }
            }
        }

        protected override async void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);

            if (BankId == null) return;
            if (await GetIsNetworkAvailableAsync())
            {
                IsTweetsLoading = true;

                List<Tweet> tweets = await _dataService.GetTweetsByBankIdAsync(BankId);

                IsTweetsLoading = false;
                TweetsLoadingBusyIndicator.Visibility = Visibility.Collapsed;

                if (tweets.Count == 0)
                {
                    ShowTweetsLoadingErrorMessage(AppResources.NoTweets);
                }
                else
                {
                    foreach (Tweet tweet in tweets.OrderByDescending(tweet => tweet.CreatedAtTimestamp))
                    {
                        AddTweet(tweet);
                    }

                    HideTweetsLoadingErrorMessage();
                }

                Bank = await _dataService.GetBankByIdAsync(BankId);
            }
            else
            {
                ShowTweetsLoadingErrorMessage(AppResources.NetworkUnavailable);
            }
        }

        private void AddTweet(Tweet tweet)
        {
            var tweetModel = new TweetModel(tweet);
            if (!Tweets.Contains(tweetModel))
            {
                tweetModel.ContextMenuOpening += OnTweetContextMenuOpening;

                Tweets.Add(tweetModel);
            }
        }

        private void OnTweetContextMenuOpening(object sender, EventArgs e)
        {
            ((ContextMenuOpeningEventArgs) e).Cancel = true;
        }

        #endregion
    }
}