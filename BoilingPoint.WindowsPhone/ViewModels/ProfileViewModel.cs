﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Events;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.DoubleGis;
using BoilingPoint.Portable.Models;
using BoilingPoint.Portable.Services;
using BoilingPoint.WindowsPhone.Extensions;
using Caliburn.Micro;
using Microsoft.Phone.Tasks;
using Telerik.Windows.Controls;

namespace BoilingPoint.WindowsPhone.ViewModels
{
    public class ProfileViewModel : ViewModelBase
    {
        #region Fields

        private readonly IAnonymousTwitterService _anonymousTwitterService;
        private readonly IDataService _dataService;
        private readonly IGeocodeService _geocodeService;
        private BindableCollection<ContactModel> _contacts;
        private bool _isTweetsLoading;
        private Profile _profile;
        private string _profileId;
        private BindableCollection<TweetModel> _tweets;
        private string _tweetsLoadingErrorMessage;

        #endregion

        #region Ctors

        public ProfileViewModel(INavigationService navigationService,
                                IDataService dataService,
                                IAnonymousTwitterService anonymousTwitterService,
                                IGeocodeService geocodeService)
            : base(navigationService)
        {
            _dataService = dataService;
            _anonymousTwitterService = anonymousTwitterService;
            _geocodeService = geocodeService;
            _anonymousTwitterService.TweetsAdded += OnAnonymousTwitterServiceTweetsAdded;

            _tweets = new BindableCollection<TweetModel>();
            _contacts = new BindableCollection<ContactModel>();
        }

        #endregion

        #region Properties

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string ProfileId
        {
            get { return _profileId; }
            set
            {
                if (Equals(value, _profileId)) return;
                _profileId = value;
                NotifyOfPropertyChange(() => ProfileId);
            }
        }

        public Profile Profile
        {
            get { return _profile; }
            set
            {
                if (Equals(value, _profile)) return;
                _profile = value;
                ProfileId = _profile.Id;
                NotifyOfPropertyChange(() => Profile);
                NotifyOfPropertyChange(() => Address);
                NotifyOfPropertyChange(() => AddressVisible);
                NotifyOfPropertyChange(() => Name);
                NotifyOfPropertyChange(() => NameVisible);
                NotifyOfPropertyChange(() => DoubleGisHyperlinkVisible);
            }
        }

        public BindableCollection<ContactModel> Contacts
        {
            get { return _contacts; }
            set
            {
                if (Equals(value, _contacts)) return;
                _contacts = value;
                NotifyOfPropertyChange(() => Contacts);
            }
        }

        public string Address
        {
            get { return Profile != null ? Profile.Address : null; }
        }

        public bool AddressVisible
        {
            get { return Address != null; }
        }

        public string Name
        {
            get { return Profile != null ? Profile.Name : null; }
        }

        public bool NameVisible
        {
            get { return Name != null; }
        }

        public BindableCollection<TweetModel> Tweets
        {
            get { return _tweets; }
            set
            {
                if (Equals(value, _tweets)) return;
                _tweets = value;
                NotifyOfPropertyChange(() => Tweets);
            }
        }

        private RadBusyIndicator TweetsLoadingBusyIndicator
        {
            get
            {
                var view = (FrameworkElement) GetView();
                if (view != null) return (RadBusyIndicator) view.FindName("TweetsLoadingBusyIndicator");
                return null;
            }
        }

        public bool IsTweetsLoading
        {
            get { return _isTweetsLoading; }
            set
            {
                if (value.Equals(_isTweetsLoading)) return;
                _isTweetsLoading = value;
                NotifyOfPropertyChange(() => IsTweetsLoading);
            }
        }

        public string TweetsLoadingErrorMessage
        {
            get { return _tweetsLoadingErrorMessage; }
            set
            {
                if (value == _tweetsLoadingErrorMessage) return;
                _tweetsLoadingErrorMessage = value;
                NotifyOfPropertyChange(() => TweetsLoadingErrorMessage);
                NotifyOfPropertyChange(() => IsTweetsLoadingErrorMessageVisible);
            }
        }

        public bool IsTweetsLoadingErrorMessageVisible
        {
            get { return !string.IsNullOrEmpty(TweetsLoadingErrorMessage); }
        }

        public bool DoubleGisHyperlinkVisible
        {
            get { return Profile != null; }
        }

        #endregion

        #region Methods

        private void ShowTweetsLoadingErrorMessage(string errorMessage)
        {
            TweetsLoadingErrorMessage = errorMessage;
        }

        private void HideTweetsLoadingErrorMessage()
        {
            TweetsLoadingErrorMessage = string.Empty;
        }

        public async void OnAddressTapped()
        {
            GeoAddress geoAddress = (await _geocodeService.GeocodeAsync(Address)).FirstOrDefault();
            NavigationService.NavigateToMapView(geoAddress);
        }

        public void OnDoubleGisHyperlinkButtonTap()
        {
            var webBrowserTask = new WebBrowserTask {Uri = new Uri("http://api.2gis.ru/", UriKind.Absolute)};
            webBrowserTask.Show();
        }

        private void OnAnonymousTwitterServiceTweetsAdded(object sender, TweetsAddedEventArgs e)
        {
            IOrderedEnumerable<Tweet> tweets = e.Tweets.Where(tweet => Equals(tweet.ProfileId, ProfileId))
                                                .OrderByDescending(tweet => tweet.CreatedAtTimestamp);

            foreach (Tweet tweet in tweets)
            {
                var tweetModel = new TweetModel(tweet);
                if (!Tweets.Contains(tweetModel))
                {
                    tweetModel.ContextMenuOpening += OnTweetContextMenuOpening;
                    Tweets.Insert(0, tweetModel);
                }
            }
        }

        public void OnContactTapped(object sender)
        {
            var contact = (ContactModel) ((FrameworkElement) sender).DataContext;

            switch (contact.Type)
            {
                case ContactType.Email:
                    {
                        var emailComposeTask = new EmailComposeTask {To = contact.Value};
                        emailComposeTask.Show();
                    }
                    break;
                case ContactType.Phone:
                    {
                        var phoneCallTask =
                            new PhoneCallTask
                                {
                                    PhoneNumber = contact.Value,
                                    DisplayName = Profile.Name
                                };
                        phoneCallTask.Show();
                    }
                    break;
                case ContactType.WebSite:
                    {
                        string uriString = contact.Value;
                        if (!uriString.StartsWith("http"))
                        {
                            uriString = string.Format("http://{0}", uriString);
                        }

                        var webBrowserTask =
                            new WebBrowserTask
                                {
                                    Uri = new Uri(uriString, UriKind.Absolute)
                                };
                        webBrowserTask.Show();
                    }
                    break;
            }
        }

        protected override async void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);

            IsTweetsLoading = true;

            var tweets = new List<Tweet>();
            if (ProfileId == null)
            {
                if (await GetIsNetworkAvailableAsync())
                {
                    var geoPoint = new GeoPoint(Latitude, Longitude);
                    tweets = await _dataService.GetTweetsAsync(geoPoint, DataService.DEFAULT_RADIUS_IN_METERS);

                    IsTweetsLoading = false;
                    TweetsLoadingBusyIndicator.Visibility = Visibility.Collapsed;

                    if (tweets.Count == 0)
                    {
                        ShowTweetsLoadingErrorMessage(AppResources.NoTweets);
                    }
                    else
                    {
                        HideTweetsLoadingErrorMessage();
                    }
                }
                else
                {
                    ShowTweetsLoadingErrorMessage(AppResources.NetworkUnavailable);
                }
            }
            else
            {
                if (await GetIsNetworkAvailableAsync())
                {
                    tweets = await _dataService.GetTweetsByProfileIdAsync(ProfileId);

                    IsTweetsLoading = false;
                    TweetsLoadingBusyIndicator.Visibility = Visibility.Collapsed;

                    if (tweets.Count == 0)
                    {
                        ShowTweetsLoadingErrorMessage(AppResources.NoTweets);
                    }
                    else
                    {
                        HideTweetsLoadingErrorMessage();
                    }

                    Profile = await _dataService.GetProfileByIdAsync(ProfileId);
                    if (Profile != null)
                    {
                        _contacts.AddRange(Profile.Contacts.Select(contact => new ContactModel(contact)));
                    }
                }
                else
                {
                    ShowTweetsLoadingErrorMessage(AppResources.NetworkUnavailable);
                }
            }

            foreach (Tweet tweet in tweets.OrderByDescending(tweet => tweet.CreatedAtTimestamp))
            {
                AddTweet(tweet);
            }
        }

        private void AddTweet(Tweet tweet)
        {
            var tweetModel = new TweetModel(tweet);
            if (!Tweets.Contains(tweetModel))
            {
                tweetModel.ContextMenuOpening += OnTweetContextMenuOpening;

                Tweets.Add(tweetModel);
            }
        }

        private void OnTweetContextMenuOpening(object sender, EventArgs e)
        {
            ((ContextMenuOpeningEventArgs) e).Cancel = true;
        }

        #endregion
    }
}