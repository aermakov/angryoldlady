﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Services;
using BoilingPoint.WindowsPhone.Models;
using BoilingPoint.WindowsPhone.Services;
using Caliburn.Micro;
using ITwitterService = BoilingPoint.WindowsPhone.Services.ITwitterService;

namespace BoilingPoint.WindowsPhone.ViewModels
{
    public class GoogleMapViewModel : MapViewModel
    {
        #region Ctors

        public GoogleMapViewModel(
            INavigationService navigationService,
            IGeolocationService geolocationService,
            IAnonymousTwitterService anonymousTwitterService,
            ITwitterService twitterService,
            ISettingsService settingsService,
            IDataService dataService)
            : base(
                navigationService, geolocationService, anonymousTwitterService, twitterService, settingsService,
                dataService)
        {
        }

        #endregion

        protected override void SetSelectedTweet(TweetModel tweet)
        {
            throw new NotImplementedException();
        }

        protected override TweetModel GetSelectedTweet()
        {
            throw new NotImplementedException();
        }

        protected override void AddImagePushpin(PushpinModelBase pushpinModel, BitmapImage bitmapImage)
        {
            throw new NotImplementedException();
        }

        protected override void AddPushpin(PushpinModelBase pushpinModel, DataTemplate template)
        {
            throw new NotImplementedException();
        }

        public override void ZoomIn()
        {
            throw new NotImplementedException();
        }

        public override void ZoomOut()
        {
            throw new NotImplementedException();
        }

        protected override void SetLocation(PushpinModelBase pushpinModel, double latitude, double longitude)
        {
            throw new NotImplementedException();
        }

        protected override void JumpToPosition(GeoPoint geoPoint)
        {
            throw new NotImplementedException();
        }

        protected override void JumpToPosition(double latitude, double longitude)
        {
            throw new NotImplementedException();
        }
    }
}