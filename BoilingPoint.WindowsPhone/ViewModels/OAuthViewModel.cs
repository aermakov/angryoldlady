﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Navigation;
using BoilingPoint.Portable.Enums;
using BoilingPoint.WindowsPhone.Services;
using Caliburn.Micro;
using HtmlAgilityPack;
using LinqToTwitter;
using Microsoft.Phone.Controls;

namespace BoilingPoint.WindowsPhone.ViewModels
{
    public class OAuthViewModel : ViewModelBase
    {
        public const string AUTHORIZE_URL = "http://api.twitter.com/oauth/authorize";

        private readonly ITwitterService _twitterService;
        private WebBrowser _oAuthWebBrowser;

        private string _pin;
        private PinAuthorizer _pinAuthorizer;

        public OAuthViewModel(INavigationService navigationService,
                              ITwitterService twitterService)
            : base(navigationService)
        {
            _twitterService = twitterService;
        }

        public WebBrowser OAuthWebBrowser
        {
            get
            {
                if (_oAuthWebBrowser == null)
                {
                    _oAuthWebBrowser = (WebBrowser) ((FrameworkElement) GetView()).FindName("OAuthWebBrowser");
                }
                return _oAuthWebBrowser;
            }
            set { _oAuthWebBrowser = value; }
        }

        public void CompleteAuthorize()
        {
            _pinAuthorizer.CompleteAuthorize(
                _pin,
                completeResp => Dispatcher.BeginInvoke(
                    () =>
                        {
                            switch (completeResp.Status)
                            {
                                case TwitterErrorStatus.Success:
                                    {
                                        _twitterService.Authorizer = _pinAuthorizer;
                                        _twitterService.AuthorizationStatus = AuthorizationStatus.Success;
                                    }
                                    break;
                                case TwitterErrorStatus.TwitterApiError:
                                case TwitterErrorStatus.RequestProcessingException:
                                    {
                                        _twitterService.AuthorizationStatus = AuthorizationStatus.Failure;
                                    }
                                    break;
                            }

                            NavigationService.GoBack();
                        }));
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);

            OAuthWebBrowser.LoadCompleted += OAuthWebBrowserLoadCompleted;
            OAuthWebBrowser.Navigating += OAuthWebBrowserNavigating;

            _pinAuthorizer =
                new PinAuthorizer
                    {
                        Credentials =
                            new InMemoryCredentials
                                {
                                    ConsumerKey = Settings.TWITTER_CONSUMER_KEY,
                                    ConsumerSecret = Settings.TWITTER_CONSUMER_SECRET
                                },
                        UseCompression = true,
                        GoToTwitterAuthorization =
                            pageLink =>
                            Dispatcher.BeginInvoke(
                                () =>
                                OAuthWebBrowser.Navigate(new Uri(pageLink, UriKind.Absolute)))
                    };

            _pinAuthorizer.BeginAuthorize(resp =>
                                          Dispatcher.BeginInvoke(
                                              () =>
                                                  {
                                                      switch (resp.Status)
                                                      {
                                                          case TwitterErrorStatus.Success:
                                                              break;
                                                          case TwitterErrorStatus.TwitterApiError:
                                                          case TwitterErrorStatus.RequestProcessingException:
                                                              {
                                                                  _twitterService.AuthorizationStatus =
                                                                      AuthorizationStatus.Failure;

                                                                  NavigationService.GoBack();
                                                              }
                                                              break;
                                                      }
                                                  }));
        }

        private void OAuthWebBrowserNavigating(object sender, NavigatingEventArgs e)
        {
            if (e.Uri.AbsoluteUri.ToLower().Replace("https://", "http://") == AUTHORIZE_URL)
            {
                OAuthWebBrowser.Visibility = Visibility.Collapsed;
            }
        }

        private void OAuthWebBrowserLoadCompleted(object sender, NavigationEventArgs e)
        {
            if (e.Uri.AbsoluteUri.ToLower().Replace("https://", "http://") != AUTHORIZE_URL)
                return;

            string htmlString = OAuthWebBrowser.SaveToString();

            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(htmlString);

            HtmlNode codeNode = htmlDocument.DocumentNode.Descendants("code").FirstOrDefault();
            if (codeNode != null)
            {
                _pin = codeNode.InnerText;
            }

            if (!string.IsNullOrEmpty(_pin))
            {
                CompleteAuthorize();
            }
            else
            {
                Dispatcher.BeginInvoke(delegate
                                           {
                                               _twitterService.AuthorizationStatus = AuthorizationStatus.Denied;

                                               NavigationService.GoBack();
                                           });
            }
        }
    }
}