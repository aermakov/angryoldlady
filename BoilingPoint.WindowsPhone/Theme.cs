﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace BoilingPoint.WindowsPhone
{
    public class Theme
    {
        public static readonly DependencyProperty LightUrlProperty =
            DependencyProperty.RegisterAttached("LightUrl",
                                                typeof (string), typeof (Theme), null);

        public static readonly DependencyProperty DarkUrlProperty =
            DependencyProperty.RegisterAttached("DarkUrl",
                                                typeof (string), typeof (Theme), null);

        public static string GetLightUrl(DependencyObject obj)
        {
            return (string) obj.GetValue(LightUrlProperty);
        }

        public static void SetLightUrl(DependencyObject obj, string value)
        {
            obj.SetValue(LightUrlProperty, value);
            Setup(obj);
        }

        public static string GetDarkUrl(DependencyObject obj)
        {
            return (string) obj.GetValue(DarkUrlProperty);
        }

        public static void SetDarkUrl(DependencyObject obj, string value)
        {
            obj.SetValue(DarkUrlProperty, value);
            Setup(obj);
        }

        private static void Setup(DependencyObject obj)
        {
            if (DesignerProperties.GetIsInDesignMode(obj))
                return;

            var image = obj as Image;
            if (image == null)
                throw new Exception("Attach only to an Image");

            string darkUrl = GetDarkUrl(image);
            string lightUrl = GetLightUrl(image);
            if (string.IsNullOrWhiteSpace(darkUrl)
                || string.IsNullOrWhiteSpace(lightUrl))
                return;

            image.Loaded += OnImageLoaded;
        }

        private static void OnImageLoaded(object sender, RoutedEventArgs e)
        {
            var image = sender as Image;
            string darkUrl = GetDarkUrl(image);
            string lightUrl = GetLightUrl(image);

            // what if the user didn't set this up right?
            if (string.IsNullOrWhiteSpace(darkUrl)
                || string.IsNullOrWhiteSpace(lightUrl))
                throw new Exception("Dark & White are required");

            string uriString;
            switch (CurrentTheme(image))
            {
                case Themes.Dark:
                    uriString = darkUrl;
                    break;
                case Themes.Light:
                    uriString = lightUrl;
                    break;
                default:
                    throw new Exception("Theme not supported");
            }
            var uri = new Uri(uriString, UriKind.Relative);
            var bitmapImage = new BitmapImage(uri);
            if (image != null) image.Source = bitmapImage;
        }

        private static Themes CurrentTheme(Image image)
        {
            object resource = image.Resources["PhoneDarkThemeVisibility"];
            var visibility = (Visibility) resource;
            if (visibility == Visibility.Visible)
                return Themes.Dark;
            return Themes.Light;
        }

        private enum Themes
        {
            Dark,
            Light
        }
    }
}