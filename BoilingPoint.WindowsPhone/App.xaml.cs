﻿using BugSense;
using Yandex.Metrica;

namespace BoilingPoint.WindowsPhone
{
    public partial class App
    {
        /// <summary>
        ///     Constructor for the Application object.
        /// </summary>
        public App()
        {
            InitializeComponent();

            Counter.Start(Settings.YANDEX_METRICA_API_KEY);
            Counter.StartNewSessionManually();

            BugSenseHandler.Instance.InitAndStartSession(this, Settings.BUGSENSE_API_KEY);
        }
    }
}