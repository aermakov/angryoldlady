using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using BoilingPoint.Portable;
using BoilingPoint.Portable.Constants;
using BoilingPoint.Portable.Services;
using Newtonsoft.Json;

namespace BoilingPoint.WindowsPhone
{
    /// <summary>
    /// </summary>
    public class Cache : ICache
    {
        private static Cache _current;

        private readonly IsolatedStorageFile _store = IsolatedStorageFile.GetUserStoreForApplication();

        private readonly object _sync = new object();

        /// <summary>
        ///     Gets the current instance of the cache
        /// </summary>
        /// <value>The current.</value>
        public static ICache Current
        {
            get { return _current ?? (_current = new Cache()); }
        }

        public IAnalyticalService AnalyticalService { get; set; }

        /// <summary>
        ///     Adds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="absoluteExpiration">The absolute expiration.</param>
        /// <param name="slidingExpiration">The sliding expiration.</param>
        public void Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
        {
            lock (_sync)
            {
                if (Contains(key))
                    Remove(key);

                if (absoluteExpiration == CacheConstants.NoAbsoluteExpiration)
                    Add(key, DateTime.UtcNow + slidingExpiration, value);

                if (slidingExpiration == CacheConstants.NoSlidingExpiration)
                    Add(key, absoluteExpiration, value);
            }
        }

        /// <summary>
        ///     Determines whether the cache contains the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        ///     <c>true</c> if [contains] [the specified key]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(string key)
        {
            lock (_sync)
            {
                if (_store.DirectoryExists(key) && GetFileNames(key).Any())
                {
                    string currentFile = GetFileNames(key).FirstOrDefault();
                    if (currentFile != null)
                    {
                        string fileName = Path.GetFileNameWithoutExtension(currentFile);
                        if (fileName != null)
                        {
                            DateTime expirationDate = DateTime.FromFileTimeUtc(long.Parse(fileName));
                            if (expirationDate >= DateTime.UtcNow)
                                return true;
                        }
                    }
                }
                return false;
            }
        }

        /// <summary>
        ///     Removes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        public void Remove(string key)
        {
            lock (_sync)
            {
                if (!Contains(key))
                    throw new AccessViolationException("The key does not exist in the cache");

                string currentFile = GetFileNames(key).FirstOrDefault();
                if (currentFile != null)
                    _store.DeleteFile(string.Format("{0}\\{1}", key, currentFile));
                _store.DeleteDirectory(key);
            }
        }

        /// <summary>
        ///     Gets the specified key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public T Get<T>(string key)
        {
            lock (_sync)
            {
                string currentFile = GetFileNames(key).FirstOrDefault();
                if (currentFile != null)
                {
                    string fileName = Path.GetFileNameWithoutExtension(currentFile);
                    if (fileName != null)
                    {
                        DateTime expirationDate =
                            DateTime.FromFileTimeUtc(long.Parse(fileName));
                        if (expirationDate >= DateTime.UtcNow)
                        {
                            return NormalRead<T>(string.Format(@"{0}\{1}", key, currentFile));
                        }
                    }
                    Remove(key);
                }
                return default(T);
            }
        }

        #region Serialization

        private T NormalRead<T>(string fileName)
        {
            using (var isolatedStorageFileStream = new IsolatedStorageFileStream(fileName, FileMode.Open, _store))
            {
                var serializer = new JsonSerializer();

                object value;
                using (TextReader textReader = new StreamReader(isolatedStorageFileStream))
                {
                    using (JsonReader jsonReader = new JsonTextReader(textReader))
                    {
                        value = serializer.Deserialize<T>(jsonReader);
                    }
                }

                isolatedStorageFileStream.Close();
                return (T) value;
            }
        }

        private void NormalWrite(string fileName, object value)
        {
            using (
                var isolatedStorageFileStream = new IsolatedStorageFileStream(fileName, FileMode.OpenOrCreate, _store))
            {
                var serializer = new JsonSerializer();
                using (TextWriter textWriter = new StreamWriter(isolatedStorageFileStream))
                {
                    serializer.Serialize(textWriter, value);
                }
            }
        }

        #endregion

        /// <summary>
        ///     Adds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="expirationDate">The expiration date.</param>
        /// <param name="value">The value.</param>
        private void Add(string key, DateTime expirationDate, object value)
        {
            lock (_sync)
            {
                if (!_store.DirectoryExists(key))
                    _store.CreateDirectory(key);
                else
                {
                    string currentFile = GetFileNames(key).FirstOrDefault();
                    if (currentFile != null)
                        _store.DeleteFile(string.Format("{0}\\{1}", key, currentFile));
                    _store.DeleteDirectory(key);
                    _store.CreateDirectory(key);
                }

                string fileName = string.Format("{0}\\{1}.cache", key, expirationDate.ToFileTimeUtc());

                if (_store.FileExists(fileName))
                    _store.DeleteFile(fileName);

                NormalWrite(fileName, value);
            }
        }

        /// <summary>
        ///     Gets the file names.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        private IEnumerable<string> GetFileNames(string key)
        {
            return _store.GetFileNames(string.Format("{0}\\*.cache", key));
        }
    }
}