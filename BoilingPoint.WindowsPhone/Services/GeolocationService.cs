﻿using System;
using System.Device.Location;
using System.Threading.Tasks;
using System.Windows;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Events;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Services;
using BoilingPoint.WindowsPhone.Extensions;

namespace BoilingPoint.WindowsPhone.Services
{
    public class GeolocationService : BaseGeolocationService
    {
        #region Fields

        private readonly ISettingsService _settingsService;
        private readonly GeoCoordinateWatcher _watcher;
        private bool _started;
        private GeolocationStatus _status;

        #endregion

        #region Ctors

        public GeolocationService(ISettingsService settingsService)
        {
            _settingsService = settingsService;

            _watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High) {MovementThreshold = 20};
            _watcher.StatusChanged += WatcherStatusChanged;
            _watcher.PositionChanged += WatcherPositionChanged;
        }

        #endregion

        public string ApplicationName
        {
            get { return _settingsService.ApplicationName; }
        }

        public override GeolocationStatus Status
        {
            get { return _status; }
        }

        public override bool Started
        {
            get { return _started; }
        }

        public override void Start()
        {
            _watcher.Start();

            _started = true;
        }

        public override void Stop()
        {
            _watcher.Stop();

            _started = false;
        }

        public override Task<GeoPoint> GetGeoPointAsync()
        {
            var completionSource = new TaskCompletionSource<GeoPoint>();
#if TEST
            var geoPoint = new GeoPoint(55.710056, 37.623575);
#else
            GeolocationStatus status;
            GeoPoint geoPoint = null;
            GeoCoordinate geoCoordinate = GetGeoCoordinate(out status);
            if (status == GeolocationStatus.Ready)
            {
                if (geoCoordinate != null)
                {
                    geoPoint = geoCoordinate.ToGeoPoint();
                }
            }
#endif
            completionSource.TrySetResult(geoPoint);
            return completionSource.Task;
        }

        public GeoCoordinate GetGeoCoordinate(out GeolocationStatus status)
        {
            status = GeolocationStatus.Initializing;
            GeoCoordinate geoCoordinate = null;
            switch (_watcher.Status)
            {
                case GeoPositionStatus.Ready:
                    {
                        status = GeolocationStatus.Ready;
                        if (!_watcher.Position.Location.IsUnknown)
                        {
                            geoCoordinate = _watcher.Position.Location;
                        }
                    }
                    break;
                case GeoPositionStatus.Disabled:
                    {
                        status = GeolocationStatus.Disabled;
                    }
                    break;
                case GeoPositionStatus.NoData:
                    {
                        status = GeolocationStatus.NoData;
                    }
                    break;
            }

            return geoCoordinate;
        }

        public static string GetStatusString(GeolocationStatus status)
        {
            switch (status)
            {
                case GeolocationStatus.Disallowed:
                    return AppResources.GeolocationStatusDisallowed;

                case GeolocationStatus.Disabled:
                    return AppResources.GeolocationStatusDisabled;

                case GeolocationStatus.NoData:
                    return AppResources.GeolocationStatusNoData;
            }
            return null;
        }

        #region Methods

        #endregion

        #region Handlers

        private void WatcherStatusChanged(object sender, GeoPositionStatusChangedEventArgs e)
        {
            _status = (GeolocationStatus) Enum.Parse(typeof (GeolocationStatus), e.Status.ToString(), false);
            OnStatusChanged(this, new GeolocationStatusChangedEventArgs {Status = _status});

            switch (_status)
            {
                case GeolocationStatus.Disabled:
                    MessageBox.Show(GetStatusString(GeolocationStatus.Disabled), ApplicationName,
                                    MessageBoxButton.OK);
                    break;

                case GeolocationStatus.NoData:
                    MessageBox.Show(GetStatusString(GeolocationStatus.NoData), ApplicationName,
                                    MessageBoxButton.OK);
                    break;
            }
        }

        private void WatcherPositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            if (e.Position.Location.IsUnknown)
                return;

            GeoPoint point = e.Position.Location.ToGeoPoint();
            OnPositionChanged(this, new PositionChangedEventArgs {Point = point});
        }

        #endregion
    }
}