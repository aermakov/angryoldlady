﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Navigation;
using BoilingPoint.Portable;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Exceptions;
using BoilingPoint.Portable.Services;
using BoilingPoint.WindowsPhone.ViewModels;
using Caliburn.Micro;
using LinqToTwitter;
using Newtonsoft.Json.Linq;

namespace BoilingPoint.WindowsPhone.Services
{
    public class TwitterService : Portable.Services.TwitterService, ITwitterService
    {
        private readonly INavigationService _navigationService;

        public TwitterService(IGeocodeService geocodeService,
                              IDataService dataService,
                              INavigationService navigationService)
            : base(geocodeService, dataService)
        {
            _navigationService = navigationService;

            Cache = new Cache();
        }

        public ITwitterAuthorizer Authorizer { get; set; }

        public override async Task<IEnumerable<Tweet>> SendTweetAsync(Tweet tweet)
        {
            Guard.ArgumentIsNotNull(tweet, "tweet");

            await CheckAuthorizedAsync();

            var tweets = new List<Tweet>();
            var completionSource = new TaskCompletionSource<IEnumerable<Tweet>>();
            if (IsAuthorized)
            {
                Status status =
                    await UpdateStatusAsync(tweet.Text,
                                            (decimal) tweet.Latitude,
                                            (decimal) tweet.Longitude);
                if (status != null)
                {
                    tweets.Add(GetTweet(JToken.FromObject(status)));
                }

                completionSource.TrySetResult(tweets);
            }
            else
            {
                completionSource.TrySetException(new NotAuthorizedException());
            }

            return await completionSource.Task;
        }

        public override bool IsAuthorized
        {
            get { return Authorizer != null && Authorizer.IsAuthorized; }
        }

        public override Task AuthorizeAsync()
        {
            var completionSource = new TaskCompletionSource<bool>();
            _navigationService.Navigated +=
                delegate(object sender, NavigationEventArgs e)
                    {
                        if (e.NavigationMode == NavigationMode.Back)
                        {
                            completionSource.TrySetResult(IsAuthorized);
                        }
                    };
            _navigationService.UriFor<OAuthViewModel>().Navigate();
            return completionSource.Task;
        }

        private Task<Status> UpdateStatusAsync(string text, decimal latitude = 0, decimal longitude = 0)
        {
            var completionSource = new TaskCompletionSource<Status>();

            using (var context = new TwitterContext(Authorizer))
            {
                if (!Equals(latitude, 0M) && !Equals(longitude, 0M))
                {
                    context.UpdateStatus(text, latitude, longitude, true,
                                         delegate(TwitterAsyncResponse<Status> response)
                                             {
                                                 Status status = null;
                                                 if (response.Status == TwitterErrorStatus.Success)
                                                 {
                                                     status = response.State;
                                                 }

                                                 completionSource.TrySetResult(status);
                                             });
                }
                else
                {
                    context.UpdateStatus(text,
                                         delegate(TwitterAsyncResponse<Status> response)
                                             {
                                                 Status status = null;
                                                 if (response.Status == TwitterErrorStatus.Success)
                                                 {
                                                     status = response.State;
                                                 }

                                                 completionSource.TrySetResult(status);
                                             });
                }
            }

            return completionSource.Task;
        }

        public override async Task<List<Tweet>>
            SearchAsync(string query,
                        string geoCode = null,
                        ulong sinceID = 0,
                        ulong maxID = 0)
        {
            const int count = 100;

            await CheckAuthorizedAsync();

            var completionSource = new TaskCompletionSource<List<Tweet>>();
            if (Authorizer.IsAuthorized)
            {
                using (var ctx = new TwitterContext(Authorizer))
                {
                    IQueryable<Search> queryable =
                        ctx.Search.Where(search => search.Type == SearchType.Search &&
                                                   search.Query == query &&
                                                   search.ResultType == ResultType.Recent &&
                                                   search.Count == count);

                    if (!Equals(geoCode, "0,0,0km"))
                    {
                        queryable = queryable.Where(search => search.GeoCode == geoCode);
                    }

                    if (sinceID != 0)
                    {
                        queryable = queryable.Where(search => search.SinceID == sinceID);
                    }

                    if (maxID != 0)
                    {
                        queryable = queryable.Where(search => search.MaxID == maxID);
                    }

                    queryable.MaterializedAsyncCallback(delegate(TwitterAsyncResponse<IEnumerable<Search>> response)
                                                            {
                                                                if (response.Status == TwitterErrorStatus.Success)
                                                                {
                                                                    Search search = response.State.SingleOrDefault();

                                                                    if (search != null)
                                                                    {
                                                                        List<Tweet> tweets = GetTweets(search);

                                                                        completionSource.TrySetResult(tweets);
                                                                    }
                                                                    else
                                                                    {
                                                                        completionSource.TrySetResult(new List<Tweet>());
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (response.Exception != null)
                                                                    {
                                                                        completionSource.TrySetException(
                                                                            response.Exception);
                                                                    }
                                                                }
                                                            });
                }
            }
            else
            {
                completionSource.TrySetException(new NotAuthorizedException());
            }

            return await completionSource.Task;
        }

        public List<Tweet> GetTweets(Search search)
        {
            List<Status> statuses =
                search.Statuses.Where(x => !Equals(x.Coordinates.Latitude, 0.0d) &&
                                           !Equals(x.Coordinates.Longitude, 0.0d)).ToList();

            var tweets = new List<Tweet>();
            foreach (Status status in statuses)
            {
                Tweet tweet = GetTweet(JToken.FromObject(status));
                tweets.Add(tweet);
            }
            return tweets;
        }

        private async Task CheckAuthorizedAsync()
        {
            if (!IsAuthorized)
            {
                await AuthorizeAsync();
            }
        }
    }
}