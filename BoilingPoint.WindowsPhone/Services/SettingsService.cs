﻿namespace BoilingPoint.WindowsPhone.Services
{
    public class SettingsService : ISettingsService
    {
        private readonly Settings _settings = new Settings();

        public bool GeolocationEnabled
        {
            get { return _settings.GeolocationEnabled.HasValue && _settings.GeolocationEnabled.Value; }
            set { _settings.GeolocationEnabled = value; }
        }

        public bool SignatureEnabled
        {
            get { return _settings.SignatureEnabled; }
            set { _settings.SignatureEnabled = value; }
        }

        public string Signature
        {
            get { return _settings.Signature; }
            set { _settings.Signature = value; }
        }

        public string ApplicationName
        {
            get { return AppResources.ApplicationName; }
        }
    }
}