﻿using System;
using BoilingPoint.Portable.Services;
using Yandex.Metrica;

namespace BoilingPoint.WindowsPhone.Services
{
    public class YandexMetricaAnalyticalService : IAnalyticalService
    {
        public void ReportEvent(string eventName)
        {
            Counter.ReportEvent(eventName);
        }

        public void ReportError(Exception ex)
        {
            Counter.ReportError(ex.Message, ex);
        }
    }
}