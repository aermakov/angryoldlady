﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Services;

namespace BoilingPoint.WindowsPhone.Services
{
    public class GeocodeService : IGeocodeService
    {
        private readonly IGeocoder _geocoder;

        public GeocodeService(IGeocoder geocoder)
        {
            _geocoder = geocoder;
        }

        public Task<IList<GeoAddress>> GeocodeAsync(string address, string lang = "", string region = "")
        {
            return _geocoder.GeocodeAsync(address, lang, region);
        }

        public Task<IList<GeoAddress>> ReverseGeocodeAsync(GeoPoint geoPoint, string lang = "")
        {
            return _geocoder.ReverseGeocodeAsync(geoPoint, lang);
        }

        public Task<IList<GeoAddress>> ReverseGeocodeAsync(double latitude, double longitude, string lang = "")
        {
            return _geocoder.ReverseGeocodeAsync(latitude, longitude, lang);
        }

        public Task<string> GetCountryCodeAsync(double latitude, double longitude)
        {
            return _geocoder.GetCountryCodeAsync(latitude, longitude);
        }
    }
}