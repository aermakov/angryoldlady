﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoilingPoint.Portable;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.DoubleGis;
using BoilingPoint.Portable.PhoneNumbers;
using BoilingPoint.Portable.Services;
using Parse;
using Parse.Queries;
using GeoPoint = BoilingPoint.Portable.Geo.GeoPoint;

namespace BoilingPoint.WindowsPhone.Services
{
    public class DataService : Portable.Services.DataService
    {
        #region Fields

        private readonly Driver _driver = new Driver();
        private Config _config;

        #endregion

        public DataService(IGeolocationService geolocationService)
            : base(geolocationService)
        {
        }

        public override Task<Config> GetConfigAsync()
        {
            var completionSource = new TaskCompletionSource<Config>();
            if (_config == null)
            {
                _driver.Objects.Query<Config>()
                       .Where(x => x.AppBaseUrl != null)
                       .Execute(delegate(Response<ResultsResponse<Config>> response)
                                    {
                                        if (response.Success)
                                        {
                                            _config = response.Data.Results.FirstOrDefault();
                                            completionSource.TrySetResult(_config);
                                        }
                                        else
                                        {
                                            completionSource.TrySetException(response.Error.InnerException);
                                        }
                                    });
            }
            else
            {
                completionSource.TrySetResult(_config);
            }

            return completionSource.Task;
        }

        public override Task<List<Tag>> GetPredefinedTagsAsync()
        {
            var completionSource = new TaskCompletionSource<List<Tag>>();

            _driver.Objects.Query<Tag>().SortAscending(x => x.Name)
                   .Execute(delegate(Response<ResultsResponse<Tag>> response)
                                {
                                    if (response.Success)
                                    {
                                        IList<Tag> tags = response.Data.Results;
                                        completionSource.TrySetResult(new List<Tag>(tags));
                                    }
                                    else
                                    {
                                        completionSource.TrySetException(response.Error.InnerException);
                                    }
                                });

            return completionSource.Task;
        }

        public override Task<Bank> GetBankByIdAsync(string bankId)
        {
            var completionSource = new TaskCompletionSource<Bank>();

            _driver.Objects.Query<Bank>()
                   .Where(x => x.objectId == bankId)
                   .Execute(
                       delegate(Response<ResultsResponse<Bank>> response)
                           {
                               if (response.Success)
                               {
                                   Bank bank = response.Data.Results.FirstOrDefault();
                                   completionSource.TrySetResult(bank);
                               }
                               else
                               {
                                   completionSource.TrySetException(response.Error.InnerException);
                               }
                           });

            return completionSource.Task;
        }

        protected override async Task<Bank> GetBankByProfileAsync(Profile profile)
        {
            string webSite = Utils.NormalizeWebSite(profile.GetWebSite());
            Bank bank = await GetBankByWebSiteAsync(webSite);
            if (bank == null)
            {
                List<string> phones =
                    profile.Contacts.Where(x => x.Type == "phone")
                           .Select(x => PhoneNumberUtil.Normalize(x.Alias ?? x.Value)).ToList();

                bank = await GetBankByPhoneAsync(phones.ToArray());
            }
            return bank;
        }

        public Task<Bank> GetBankByWebSiteAsync(string webSite)
        {
            var completionSource = new TaskCompletionSource<Bank>();

            _driver.Objects.Query<Bank>()
                   .Where(x => x.WebSite == webSite)
                   .Execute(
                       delegate(Response<ResultsResponse<Bank>> response)
                           {
                               if (response.Success)
                               {
                                   Bank bank = response.Data.Results.FirstOrDefault();
                                   completionSource.TrySetResult(bank);
                               }
                               else
                               {
                                   completionSource.TrySetException(response.Error.InnerException);
                               }
                           });

            return completionSource.Task;
        }

        public Task<Bank> GetBankByPhoneAsync(string[] phones)
        {
            var completionSource = new TaskCompletionSource<Bank>();

            _driver.Objects.Query<Bank>()
                   .Where(x => phones.Contains(x.PhoneNumber))
                   .Execute(
                       delegate(Response<ResultsResponse<Bank>> response)
                           {
                               if (response.Success)
                               {
                                   Bank bank = response.Data.Results.FirstOrDefault();
                                   completionSource.TrySetResult(bank);
                               }
                               else
                               {
                                   completionSource.TrySetException(response.Error.InnerException);
                               }
                           });

            return completionSource.Task;
        }

        public override Task<int> GetBanksCountAsync(string countryCode = null)
        {
            var completionSource = new TaskCompletionSource<int>();

            IParseQuery<Bank> query = _driver.Objects.Query<Bank>().SortAscending(x => x.Title);
            if (countryCode != null)
            {
                countryCode = countryCode.ToLower();
                query = query.Where(x => x.CountryCode == countryCode);
            }

            query.Count().Execute(delegate(Response<ResultsResponse<Bank>> response)
                                      {
                                          if (response.Success)
                                          {
                                              completionSource.TrySetResult(response.Data.Count);
                                          }
                                          else
                                          {
                                              completionSource.TrySetException(response.Error.InnerException);
                                          }
                                      });

            return completionSource.Task;
        }

        public override Task<List<Bank>> GetBanksAsync(int page, int pageSize, string countryCode = null)
        {
            var completionSource = new TaskCompletionSource<List<Bank>>();

            IParseQuery<Bank> query =
                _driver.Objects.Query<Bank>().Skip(page*pageSize).Limit(pageSize).SortAscending(x => x.Title);
            if (countryCode != null)
            {
                countryCode = countryCode.ToLower();
                query = query.Where(x => x.CountryCode == countryCode);
            }

            query.Execute(delegate(Response<ResultsResponse<Bank>> response)
                              {
                                  if (response.Success)
                                  {
                                      completionSource.TrySetResult(new List<Bank>(response.Data.Results));
                                  }
                                  else
                                  {
                                      completionSource.TrySetException(response.Error.InnerException);
                                  }
                              });

            return completionSource.Task;
        }

        public override Task<List<Bank>> SearchBanksByTitleAsync(string title, string countryCode = null)
        {
            var completionSource = new TaskCompletionSource<List<Bank>>();

            string lowercaseHashtagTitle = Utils.GetLowerCaseHashtag(title);
            IParseQuery<Bank> query =
                _driver.Objects.Query<Bank>()
                       .Where(x => x.LowercaseHashtagTitle.Contains(lowercaseHashtagTitle))
                       .Limit(1000)
                       .SortAscending(x => x.Title);

            query.Execute(delegate(Response<ResultsResponse<Bank>> response)
                              {
                                  if (response.Success)
                                  {
                                      var results = new List<Bank>(response.Data.Results);
                                      if (countryCode != null)
                                      {
                                          countryCode = countryCode.ToLower();
                                          results = results.Where(x => x.CountryCode == countryCode).ToList();
                                      }

                                      completionSource.TrySetResult(results);
                                  }
                                  else
                                  {
                                      completionSource.TrySetException(response.Error.InnerException);
                                  }
                              });

            return completionSource.Task;
        }

        public override Task<List<Bank>> SearchBanksByHashtagTitleAsync(string hashtagTitle)
        {
            Guard.ArgumentIsNotNull(hashtagTitle, "hashtagTitle");

            var completionSource = new TaskCompletionSource<List<Bank>>();

            string lowercaseHashtagTitle = Utils.GetLowerCaseHashtag(hashtagTitle);

            IParseQuery<Bank> query =
                _driver.Objects.Query<Bank>()
                       .Where(x => x.LowercaseHashtagTitle == lowercaseHashtagTitle);

            query.Execute(delegate(Response<ResultsResponse<Bank>> response)
                              {
                                  if (response.Success)
                                  {
                                      completionSource.TrySetResult(new List<Bank>(response.Data.Results));
                                  }
                                  else
                                  {
                                      completionSource.TrySetException(response.Error.InnerException);
                                  }
                              });

            return completionSource.Task;
        }

        public override Task<List<Bank>> SearchBanksByTwitterUsernameAsync(string twitterUsername)
        {
            Guard.ArgumentIsNotNull(twitterUsername, "twitterUsername");

            var completionSource = new TaskCompletionSource<List<Bank>>();

            twitterUsername = twitterUsername.ToLower().Trim();

            IParseQuery<Bank> query =
                _driver.Objects.Query<Bank>()
                       .Where(x => x.TwitterUsername == twitterUsername);

            query.Execute(delegate(Response<ResultsResponse<Bank>> response)
                              {
                                  if (response.Success)
                                  {
                                      completionSource.TrySetResult(new List<Bank>(response.Data.Results));
                                  }
                                  else
                                  {
                                      completionSource.TrySetException(response.Error.InnerException);
                                  }
                              });

            return completionSource.Task;
        }

        public override Task<List<Tweet>>
            GetTweetsAsync(GeoPoint geoPoint = null, int radiusInMeters = 0, ulong sinceID = 0, ulong maxID = 0)
        {
            var completionSource = new TaskCompletionSource<List<Tweet>>();

            IParseQuery<Tweet> query = _driver.Objects.Query<Tweet>();

            if (sinceID != 0)
            {
                query = query.Where(x => x.ID > sinceID);
            }
            else if (maxID != 0)
            {
                query = query.Where(x => x.ID < maxID);
            }

            query.SortDescending(x => x.CreatedAt)
                 .Execute(delegate(Response<ResultsResponse<Tweet>> response)
                              {
                                  if (response.Success)
                                  {
                                      var tweets = new List<Tweet>(response.Data.Results);
                                      if (geoPoint != null && radiusInMeters != 0)
                                      {
                                          double radiusInKilometers = Distance.ToKilometers(radiusInMeters);
                                          Tweet[] array = tweets.ToArray();
                                          foreach (Tweet tweet in array)
                                          {
                                              double distance =
                                                  tweet.Location.DistanceBetween(geoPoint).ToKilometers().Value;
                                              if (distance > radiusInKilometers)
                                              {
                                                  tweets.Remove(tweet);
                                              }
                                          }
                                      }

                                      completionSource.TrySetResult(tweets);
                                  }
                                  else
                                  {
                                      completionSource.TrySetException(response.Error.InnerException);
                                  }
                              });

            return completionSource.Task;
        }

        public override Task<List<Tweet>> GetTweetsByBankIdAsync(string bankId)
        {
            var completionSource = new TaskCompletionSource<List<Tweet>>();

            IParseQuery<Tweet> query =
                _driver.Objects.Query<Tweet>()
                       .Where(x => x.BankId == bankId);

            query.SortDescending(x => x.CreatedAt)
                 .Execute(delegate(Response<ResultsResponse<Tweet>> response)
                              {
                                  if (response.Success)
                                  {
                                      completionSource.TrySetResult(new List<Tweet>(response.Data.Results));
                                  }
                                  else
                                  {
                                      completionSource.TrySetException(response.Error.InnerException);
                                  }
                              });

            return completionSource.Task;
        }

        public override Task<List<Tweet>> GetTweetsByProfileIdAsync(string profileId)
        {
            var completionSource = new TaskCompletionSource<List<Tweet>>();

            IParseQuery<Tweet> query =
                _driver.Objects.Query<Tweet>()
                       .Where(x => x.ProfileId == profileId);

            query.SortDescending(x => x.CreatedAt)
                 .Execute(delegate(Response<ResultsResponse<Tweet>> response)
                              {
                                  if (response.Success)
                                  {
                                      completionSource.TrySetResult(new List<Tweet>(response.Data.Results));
                                  }
                                  else
                                  {
                                      completionSource.TrySetException(response.Error.InnerException);
                                  }
                              });

            return completionSource.Task;
        }
    }
}