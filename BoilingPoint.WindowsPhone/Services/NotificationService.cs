﻿using System;
using System.Globalization;
using System.IO;
using System.Xml.Linq;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Events;
using BoilingPoint.Portable.Services;
using BoilingPoint.WindowsPhone.Helpers;
using Microsoft.Phone.Notification;

namespace BoilingPoint.WindowsPhone.Services
{
    public class NotificationService : BaseNotificationService, IDisposable
    {
        #region Fields

        private const string CHANNEL_NAME = "BoilingPointChannel";

        private HttpNotificationChannel _pushChannel;

        #endregion

        #region Ctors

        public NotificationService()
        {
            InitializeChannel();
        }

        private void InitializeChannel()
        {
            // Try to find the push channel.
            _pushChannel = HttpNotificationChannel.Find(CHANNEL_NAME);

            // If the channel was not found, then create a new connection to the push service.
            if (_pushChannel == null)
            {
                _pushChannel = new HttpNotificationChannel(CHANNEL_NAME);

                // Register for all the events before attempting to open the channel.
                SubscribeToChannelEvents();

                _pushChannel.Open();
            }
            else
            {
                SubscribeToChannelEvents();
            }
        }

        private void SubscribeToChannelEvents()
        {
            _pushChannel.ChannelUriUpdated += OnPushChannelChannelUriUpdated;
            _pushChannel.HttpNotificationReceived += OnPushChannelHttpNotificationReceived;
        }

        #endregion

        #region Properties

        protected Uri ChannelUri
        {
            get { return _pushChannel.ChannelUri; }
        }

        #endregion

        #region Methods

        public override void RegisterDevice(Device device)
        {
            OnDeviceRegistered(this, EventArgs.Empty);
        }

        #endregion

        #region Handlers

        private void OnPushChannelChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
        {
            var device =
                new Device
                    {
                        Id = DeviceInfoHelper.GetDeviceId(),
                        ChannelUrl = e.ChannelUri.ToString(),
                        PlatformType = PlatformType.WindowsPhone
                    };

            RegisterDevice(device);
        }

        private void OnPushChannelHttpNotificationReceived(object sender, HttpNotificationEventArgs e)
        {
            string message;
            using (var reader = new StreamReader(e.Notification.Body))
            {
                message = reader.ReadToEnd();
            }

            XDocument document = XDocument.Parse(message);
            NotificationReceivedEventArgs args = ParseNotification(document);
            OnNotificationReceived(this, args);
        }

        private NotificationReceivedEventArgs ParseNotification(XDocument document)
        {
            var notificationType =
                (NotificationType) Enum.Parse(typeof (NotificationType),
                                              GetElementValue(document, "NotificationType", string.Empty), true);
            NotificationData notificationData = GetNotificationData(notificationType);
            var e = new NotificationReceivedEventArgs
                        {
                            NotificationType = notificationType,
                            NotificationData = notificationData
                        };
            return e;
        }

        private static NotificationData GetNotificationData(NotificationType notificationType)
        {
            NotificationData notificationData = null;
            switch (notificationType)
            {
                case NotificationType.Unknown:
                    break;
            }
            return notificationData;
        }

        private T GetElementValue<T>(XDocument document, XName name, T defaultValue)
        {
            if (document.Root != null)
            {
                XElement element = document.Root.Element(name);
                if (element != null)
                {
                    return (T) Convert.ChangeType(element.Value, typeof (T), CultureInfo.InvariantCulture);
                }
            }
            return defaultValue;
        }

        #endregion

        public void Dispose()
        {
            if (_pushChannel != null)
            {
                _pushChannel.Dispose();
            }
        }
    }
}