﻿namespace BoilingPoint.WindowsPhone.Services
{
    public interface ISettingsService
    {
        string ApplicationName { get; }

        bool GeolocationEnabled { get; set; }
        bool SignatureEnabled { get; set; }
        string Signature { get; set; }
    }
}