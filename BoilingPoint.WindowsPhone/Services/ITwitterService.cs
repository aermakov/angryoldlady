﻿using LinqToTwitter;

namespace BoilingPoint.WindowsPhone.Services
{
    public interface ITwitterService : Portable.Services.ITwitterService
    {
        ITwitterAuthorizer Authorizer { get; set; }
    }
}