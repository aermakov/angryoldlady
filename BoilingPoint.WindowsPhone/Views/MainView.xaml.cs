﻿using Telerik.Windows.Controls;

namespace BoilingPoint.WindowsPhone.Views
{
    public partial class MainView
    {
        public MainView()
        {
            PrimitivesLocalizationManager.Instance.ResourceManager = LocalizedResources.ResourceManager;
            InputLocalizationManager.Instance.ResourceManager = LocalizedResources.ResourceManager;

            InitializeComponent();
        }
    }
}