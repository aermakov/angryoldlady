﻿using System.Web.Mvc;
using System.Web.Routing;

namespace BoilingPoint.Web.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Twitter", "twitter/sendtweet/{status}/{latitude}/{longitude}",
                            new {controller = "Twitter", action = "SendTweet"});

            routes.MapRoute("Default", "{controller}/{action}/{id}",
                            new {controller = "Home", action = "Index", id = UrlParameter.Optional});
        }
    }
}