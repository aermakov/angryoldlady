﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace BoilingPoint.Web
{
    public class JsonResult : ActionResult
    {
        public JsonResult()
        {
            Settings = new JsonSerializerSettings();
        }

        public Encoding ContentEncoding { get; set; }
        public string ContentType { get; set; }
        public object Data { get; set; }

        public JsonSerializerSettings Settings { get; set; }
        public Formatting Formatting { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            HttpResponseBase response = context.HttpContext.Response;

            response.ContentType = !string.IsNullOrEmpty(ContentType)
                                       ? ContentType
                                       : "application/json";

            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;

            if (Data == null) return;
            using (var writer = new JsonTextWriter(response.Output) {Formatting = Formatting})
            {
                JsonSerializer serializer = JsonSerializer.Create(Settings);
                serializer.Serialize(writer, Data);

                writer.Flush();
            }
        }
    }
}