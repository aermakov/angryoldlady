﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using BoilingPoint.Portable.Constants;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Services;
using BoilingPoint.Web.Helpers;
using BoilingPoint.Web.Services;
using Newtonsoft.Json;
using Parse;
using DataService = BoilingPoint.Web.Services.DataService;
using ITwitterService = BoilingPoint.Web.Services.ITwitterService;
using TwitterService = BoilingPoint.Portable.Services.TwitterService;

namespace BoilingPoint.Web.Controllers
{
    public class TwitterController : Controller
    {
        #region Fields

        private const string QUERY = "#ihatetowait";

        private readonly IDataService _dataService = new DataService(null);
        private readonly IGeocodeService _geocodeService = new GeocodeService();
        private readonly TwitterService _portableTwitterService;
        private readonly ITwitterService _twitterService = new Services.TwitterService();

        #endregion

        #region Ctors

        public TwitterController()
        {
            _portableTwitterService =
                new TwitterService(_geocodeService, _dataService) {Cache = new Cache()};
        }

        #endregion

        #region Methods

        [HttpGet]
        [OutputCache(Duration = 300, Location = OutputCacheLocation.Server, VaryByCustom = "GeoCodeSinceIDMaxID")]
        public async Task<ActionResult> Search(string q, string geoCode = null, ulong sinceID = 0, ulong maxID = 0)
        {
            List<Tweet> tweets = await SearchInternal(q, geoCode, sinceID, maxID);

            return Result(tweets);
        }

        public ActionResult Users(string userScreenNames)
        {
            List<User> users = _twitterService.GetUsers(userScreenNames);

            return Result(users);
        }

        public async Task<ActionResult> ScheduledUpdate()
        {
            ulong lastID = 0;
            ParseObject lastTweetParseObject =
                await ParseObject.GetQuery("Tweet").OrderByDescending("ID").FirstOrDefaultAsync();
            if (lastTweetParseObject != null)
            {
                lastID = lastTweetParseObject.Get<ulong>("ID");
            }

            ulong sinceID = lastID;

            Blacklist blacklist = await GetBlacklistAsync();
            List<Tweet> tweets = await SearchInternal(QUERY, null, sinceID, 0);
            Tweet[] array = tweets.ToArray();
            var objects = new List<ParseObject>();
            for (int i = 0; i < array.Length; i++)
            {
                Tweet tweet = array[i];
                if (!IsInBlacklist(blacklist, tweet.UserScreenName))
                {
                    objects.Add(ParseHelper.GetParseObject(tweet));
                }
                else
                {
                    tweets.Remove(tweet);
                }
            }

            await ParseObject.SaveAllAsync(objects);

            return Result(tweets);
        }

        [HttpGet]
        public async Task<ActionResult> SendTweet(string status, double latitude, double longitude)
        {
            var tweet = new Tweet {Text = status, ID = (ulong) DateTime.UtcNow.Ticks};

            if (!Equals(latitude, 0d) && (!Equals(longitude, 0d)))
            {
                tweet.Latitude = latitude;
                tweet.Longitude = longitude;
            }

            IEnumerable<Tweet> tweets = await _twitterService.SendTweetAsync(tweet);

            return Result(tweets.ToList());
        }

        private bool IsInBlacklist(IEnumerable<BlacklistItem> blacklist, string userScreenName)
        {
            foreach (BlacklistItem blacklistItem in blacklist)
            {
                if (string.Equals(blacklistItem.UserScreenName, userScreenName,
                                  StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

        private async Task<Blacklist> GetBlacklistAsync()
        {
            var blacklist = new Blacklist();
            blacklist.AddRange((await ParseObject.GetQuery("BlacklistItem").FindAsync())
                                   .Select(ParseHelper.GetObject<BlacklistItem>));
            return blacklist;
        }

        private static JsonResult Result<T>(List<T> list)
        {
            var settings = new JsonSerializerSettings {DefaultValueHandling = DefaultValueHandling.Ignore};
            var result = new JsonResult {Data = list, Settings = settings};
            return result;
        }

        private async Task<List<Tweet>> SearchInternal(string q, string geoCode, ulong sinceID, ulong maxID)
        {
            List<Tweet> tweets = await _twitterService.SearchAsync(q, geoCode, sinceID, maxID);

            foreach (Tweet tweet in tweets)
            {
                if (!Equals(tweet.Latitude, 0d) && !Equals(tweet.Longitude))
                {
                    var geoPoint = new GeoPoint(tweet.Latitude, tweet.Longitude);

                    tweet.CountryCode = await GetCountryCodeAsync(geoPoint);

                    GeoAddress geoAddress = await GetGeoAddressAsync(geoPoint);
                    if (geoAddress != null)
                    {
                        tweet.Address = geoAddress.FormattedAddress;
                    }

                    await _portableTwitterService.InitializeProfileAsync(tweet, geoPoint);    
                }

                tweet.Bank = await GetBankAsync(tweet);
            }

            return tweets;
        }

        private async Task<Bank> GetBankAsync(Tweet tweet)
        {
            string countryCode = null;
            if (!Equals(tweet.Latitude, 0d) && !Equals(tweet.Longitude))
            {
                var geoPoint = new GeoPoint(tweet.Latitude, tweet.Longitude);
                countryCode = await _geocodeService.GetCountryCodeAsync(geoPoint.Latitude, geoPoint.Longitude);
            }

            Bank bank = await _portableTwitterService.SearchBankByUsersAsync(tweet.Users, countryCode);
            if (bank == null)
            {
                bank = await _portableTwitterService.SearchBankByTagsAsync(tweet.Tags, countryCode);
            }

            if (bank == null && tweet.Profile != null)
            {
                bank = await _portableTwitterService.SearchBankByProfileAsync(tweet.Profile);
            }

            return bank;
        }

        private async Task<string> GetCountryCodeAsync(GeoPoint geoPoint)
        {
            int hashCode = geoPoint.GetHashCode();
            string key = string.Format("countryCode{0}", hashCode.ToString(CultureInfo.InvariantCulture));
            var cache = new Cache();
            var countryCode = cache.Get<string>(key);
            if (countryCode == null)
            {
                countryCode = await _geocodeService.GetCountryCodeAsync(geoPoint.Latitude, geoPoint.Longitude);
                if (countryCode != null)
                {
                    cache.Add(key, countryCode, CacheConstants.NoAbsoluteExpiration,
                              CacheConstants.NoSlidingExpiration);
                }
            }
            return countryCode;
        }

        private async Task<GeoAddress> GetGeoAddressAsync(GeoPoint geoPoint)
        {
            int hashCode = geoPoint.GetHashCode();
            string key = string.Format("geoAddress{0}", hashCode.ToString(CultureInfo.InvariantCulture));
            var cache = new Cache();
            var geoAddress = cache.Get<GeoAddress>(key);
            if (geoAddress == null)
            {
                IList<GeoAddress> geoAddresses = await _geocodeService.ReverseGeocodeAsync(geoPoint);
                geoAddress = geoAddresses.FirstOrDefault();
                if (geoAddress != null)
                {
                    cache.Add(key, geoAddress, CacheConstants.NoAbsoluteExpiration,
                              CacheConstants.NoSlidingExpiration);
                }
            }
            return geoAddress;
        }

        #endregion
    }
}