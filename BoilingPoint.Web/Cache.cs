﻿using System;
using System.Web;
using System.Web.Caching;
using BoilingPoint.Portable;

namespace BoilingPoint.Web
{
    public class Cache : ICache
    {
        public void Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration)
        {
            HttpContext.Current.Cache.Add(key, value, null, absoluteExpiration, slidingExpiration,
                                          CacheItemPriority.Normal, null);
        }

        public bool Contains(string key)
        {
            return HttpContext.Current.Cache.Get(key) != null;
        }

        public void Remove(string key)
        {
            HttpContext.Current.Cache.Remove(key);
        }

        public T Get<T>(string key)
        {
            return (T) HttpContext.Current.Cache.Get(key);
        }
    }
}