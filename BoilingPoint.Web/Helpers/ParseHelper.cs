﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BoilingPoint.Portable.Attributes;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Geo;
using Parse;

namespace BoilingPoint.Web.Helpers
{
    public static class ParseHelper
    {
        public static T GetObject<T>(ParseObject parseObject) where T : BaseEntity
        {
            if (parseObject != null)
            {
                Type type = typeof (T);
                var t = (T) Activator.CreateInstance(type);

                t.objectId = parseObject.ObjectId;
                t.updatedAt = parseObject.UpdatedAt;
                t.createdAt = parseObject.CreatedAt;

                UpdateObject(t, parseObject);

                return t;
            }

            return default(T);
        }

        public static ParseObject GetParseObject<T>(T data) where T : BaseEntity
        {
            Type type = typeof (T);
            string className = type.Name;
            var parseObject = new ParseObject(className) {ObjectId = data.objectId};

            UpdateParseObject(parseObject, data);

            return parseObject;
        }

        private static void UpdateObject<T>(T t, IEnumerable<KeyValuePair<string, object>> parseObject)
        {
            Type type = typeof (T);
            PropertyInfo[] properties = type.GetProperties();
            foreach (var pair in parseObject)
            {
                PropertyInfo property = properties.SingleOrDefault(x => x.Name == pair.Key);
                if (property == null || property.GetSetMethod() == null)
                    continue;

                object value = pair.Value;
                if (value != null && property.PropertyType.IsEnum)
                {
                    value = Enum.Parse(property.PropertyType, value.ToString());
                }

                if (value is ParseGeoPoint)
                {
                    value = new GeoPoint(((ParseGeoPoint) value).Latitude, ((ParseGeoPoint) value).Longitude);
                }

                SetValue(t, property.Name, value);
            }
        }

        public static void SetValue(object inputObject, string propertyName, object propertyVal)
        {
            // Find out the type
            Type type = inputObject.GetType();

            // Get the property information based on the type
            PropertyInfo propertyInfo = type.GetProperty(propertyName);

            // Find the property type
            Type propertyType = propertyInfo.PropertyType;

            // Convert.ChangeType does not handle conversion to nullable types
            // if the property type is nullable, we need to get the underlying type of the property
            Type targetType = IsNullableType(propertyType) ? Nullable.GetUnderlyingType(propertyType) : propertyType;

            if (propertyVal is List<object>)
            {
                var list = (List<object>) propertyVal;
                object target = Activator.CreateInstance(targetType);
                for (int i = 0; i < list.Count; i++)
                {
                    object value = list[i];
                    value = Convert.ChangeType(value, targetType.GenericTypeArguments[0]);

                    propertyType.GetMethod("Add").Invoke(target, new[] {value});
                }

                SetValue(inputObject, propertyName, target);
            }
            else
            {
                // Returns an System.Object with the specified System.Type and whose value is
                // equivalent to the specified object.
                propertyVal = Convert.ChangeType(propertyVal, targetType);

                // Set the value of the property
                propertyInfo.SetValue(inputObject, propertyVal, null);
            }
        }

        private static bool IsNullableType(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof (Nullable<>);
        }

        public static void UpdateParseObject<T>(ParseObject parseObject, T data)
        {
            Type type = typeof (T);
            PropertyInfo[] properties = type.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                var attribute = property.GetCustomAttribute<IgnoreAttribute>();
                if (attribute != null)
                    continue;

                object value = property.GetValue(data);
                if (value is GeoPoint)
                {
                    var geoPoint = (GeoPoint) value;
                    value = new ParseGeoPoint(geoPoint.Latitude, geoPoint.Longitude);
                }

                if (property.PropertyType.IsEnum)
                    value = value.ToString();

                parseObject[property.Name] = value;
            }
        }
    }
}