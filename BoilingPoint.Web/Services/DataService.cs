﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoilingPoint.Portable;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.DoubleGis;
using BoilingPoint.Portable.Services;
using BoilingPoint.Web.Helpers;
using Parse;

namespace BoilingPoint.Web.Services
{
    public class DataService : Portable.Services.DataService
    {
        public DataService(IGeolocationService geolocationService)
            : base(geolocationService)
        {
        }

        public override async Task<List<Bank>> SearchBanksByHashtagTitleAsync(string hashtagTitle)
        {
            Guard.ArgumentIsNotNull(hashtagTitle, "hashtagTitle");

            string lowercaseHashtagTitle = Utils.GetLowerCaseHashtag(hashtagTitle);
            ParseQuery<ParseObject> query =
                ParseObject.GetQuery("Bank").WhereEqualTo("LowercaseHashtagTitle", lowercaseHashtagTitle);
            return (await query.FindAsync()).Select(ParseHelper.GetObject<Bank>).ToList();
        }

        public override async Task<List<Bank>> SearchBanksByTwitterUsernameAsync(string twitterUsername)
        {
            Guard.ArgumentIsNotNull(twitterUsername, "twitterUsername");

            twitterUsername = twitterUsername.ToLower().Trim();
            ParseQuery<ParseObject> query =
                ParseObject.GetQuery("Bank").WhereEqualTo("TwitterUsername", twitterUsername);
            return (await query.FindAsync()).Select(ParseHelper.GetObject<Bank>).ToList();
        }

        public override Task<Bank> GetBankByIdAsync(string bankId)
        {
            throw new NotImplementedException();
        }

        public override Task<int> GetBanksCountAsync(string countryCode = null)
        {
            throw new NotImplementedException();
        }

        public override Task<List<Tweet>>
            GetTweetsAsync(GeoPoint geoPoint = null, int radiusInMeters = 0, ulong sinceID = 0, ulong maxID = 0)
        {
            throw new NotImplementedException();
        }

        public override Task<List<Tweet>> GetTweetsByBankIdAsync(string bankId)
        {
            throw new NotImplementedException();
        }

        public override Task<List<Tweet>> GetTweetsByProfileIdAsync(string profileId)
        {
            throw new NotImplementedException();
        }

        public override Task<Config> GetConfigAsync()
        {
            throw new NotImplementedException();
        }

        public override async Task<List<Tag>> GetPredefinedTagsAsync()
        {
            return (await ParseObject.GetQuery("Tag").FindAsync()).Select(ParseHelper.GetObject<Tag>).ToList();
        }

        public override Task<List<Bank>> GetBanksAsync(int page, int pageSize, string countryCode = null)
        {
            throw new NotImplementedException();
        }

        public override Task<List<Bank>> SearchBanksByTitleAsync(string title, string countryCode = null)
        {
            throw new NotImplementedException();
        }

        protected override Task<Bank> GetBankByProfileAsync(Profile profile)
        {
            throw new NotImplementedException();
        }
    }
}