﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BoilingPoint.Portable.Data;

namespace BoilingPoint.Web.Services
{
    public interface ITwitterService
    {
        Task<IEnumerable<Tweet>> SendTweetAsync(Tweet tweet);

        Task<List<Tweet>> SearchAsync(string query, string geoCode = null, ulong sinceID = 0, ulong maxID = 0);

        List<User> GetUsers(string userScreenNames);
    }
}