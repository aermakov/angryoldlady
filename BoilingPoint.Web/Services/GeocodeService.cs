﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.Yandex;
using BoilingPoint.Portable.Services;

namespace BoilingPoint.Web.Services
{
    public class GeocodeService : IGeocodeService
    {
        #region Fields

        private readonly IGeocoder _geocoder;

        #endregion

        #region Ctors

        public GeocodeService()
        {
            _geocoder = new YandexGeocoder();
        }

        #endregion

        #region Methods

        public Task<IList<GeoAddress>> GeocodeAsync(string address, string lang = "", string region = "")
        {
            return _geocoder.GeocodeAsync(address, lang, region);
        }

        public Task<IList<GeoAddress>> ReverseGeocodeAsync(GeoPoint geoPoint, string lang = "")
        {
            return _geocoder.ReverseGeocodeAsync(geoPoint, lang);
        }

        public Task<IList<GeoAddress>> ReverseGeocodeAsync(double latitude, double longitude, string lang = "")
        {
            return _geocoder.ReverseGeocodeAsync(latitude, longitude, lang);
        }

        public Task<string> GetCountryCodeAsync(double latitude, double longitude)
        {
            return _geocoder.GetCountryCodeAsync(latitude, longitude);
        }

        #endregion
    }
}