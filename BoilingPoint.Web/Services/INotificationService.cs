﻿using System.Collections.Generic;
using BoilingPoint.Web.Services.WindowsPhone;

namespace BoilingPoint.Web.Services
{
    public interface INotificationService
    {
        SendNotificationResponse Send(IDictionary<string, object> parameters);
    }
}