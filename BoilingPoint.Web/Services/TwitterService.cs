﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using BoilingPoint.Portable;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Exceptions;
using LinqToTwitter;
using Newtonsoft.Json.Linq;
using User = BoilingPoint.Portable.Data.User;

namespace BoilingPoint.Web.Services
{
    public class TwitterService : ITwitterService
    {
        private ITwitterAuthorizer _authorizer;

        public async Task<IEnumerable<Tweet>> SendTweetAsync(Tweet tweet)
        {
            Guard.ArgumentIsNotNull(tweet, "tweet");

            CheckAuthorized();

            var tweets = new List<Tweet>();
            var completionSource = new TaskCompletionSource<IEnumerable<Tweet>>();
            if (_authorizer.IsAuthorized)
            {
                Status status =
                    await UpdateStatusAsync(tweet.Text,
                                            (decimal) tweet.Latitude,
                                            (decimal) tweet.Longitude);
                if (status != null)
                {
                    tweets.Add(new Tweet(JToken.FromObject(status)));
                }

                completionSource.TrySetResult(tweets);
            }
            else
            {
                completionSource.TrySetException(new NotAuthorizedException());
            }

            return await completionSource.Task;
        }

        public Task<List<Tweet>>
            SearchAsync(string query, string geoCode = null, ulong sinceID = 0, ulong maxID = 0)
        {
            const int count = 100;

            CheckAuthorized();

            var completionSource = new TaskCompletionSource<List<Tweet>>();
            if (_authorizer.IsAuthorized)
            {
                using (var ctx = new TwitterContext(_authorizer))
                {
                    IQueryable<Search> queryable =
                        ctx.Search.Where(search => search.Type == SearchType.Search &&
                                                   search.Query == query &&
                                                   search.ResultType == ResultType.Recent &&
                                                   search.Count == count);

                    if (!string.IsNullOrEmpty(geoCode))
                    {
                        queryable = queryable.Where(search => search.GeoCode == geoCode);
                    }

                    if (sinceID != 0)
                    {
                        queryable = queryable.Where(search => search.SinceID == sinceID);
                    }

                    if (maxID != 0)
                    {
                        queryable = queryable.Where(search => search.MaxID == maxID);
                    }

                    queryable.MaterializedAsyncCallback(delegate(TwitterAsyncResponse<IEnumerable<Search>> response)
                                                            {
                                                                if (response.Status == TwitterErrorStatus.Success)
                                                                {
                                                                    Search search = response.State.SingleOrDefault();

                                                                    if (search != null)
                                                                    {
                                                                        List<Tweet> tweets = GetTweets(search);

                                                                        completionSource.TrySetResult(tweets);
                                                                    }
                                                                    else
                                                                    {
                                                                        completionSource.TrySetResult(
                                                                            new List<Tweet>());
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (response.Exception != null)
                                                                    {
                                                                        completionSource.TrySetException(
                                                                            response.Exception);
                                                                    }
                                                                }
                                                            });
                }
            }
            else
            {
                completionSource.TrySetResult(new List<Tweet>());
            }

            return completionSource.Task;
        }

        public List<User> GetUsers(string userScreenNames)
        {
            CheckAuthorized();

            var portableUsers = new List<User>();
            using (var context = new TwitterContext(_authorizer))
            {
                foreach (string userScreenName in userScreenNames.Split(new[] {','}))
                {
                    string name = userScreenName;
                    List<LinqToTwitter.User> users =
                        (context.User.Where(user => user.Type == UserType.Lookup &&
                                                    user.ScreenName == name)).ToList();

                    foreach (LinqToTwitter.User user in users)
                    {
                        portableUsers.Add(new User
                                              {
                                                  Name = user.Name,
                                                  ScreenName = user.ScreenName,
                                                  ProfileImageUrl = user.ProfileImageUrl
                                              });
                    }
                }
            }

            return portableUsers;
        }

        private Task<Status> UpdateStatusAsync(string text, decimal latitude = 0, decimal longitude = 0)
        {
            var completionSource = new TaskCompletionSource<Status>();

            using (var context = new TwitterContext(_authorizer))
            {
                if (!Equals(latitude, 0M) && !Equals(longitude, 0M))
                {
                    context.UpdateStatus(text, latitude, longitude, true,
                                         delegate(TwitterAsyncResponse<Status> response)
                                             {
                                                 Status status = null;
                                                 if (response.Status == TwitterErrorStatus.Success)
                                                 {
                                                     status = response.State;
                                                 }

                                                 completionSource.TrySetResult(status);
                                             });
                }
                else
                {
                    context.UpdateStatus(text,
                                         delegate(TwitterAsyncResponse<Status> response)
                                             {
                                                 Status status = null;
                                                 if (response.Status == TwitterErrorStatus.Success)
                                                 {
                                                     status = response.State;
                                                 }

                                                 completionSource.TrySetResult(status);
                                             });
                }
            }

            return completionSource.Task;
        }

        private List<Tweet> GetTweets(Search search)
        {
            var tweets = new List<Tweet>();
            foreach (Status status in search.Statuses)
            {
                JToken token = JToken.FromObject(status);
                var tweet = (Tweet) Activator.CreateInstance(typeof (Tweet), token);

                if ((!Equals(tweet.Latitude, 0.0d) && !Equals(tweet.Longitude, 0.0d)) ||
                    (tweet.CashBoxAvailable > 0 || tweet.CashBoxClosed > 0 || tweet.PeopleInline > 0 ||
                     tweet.WaitTime > 0))
                {
                    tweets.Add(tweet);
                }
            }

            return tweets;
        }

        private void Authorize()
        {
            string consumerKey = ConfigurationManager.AppSettings["TwitterConsumerKey"];
            string consumerSecret = ConfigurationManager.AppSettings["TwitterConsumerSecret"];
            string oauthToken = ConfigurationManager.AppSettings["TwitterOAuthToken"];
            string accessToken = ConfigurationManager.AppSettings["TwitterAccessToken"];

            _authorizer =
                new SingleUserAuthorizer
                    {
                        Credentials =
                            new InMemoryCredentials
                                {
                                    ConsumerKey = consumerKey,
                                    ConsumerSecret = consumerSecret,
                                    OAuthToken = oauthToken,
                                    AccessToken = accessToken
                                }
                    };

            _authorizer.Authorize();
        }

        private void CheckAuthorized()
        {
            if (_authorizer == null || !_authorizer.IsAuthorized)
            {
                Authorize();
            }
        }
    }
}