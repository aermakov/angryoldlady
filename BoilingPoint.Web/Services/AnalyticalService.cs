﻿using System;
using System.Collections.Generic;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Services;
using BoilingPoint.Web.Helpers;
using Parse;

namespace BoilingPoint.Web.Services
{
    public class AnalyticalService : IAnalyticalService
    {
        public void Debug(string format, params object[] args)
        {
            ReportEvent((object) string.Format(format, args));
        }

        public void ReportEvent(object value)
        {
            var logItem = new LogItem(value.ToString());
            ParseObject.SaveAllAsync(new[] {ParseHelper.GetParseObject(logItem)});
        }

        public void Error(Exception ex, Dictionary<string, string> logExtra = null)
        {
            throw new NotImplementedException();
        }
    }
}