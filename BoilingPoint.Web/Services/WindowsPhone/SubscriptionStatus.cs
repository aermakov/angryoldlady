﻿namespace BoilingPoint.Web.Services.WindowsPhone
{
    public enum SubscriptionStatus
    {
        Active,
        Expired
    }
}