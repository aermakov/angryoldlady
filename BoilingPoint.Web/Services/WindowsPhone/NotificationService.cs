﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace BoilingPoint.Web.Services.WindowsPhone
{
    public class NotificationService
    {
        private readonly string _channelUriString;

        public NotificationService(string channelUriString)
        {
            _channelUriString = channelUriString;
        }

        public SendNotificationResponse Send(IDictionary<string, object> parameters)
        {
            var builder = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            builder.Append("<root>");

            foreach (var pair in parameters)
            {
                builder.AppendFormat("<{0}>{1}</{0}>", pair.Key.Trim(), pair.Value);
            }

            builder.Append("</root>");

            return Send(builder.ToString());
        }

        private SendNotificationResponse Send(string payload)
        {
            SendNotificationResponse notificationResponse;
            using (var webClient = new WebClient())
            {
                webClient.Headers.Add(HttpRequestHeader.ContentType, "text/xml");
                webClient.Headers.Add("X-NotificationClass", "3");

                var address = new Uri(_channelUriString);
                byte[] data = Encoding.UTF8.GetBytes(payload);
                webClient.UploadData(address, "POST", data);

                notificationResponse =
                    new SendNotificationResponse
                        {
                            NotificationStatus =
                                ParseEnum<NotificationStatus>(webClient.ResponseHeaders["X-NotificationStatus"]),
                            SubscriptionStatus =
                                ParseEnum<SubscriptionStatus>(webClient.ResponseHeaders["X-SubscriptionStatus"]),
                            DeviceConnectionStatus =
                                ParseEnum<DeviceConnectionStatus>(webClient.ResponseHeaders["X-DeviceConnectionStatus"])
                        };
            }

            return notificationResponse;
        }

        private T ParseEnum<T>(string value)
            where T : struct
        {
            T t;
            if (Enum.TryParse(value.Replace(" ", string.Empty), out t))
            {
                return t;
            }
            return default(T);
        }
    }
}