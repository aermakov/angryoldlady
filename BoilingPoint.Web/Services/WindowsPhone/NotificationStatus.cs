﻿namespace BoilingPoint.Web.Services.WindowsPhone
{
    public enum NotificationStatus
    {
        Received,
        QueueFull,
        Suppressed,
        Dropped,
    }
}