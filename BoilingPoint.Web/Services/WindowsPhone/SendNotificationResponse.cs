﻿namespace BoilingPoint.Web.Services.WindowsPhone
{
    public class SendNotificationResponse
    {
        public NotificationStatus NotificationStatus { get; set; }
        public SubscriptionStatus SubscriptionStatus { get; set; }
        public DeviceConnectionStatus DeviceConnectionStatus { get; set; }
    }
}