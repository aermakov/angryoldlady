﻿namespace BoilingPoint.Web.Services.WindowsPhone
{
    public enum DeviceConnectionStatus
    {
        Connected,
        TempDisconnected,
        Disconnected,
        Inactive
    }
}