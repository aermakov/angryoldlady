﻿using System.Configuration;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Geo.DoubleGis;
using BoilingPoint.Web.App_Start;
using BoilingPoint.Web.Helpers;
using Parse;

namespace BoilingPoint.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : HttpApplication
    {
        protected async void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            RouteConfig.RegisterRoutes(RouteTable.Routes);

            ParseClient.Initialize(ConfigurationManager.AppSettings["ParseApplicationId"],
                                   ConfigurationManager.AppSettings["ParseWindowsKey"]);

            await InitializeDoubleGisAsync();
        }

        public async Task<Config> GetConfigAsync()
        {
            Config config = null;
            var query = new ParseQuery<ParseObject>("Config");
            ParseObject parseObject = await query.WhereNotEqualTo("AppBaseUrl", null).FirstOrDefaultAsync();

            if (parseObject != null)
            {
                config = ParseHelper.GetObject<Config>(parseObject);
            }
            return config;
        }

        private async Task InitializeDoubleGisAsync()
        {
            Config config = await GetConfigAsync();

            DoubleGis.Initialize(config.DoubleGisApiKey);
        }

        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            if (custom == "GeoCodeSinceIDMaxID")
            {
                string customString =
                    string.Format("{0}{1}{2}",
                                  GetQueryParameter(context, "geoCode", "0,0,0km"),
                                  GetQueryParameter(context, "sinceID", "0"),
                                  GetQueryParameter(context, "maxID", "0"));
                return customString;
            }
            return base.GetVaryByCustomString(context, custom);
        }

        private string GetQueryParameter(HttpContext context, string name, string defaultValue)
        {
            string value = context.Request.QueryString[name];
            return value ?? defaultValue;
        }
    }
}