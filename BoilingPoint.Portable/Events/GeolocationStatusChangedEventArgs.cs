﻿using System;
using BoilingPoint.Portable.Enums;

namespace BoilingPoint.Portable.Events
{
    public class GeolocationStatusChangedEventArgs : EventArgs
    {
        public GeolocationStatus Status { get; set; }
    }
}