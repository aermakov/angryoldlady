﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Events;
using BoilingPoint.Portable.Geo;
using Newtonsoft.Json.Linq;

namespace BoilingPoint.Portable.Services
{
    public class AnonymousTwitterService : TwitterService, IAnonymousTwitterService
    {
        #region Events

        public event EventHandler<TweetsAddedEventArgs> TweetsAdded;

        protected virtual void OnTweetsAdded(object sender, TweetsAddedEventArgs e)
        {
            if (TweetsAdded != null)
                TweetsAdded(sender, e);
        }

        #endregion

        #region Fields

        private readonly List<Tweet> _tweets;

        #endregion

        #region Ctors

        public AnonymousTwitterService(
            IGeocodeService geocodeService,
            IDataService dataService)
            : base(geocodeService, dataService)
        {
            _tweets = new List<Tweet>();
        }

        #endregion

        #region Properties

        public override bool IsAuthorized
        {
            get { return true; }
        }

        public List<Tweet> Tweets
        {
            get { return _tweets; }
        }

        #endregion

        #region Methods

        public async Task<List<User>> GetUsersAsync(string userScreenNames)
        {
            var users = new List<User>();
            if (!string.IsNullOrEmpty(userScreenNames))
            {
                try
                {
                    Config config = await _dataService.GetConfigAsync();

                    string s;
                    using (var httpClient = new HttpClient())
                    {
                        var requestUri =
                            new Uri(string.Format("{0}twitter/users?userScreenNames={1}",
                                                  config.AppBaseUrl,
                                                  userScreenNames));
                        using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, requestUri))
                        {
                            using (HttpResponseMessage responseMessage = await httpClient.SendAsync(requestMessage))
                            {
                                s = await responseMessage.Content.ReadAsStringAsync();
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(s))
                    {
                        users = JToken.Parse(s).ToObject<List<User>>();
                    }
                }
                catch (Exception)
                {
                    users = new List<User>();
                }
            }

            return users;
        }

        public override async Task<List<Tweet>>
            SearchAsync(string query, double latitude, double longitude, int radiusInMeters, ulong sinceID = 0,
                        ulong maxID = 0)
        {
            GeoPoint geoPoint = null;
            if (!Equals(latitude, 0d) && !Equals(longitude, 0d))
            {
                geoPoint = new GeoPoint(latitude, longitude);
            }

            List<Tweet> tweets =
                (await _dataService.GetTweetsAsync(geoPoint, radiusInMeters, sinceID, maxID))
                    .OrderByDescending(tweet => tweet.CreatedAtTimestamp)
                    .ToList();

            var e = new TweetsAddedEventArgs();

            foreach (Tweet tweet in tweets)
            {
                if (_tweets.Contains(tweet)) continue;
                _tweets.Add(tweet);
                e.Tweets.Add(tweet);
            }

            if (e.Tweets.Count > 0)
            {
                OnTweetsAdded(this, e);
            }

            return tweets;
        }

        public async Task UpdateProfileImageUrlsAsync(List<Tweet> tweets)
        {
            var userScreenNames = new List<string>();
            using (var httpClient = new HttpClient())
            {
                foreach (Tweet tweet in tweets)
                {
                    using (var requestMessage = new HttpRequestMessage(HttpMethod.Head, tweet.ProfileImageUrl))
                    {
                        using (HttpResponseMessage responseMessage = await httpClient.SendAsync(requestMessage))
                        {
                            if (!responseMessage.IsSuccessStatusCode)
                            {
                                userScreenNames.Add(tweet.UserScreenName);
                            }
                        }
                    }
                }
            }

            if (userScreenNames.Count > 0)
            {
                List<User> users = await GetUsersAsync(string.Join(",", userScreenNames.ToArray()));
                foreach (User user in users)
                {
                    Tweet tweet = tweets.First(x => x.UserScreenName == user.ScreenName);
                    tweet.ProfileImageUrl = user.ProfileImageUrl;
                }
            }
        }

        public async Task<IEnumerable<Tweet>> SendTweetAsync(string status, double latitude, double longitude)
        {
            var tweets = new List<Tweet>();

            try
            {
                Config config = await _dataService.GetConfigAsync();

                string s;
                using (var httpClient = new HttpClient())
                {
                    string uriString =
                        string.Format("{0}twitter/sendtweet?status={1}&latitude={2}&longitude={3}",
                                      config.AppBaseUrl,
                                      Uri.EscapeDataString(status),
                                      latitude.ToString(CultureInfo.InvariantCulture),
                                      longitude.ToString(CultureInfo.InvariantCulture));

                    var requestUri = new Uri(uriString);
                    using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, requestUri))
                    {
                        using (HttpResponseMessage responseMessage = await httpClient.SendAsync(requestMessage))
                        {
                            s = await responseMessage.Content.ReadAsStringAsync();
                        }
                    }
                }

                if (!string.IsNullOrEmpty(s))
                {
                    tweets = JToken.Parse(s).ToObject<List<Tweet>>();
                }
            }
            catch (Exception)
            {
                tweets = new List<Tweet>();
            }
            return tweets;
        }

        public override Task<IEnumerable<Tweet>> SendTweetAsync(Tweet tweet)
        {
            string status = StatusBuilder.Build(tweet);

            return SendTweetAsync(status, tweet.Latitude, tweet.Longitude);
        }

        public override Task AuthorizeAsync()
        {
            var completionSource = new TaskCompletionSource<bool>();
            completionSource.TrySetResult(IsAuthorized);
            return completionSource.Task;
        }

        #endregion
    }
}