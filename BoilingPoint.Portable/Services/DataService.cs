﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.DoubleGis;

namespace BoilingPoint.Portable.Services
{
    public abstract class DataService : IDataService
    {
        public const int DEFAULT_RADIUS_IN_METERS = 20;
        public const int PAGE_SIZE = 1000;

        private readonly DoubleGis _doubleGis = new DoubleGis();

        private readonly IGeolocationService _geolocationService;

        protected DataService(IGeolocationService geolocationService)
        {
            _geolocationService = geolocationService;
        }

        public abstract Task<Config> GetConfigAsync();
        public abstract Task<List<Tag>> GetPredefinedTagsAsync();
        public abstract Task<Bank> GetBankByIdAsync(string bankId);
        public abstract Task<int> GetBanksCountAsync(string countryCode = null);

        public abstract Task<List<Tweet>>
            GetTweetsAsync(GeoPoint geoPoint = null, int radiusInMeters = 0,
                           ulong sinceID = 0, ulong maxID = 0);

        public abstract Task<List<Tweet>> GetTweetsByBankIdAsync(string bankId);
        public abstract Task<List<Tweet>> GetTweetsByProfileIdAsync(string profileId);

        public async Task<Profile> GetProfileByIdAsync(string profileId)
        {
            Profile profile = null;
            if (DoubleGis.IsInitialized)
            {
                var filial = new Filial {Id = profileId};
                List<Profile> profiles = await _doubleGis.SearchProfilesAsync(new[] {filial});
                profile = profiles.First();
            }
            return profile;
        }

        public async Task<List<Bank>> GetBanksAsync(string countryCode = null)
        {
            if (countryCode != null)
                countryCode = countryCode.ToLower();

            var banks = new List<Bank>();
            int count = await GetBanksCountAsync(countryCode);
            if (count > 0)
            {
                int pagesCount = count > 0 ? (int) Math.Ceiling(count/(double) PAGE_SIZE) : 0;
                for (int page = 0; page < pagesCount; page++)
                {
                    banks.AddRange(await GetBanksAsync(page, PAGE_SIZE, countryCode));
                }
            }
            return banks;
        }

        public abstract Task<List<Bank>> GetBanksAsync(int page, int pageSize, string countryCode = null);

        public abstract Task<List<Bank>> SearchBanksByTitleAsync(string title, string countryCode = null);
        public abstract Task<List<Bank>> SearchBanksByHashtagTitleAsync(string hashtagTitle);
        public abstract Task<List<Bank>> SearchBanksByTwitterUsernameAsync(string twitterUsername);

        public async Task<List<Profile>>
            SearchProfilesAsync(double latitude, double longitude, int radiusInMeters = DEFAULT_RADIUS_IN_METERS,
                                string rubric = DoubleGis.RUBRIC_BANKS)
        {
            var profiles = new List<Profile>();
            if (DoubleGis.IsInitialized)
            {
                profiles =
                    (await _doubleGis.SearchProfilesAsync(new GeoPoint(latitude, longitude), radiusInMeters)).ToList();
            }
            return profiles;
        }

        public async Task<List<Filial>>
            SearchFilialsAsync(double latitude, double longitude, int radiusInMeters = DEFAULT_RADIUS_IN_METERS,
                               string rubric = DoubleGis.RUBRIC_BANKS)
        {
            var filials = new List<Filial>();
            if (DoubleGis.IsInitialized)
            {
                filials = await _doubleGis.SearchFilialsAsync(new GeoPoint(latitude, longitude), radiusInMeters, rubric);
            }
            return filials;
        }

        public async Task<List<Bank>>
            SearchBanksAsync(double latitude, double longitude, int radiusInMeters = DEFAULT_RADIUS_IN_METERS)
        {
            IEnumerable<Profile> profiles =
                await SearchProfilesAsync(latitude, longitude, radiusInMeters);

            return await SearchBanksAsync(profiles);
        }

        public async Task<List<Bank>> SearchBanksAsync(IEnumerable<Profile> profiles)
        {
            var banks = new List<Bank>();
            foreach (Profile profile in profiles)
            {
                Bank bank = await GetBankByProfileAsync(profile);
                if (bank == null) continue;
                bank.Profile = profile;
                profile.Bank = bank;
                banks.Add(bank);
            }

            return banks;
        }

        public async Task<List<Bank>> SearchBanksNearbyAsync(int radiusInMeters = DEFAULT_RADIUS_IN_METERS)
        {
            var banks = new List<Bank>();
            if (_geolocationService != null)
            {
                GeoPoint geoPoint = await _geolocationService.GetGeoPointAsync();
                if (geoPoint != null)
                {
                    banks = await SearchBanksAsync(geoPoint.Latitude, geoPoint.Longitude, radiusInMeters);
                }
            }
            return banks;
        }

        protected abstract Task<Bank> GetBankByProfileAsync(Profile profile);
    }
}