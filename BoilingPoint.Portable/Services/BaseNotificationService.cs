﻿using System;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Events;

namespace BoilingPoint.Portable.Services
{
    public abstract class BaseNotificationService : INotificationService
    {
        #region Events

        public event EventHandler DeviceRegistered;

        public event EventHandler<NotificationReceivedEventArgs> NotificationReceived;

        protected virtual void OnDeviceRegistered(object sender, EventArgs e)
        {
            if (DeviceRegistered != null)
                DeviceRegistered(sender, e);
        }

        protected virtual void OnNotificationReceived(object sender, NotificationReceivedEventArgs e)
        {
            if (NotificationReceived != null)
                NotificationReceived(sender, e);
        }

        #endregion

        public abstract void RegisterDevice(Device device);
    }
}