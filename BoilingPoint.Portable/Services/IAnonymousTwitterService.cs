﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Events;

namespace BoilingPoint.Portable.Services
{
    public interface IAnonymousTwitterService : ITwitterService
    {
        List<Tweet> Tweets { get; }
        event EventHandler<TweetsAddedEventArgs> TweetsAdded;

        Task<List<User>> GetUsersAsync(string userScreenNames);

        Task<IEnumerable<Tweet>> SendTweetAsync(string status, double latitude, double longitude);

        Task UpdateProfileImageUrlsAsync(List<Tweet> tweets);
    }
}