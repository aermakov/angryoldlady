﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.DoubleGis;

namespace BoilingPoint.Portable.Services
{
    public interface IDataService
    {
        Task<int> GetBanksCountAsync(string countryCode = null);

        Task<Config> GetConfigAsync();

        Task<List<Tag>> GetPredefinedTagsAsync();

        Task<Profile> GetProfileByIdAsync(string profileId);
        Task<Bank> GetBankByIdAsync(string bankId);

        Task<List<Bank>> GetBanksAsync(string countryCode = null);
        Task<List<Bank>> GetBanksAsync(int page, int pageSize, string countryCode = null);
        Task<List<Bank>> SearchBanksByTitleAsync(string title, string countryCode = null);
        Task<List<Bank>> SearchBanksByHashtagTitleAsync(string hashtagTitle);
        Task<List<Bank>> SearchBanksByTwitterUsernameAsync(string twitterUsername);

        Task<List<Bank>> SearchBanksAsync(double latitude, double longitude,
                                          int radiusInMeters = DataService.DEFAULT_RADIUS_IN_METERS);

        Task<List<Bank>> SearchBanksAsync(IEnumerable<Profile> profiles);
        Task<List<Bank>> SearchBanksNearbyAsync(int radiusInMeters = DataService.DEFAULT_RADIUS_IN_METERS);

        Task<List<Profile>> SearchProfilesAsync(double latitude, double longitude,
                                                int radiusInMeters = DataService.DEFAULT_RADIUS_IN_METERS,
                                                string rubric = DoubleGis.RUBRIC_BANKS);

        Task<List<Filial>> SearchFilialsAsync(double latitude, double longitude,
                                              int radiusInMeters = DataService.DEFAULT_RADIUS_IN_METERS,
                                              string rubric = DoubleGis.RUBRIC_BANKS);

        Task<List<Tweet>> GetTweetsAsync(
            GeoPoint geoPoint = null,
            int radiusInMeters = 0,
            ulong sinceID = 0,
            ulong maxID = 0);

        Task<List<Tweet>> GetTweetsByBankIdAsync(string bankId);
        Task<List<Tweet>> GetTweetsByProfileIdAsync(string profileId);
    }
}