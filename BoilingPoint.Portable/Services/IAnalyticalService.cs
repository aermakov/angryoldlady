﻿using System;

namespace BoilingPoint.Portable.Services
{
    public interface IAnalyticalService
    {
        void ReportEvent(string eventName);

        void ReportError(Exception ex);
    }
}