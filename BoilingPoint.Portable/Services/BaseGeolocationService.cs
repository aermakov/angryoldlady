﻿using System;
using System.Threading.Tasks;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Events;
using BoilingPoint.Portable.Geo;

namespace BoilingPoint.Portable.Services
{
    public abstract class BaseGeolocationService : IGeolocationService
    {
        #region Events

        public event EventHandler<PositionChangedEventArgs> PositionChanged;

        public event EventHandler<GeolocationStatusChangedEventArgs> StatusChanged;

        protected virtual void OnPositionChanged(object sender, PositionChangedEventArgs e)
        {
            if (PositionChanged != null)
                PositionChanged(this, e);
        }

        protected virtual void OnStatusChanged(object sender, GeolocationStatusChangedEventArgs e)
        {
            if (StatusChanged != null)
                StatusChanged(this, e);
        }

        #endregion

        #region Properties

        public abstract GeolocationStatus Status { get; }

        #endregion

        #region Methods

        public abstract bool Started { get; }
        public abstract Task<GeoPoint> GetGeoPointAsync();
        public abstract void Start();
        public abstract void Stop();

        #endregion
    }
}