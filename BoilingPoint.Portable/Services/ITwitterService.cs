﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Geo;
using Newtonsoft.Json.Linq;

namespace BoilingPoint.Portable.Services
{
    public interface ITwitterService
    {
        ICache Cache { get; set; }
        bool IsAuthorized { get; }

        AuthorizationStatus AuthorizationStatus { get; set; }

        Task<IEnumerable<Tweet>> SendTweetAsync(Tweet tweet);

        Tweet GetTweet(JToken token);
        Task AuthorizeAsync();

        /// <summary>
        /// </summary>
        /// <param name="query">A UTF-8, URL-encoded search query of 1,000 characters maximum, including operators.</param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="radiusInMeters"></param>
        /// <param name="sinceID">Returns results with an ID greater than (that is, more recent than) the specified ID.</param>
        /// <param name="maxID">Returns results with an ID less than (that is, older than) or equal to the specified ID.</param>
        /// <returns></returns>
        Task<List<Tweet>> SearchAsync(string query, double latitude, double longitude, int radiusInMeters,
                                      ulong sinceID = 0, ulong maxID = 0);

        Task<List<Tweet>> SearchAsync(string query, ulong sinceID = 0, ulong maxID = 0);

        Task InitializeAddressAsync(Tweet tweet, GeoPoint geoPoint);
        Task InitializeProfileAsync(Tweet tweet, GeoPoint geoPoint);
        Task InitializeBankAsync(Tweet tweet, GeoPoint geoPoint);

        List<TweetGrouping> GroupTweetsByDistance(IEnumerable<Tweet> tweets,
                                                  double radiusInMeters = DataService.DEFAULT_RADIUS_IN_METERS);
    }
}