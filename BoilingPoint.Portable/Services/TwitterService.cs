﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using BoilingPoint.Portable.Constants;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Extensions;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.DoubleGis;
using Newtonsoft.Json.Linq;

namespace BoilingPoint.Portable.Services
{
    public class TwitterService : ITwitterService
    {
        protected readonly IDataService _dataService;
        protected readonly DoubleGis _doubleGis = new DoubleGis();
        protected readonly IGeocodeService _geocodeService;

        protected List<string> _predefinedTags = new List<string>();

        public TwitterService(IGeocodeService geocodeService, IDataService dataService)
        {
            _geocodeService = geocodeService;
            _dataService = dataService;
        }

        public Tweet GetTweet(JToken token)
        {
            return new Tweet(token);
        }

        public ICache Cache { get; set; }

        public virtual bool IsAuthorized
        {
            get { throw new NotImplementedException(); }
        }

        public AuthorizationStatus AuthorizationStatus { get; set; }

        public virtual Task AuthorizeAsync()
        {
            throw new NotImplementedException();
        }

        public virtual Task<IEnumerable<Tweet>> SendTweetAsync(Tweet tweet)
        {
            throw new NotImplementedException();
        }

        public virtual Task<List<Tweet>> SearchAsync(string query, double latitude, double longitude, int radiusInMeters,
                                                     ulong sinceID = 0, ulong maxID = 0)
        {
            string geoCode = string.Format("{0},{1},{2}km",
                                           latitude.ToString(CultureInfo.InvariantCulture),
                                           longitude.ToString(CultureInfo.InvariantCulture),
                                           Distance.ToKilometers(radiusInMeters)
                                                   .Value.ToString(CultureInfo.InvariantCulture));

            return SearchAsync(query, geoCode, sinceID, maxID);
        }

        public virtual Task<List<Tweet>> SearchAsync(string query, ulong sinceID = 0, ulong maxID = 0)
        {
            return SearchAsync(query, 0, 0, 0, sinceID, maxID);
        }

        public virtual async Task InitializeAddressAsync(Tweet tweet, GeoPoint geoPoint)
        {
            if (string.IsNullOrEmpty(tweet.Address))
            {
                IList<GeoAddress> geoAddresses = await _geocodeService.ReverseGeocodeAsync(geoPoint);
                if (geoAddresses.Any())
                {
                    GeoAddress geoAddress = geoAddresses.First();
                    tweet.Address = geoAddress.FormattedAddress;
                }
            }
        }

        public virtual async Task InitializeProfileAsync(Tweet tweet, GeoPoint geoPoint)
        {
            if (DoubleGis.IsInitialized)
            {
                List<Filial> filials = await _doubleGis.SearchFilialsAsync(geoPoint);
                if (filials.Any())
                {
                    Filial filial = filials.First();
                    tweet.Filial = filial;

                    List<Profile> profiles = await _doubleGis.SearchProfilesAsync(new[] {filial});
                    if (profiles.Any())
                    {
                        Profile profile = profiles.First();
                        tweet.Profile = profile;
                    }
                }
            }
        }

        public virtual async Task InitializeBankAsync(Tweet tweet, GeoPoint geoPoint)
        {
            if (tweet.Bank == null)
            {
                if (tweet.Users.Count > 0)
                {
                    tweet.Bank = await SearchBankByUsersAsync(tweet.Users, tweet.CountryCode);
                }

                if (tweet.Bank == null && tweet.Tags.Count > 0)
                {
                    tweet.Bank = await SearchBankByTagsAsync(tweet.Tags, tweet.CountryCode);
                }

                if (tweet.Bank == null && tweet.Profile != null)
                {
                    tweet.Bank = await SearchBankByProfileAsync(tweet.Profile);
                }
            }
        }

        public virtual List<TweetGrouping>
            GroupTweetsByDistance(IEnumerable<Tweet> tweets,
                                  double radiusInMeters = DataService.DEFAULT_RADIUS_IN_METERS)
        {
            var indices = new List<int>();
            var tweetGroupings = new List<TweetGrouping>();
            Tweet[] array = tweets.ToArray();
            for (int i = 0; i < array.Count(); i++)
            {
                if (indices.Contains(i))
                    continue;

                indices.Add(i);

                var tweetGrouping = new TweetGrouping(radiusInMeters) {array[i]};
                for (int j = i + 1; j < array.Count(); j++)
                {
                    if (indices.Contains(j) || !tweetGrouping.CanAdd(array[j]))
                        continue;

                    indices.Add(j);
                    tweetGrouping.Add(array[j]);
                }

                tweetGroupings.Add(tweetGrouping);
            }
            return tweetGroupings;
        }

        public virtual Task<List<Tweet>> SearchAsync(string query, string geoCode = null, ulong sinceID = 0,
                                                     ulong maxID = 0)
        {
            throw new NotImplementedException();
        }

        private bool TryGetBankFromCache(int hashCode, out Bank bank)
        {
            bank = null;
            if (Cache == null) return false;
            string key = hashCode.ToString();
            if (Cache.Contains(key))
            {
                bank = Cache.Get<Bank>(key);
                return true;
            }
            return false;
        }

        private void AddBankToCache(object keyObj, Bank bank)
        {
            if (Cache == null || bank == null) return;
            int hashCode = keyObj.GetHashCode();
            string key = hashCode.ToString();
            if (!Cache.Contains(key))
            {
                Cache.Add(key, bank, CacheConstants.NoAbsoluteExpiration, CacheConstants.NoSlidingExpiration);
            }
        }

        protected async Task<List<string>> GetPredefinedTagsAsync()
        {
            if (_predefinedTags != null && _predefinedTags.Count != 0)
                return _predefinedTags;

            if (_dataService != null)
            {
                _predefinedTags = (await _dataService.GetPredefinedTagsAsync()).Select(x => x.Name).ToList();
            }

            AddServiceTags();

            return _predefinedTags;
        }

        private void AddServiceTags()
        {
            _predefinedTags.AddIfNotContains(Tweet.CASHBOX_AVAILABLE_TAG);
            _predefinedTags.AddIfNotContains(Tweet.CASHBOX_CLOSED_TAG);
            _predefinedTags.AddIfNotContains(Tweet.HATE_TO_WAIT_TAG);
            _predefinedTags.AddIfNotContains(Tweet.PEOPLE_INLINE_TAG);
            _predefinedTags.AddIfNotContains(Tweet.WAIT_TIME_TAG);
        }

        public virtual async Task<Bank> SearchBankByProfileAsync(Profile profile)
        {
            Bank bank;
            if (TryGetBankFromCache(profile.GetHashCode(), out bank)) return bank;
            if (_dataService != null)
            {
                List<Bank> banks = await _dataService.SearchBanksAsync(new[] {profile});
                if (!banks.Any()) return bank;
                bank = banks.First();
            }
            AddBankToCache(profile, bank);
            return bank;
        }

        public virtual async Task<Bank> SearchBankByUsersAsync(List<User> users, string countryCode)
        {
            Bank bank = null;
            for (int i = 0; i < users.Count && bank == null; i++)
            {
                User user = users[i];
                if (TryGetBankFromCache(user.GetHashCode(), out bank)) continue;
                string twitterUsername = user.ScreenName.ToLower();
                if (_dataService != null)
                {
                    List<Bank> banks = await _dataService.SearchBanksByTwitterUsernameAsync(twitterUsername);
                    if (!banks.Any()) continue;
                    if (!string.IsNullOrEmpty(countryCode))
                    {
                        bank = banks.FirstOrDefault(x => string.Equals(x.CountryCode, countryCode.ToLower()));
                    }

                    if (bank == null)
                    {
                        bank = banks.First();
                    }
                }

                AddBankToCache(user, bank);
            }
            return bank;
        }

        public virtual async Task<Bank> SearchBankByTagsAsync(List<string> tags, string countryCode)
        {
            Bank bank = null;
            List<string> predefinedTags = await GetPredefinedTagsAsync();
            for (int i = 0; i < tags.Count && bank == null; i++)
            {
                string tag = tags[i].ToLower();
                if (TryGetBankFromCache(tag.GetHashCode(), out bank)) continue;
                bool isPredefinedTag = false;
                for (int j = 0; j < predefinedTags.Count && !isPredefinedTag; j++)
                {
                    string predefinedTag = predefinedTags[j];
                    if (tag.EndsWith(predefinedTag))
                    {
                        isPredefinedTag = true;
                    }
                }

                if (isPredefinedTag) continue;
                if (_dataService != null)
                {
                    List<Bank> banks = await _dataService.SearchBanksByHashtagTitleAsync(tag);
                    if (!banks.Any()) continue;
                    if (!string.IsNullOrEmpty(countryCode))
                    {
                        bank = banks.FirstOrDefault(x => string.Equals(x.CountryCode, countryCode.ToLower()));
                    }

                    if (bank == null)
                    {
                        bank = banks.First();
                    }
                }

                AddBankToCache(tag, bank);
            }

            return bank;
        }
    }
}