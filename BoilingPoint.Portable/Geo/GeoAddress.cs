﻿namespace BoilingPoint.Portable.Geo
{
    public class GeoAddress
    {
        private readonly GeoPoint coordinates;
        private readonly string formattedAddress;

        public GeoAddress(string formattedAddress, GeoPoint coordinates)
        {
            formattedAddress = (formattedAddress ?? string.Empty).Trim();

            Guard.ArgumentIsNotNull(formattedAddress, "formattedAddress");
            Guard.ArgumentIsNotNull(coordinates, "coordinates");

            this.formattedAddress = formattedAddress;
            this.coordinates = coordinates;
        }

        public GeoPoint Coordinates
        {
            get { return coordinates; }
        }

        public string FormattedAddress
        {
            get { return formattedAddress ?? string.Empty; }
        }

        public override string ToString()
        {
            return FormattedAddress;
        }
    }
}