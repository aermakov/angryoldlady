﻿using System;

namespace BoilingPoint.Portable.Geo
{
    public struct Distance
    {
        public const double EarthRadiusInMiles = 3956.545;
        public const double EarthRadiusInKilometers = 6378.135;
        public const double MetersInKilometers = 1000;

        private const double ConversionConstant = 0.621371192;

        private readonly DistanceUnits _units;
        private readonly double _value;

        public Distance(double value, DistanceUnits units)
        {
            _value = Math.Round(value, 8);
            _units = units;
        }

        #region Helper Factory Methods

        public static Distance FromMiles(double miles)
        {
            return new Distance(miles, DistanceUnits.Miles);
        }

        public static Distance FromKilometers(double kilometers)
        {
            return new Distance(kilometers, DistanceUnits.Kilometers);
        }

        public static Distance FromMeters(double meters)
        {
            return new Distance(meters, DistanceUnits.Meters);
        }

        #endregion

        #region Unit Conversions

        private Distance ConvertUnits(DistanceUnits units)
        {
            if (_units == units) return this;

            double newValue;
            switch (units)
            {
                case DistanceUnits.Miles:
                    newValue = _value*ConversionConstant;
                    break;
                case DistanceUnits.Kilometers:
                    newValue = _value/ConversionConstant;
                    break;
                default:
                    newValue = 0;
                    break;
            }

            return new Distance(newValue, units);
        }

        public Distance ToMiles()
        {
            return ConvertUnits(DistanceUnits.Miles);
        }

        public Distance ToKilometers()
        {
            return ConvertUnits(DistanceUnits.Kilometers);
        }

        public static Distance ToKilometers(double meters)
        {
            return FromKilometers(meters/MetersInKilometers);
        }

        #endregion

        public double Value
        {
            get { return _value; }
        }

        public DistanceUnits Units
        {
            get { return _units; }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Distance && Equals((Distance) obj);
        }

        public bool Equals(Distance obj)
        {
            return _units == obj._units && _value.Equals(obj._value);
        }

        public bool Equals(Distance obj, bool normalizeUnits)
        {
            if (normalizeUnits)
                obj = obj.ConvertUnits(Units);
            return Equals(obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((int) _units*397) ^ _value.GetHashCode();
            }
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", _value, _units);
        }

        #region Operators

        public static Distance operator *(Distance d1, double d)
        {
            double newValue = d1.Value*d;
            return new Distance(newValue, d1.Units);
        }

        public static Distance operator +(Distance left, Distance right)
        {
            double newValue = left.Value + right.ConvertUnits(left.Units).Value;
            return new Distance(newValue, left.Units);
        }

        public static Distance operator -(Distance left, Distance right)
        {
            double newValue = left.Value - right.ConvertUnits(left.Units).Value;
            return new Distance(newValue, left.Units);
        }

        public static bool operator ==(Distance left, Distance right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Distance left, Distance right)
        {
            return !left.Equals(right);
        }

        public static bool operator <(Distance left, Distance right)
        {
            return (left.Value < right.ConvertUnits(left.Units).Value);
        }

        public static bool operator <=(Distance left, Distance right)
        {
            return (left.Value <= right.ConvertUnits(left.Units).Value);
        }

        public static bool operator >(Distance left, Distance right)
        {
            return (left.Value > right.ConvertUnits(left.Units).Value);
        }

        public static bool operator >=(Distance left, Distance right)
        {
            return (left.Value >= right.ConvertUnits(left.Units).Value);
        }

        public static implicit operator double(Distance distance)
        {
            return distance.Value;
        }

        #endregion
    }
}