﻿using Newtonsoft.Json.Linq;

namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public class ProfileResponse : BaseResponse
    {
        public ProfileResponse(JToken token) : base(token)
        {
            if (ResponseCode != ResponseCode.OK) return;
            Profile = new Profile(token);
        }

        public Profile Profile { get; private set; }
    }
}