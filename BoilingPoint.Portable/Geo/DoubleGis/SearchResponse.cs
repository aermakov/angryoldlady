﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public class SearchResponse : BaseResponse
    {
        public SearchResponse(JToken token) : base(token)
        {
            if (ResponseCode != ResponseCode.OK) return;

            Total = token.Value<int>("total");

            Result = new List<Filial>(Total);

            var resultToken = token.Value<JToken>("result");
            foreach (JToken filialToken in resultToken)
            {
                var filial = filialToken.ToObject<Filial>();
                Result.Add(filial);
            }
        }

        public int Total { get; set; }
        public List<Filial> Result { get; set; }
    }
}