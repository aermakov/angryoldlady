﻿namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public enum ResponseCode
    {
        OK = 200,
        BadRequest = 400,
        Forbidden = 403,
        NotFound = 404,
        InternalServerError = 500,
        NotImplemented = 501,
        ServiceUnavailable = 503
    }
}