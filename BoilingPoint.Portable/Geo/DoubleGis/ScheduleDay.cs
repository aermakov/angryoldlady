﻿using Newtonsoft.Json;

namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public class ScheduleDay
    {
        [JsonProperty("working_hours-0")]
        public WorkingHours WorkingHours0 { get; set; }

        [JsonProperty("working_hours-1")]
        public WorkingHours WorkingHours1 { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1}", WorkingHours0, WorkingHours1);
        }
    }
}