﻿using System.Collections.Generic;

namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public abstract class BaseRequest
    {
        protected IDictionary<string, object> _parameters = new Dictionary<string, object>();

        protected BaseRequest(string path)
        {
            Path = path;
        }

        public IDictionary<string, object> Parameters
        {
            get { return _parameters; }
        }

        public string Path { get; private set; }

        public void AddParameter(string key, object value)
        {
            if (!_parameters.ContainsKey(key))
            {
                _parameters.Add(key, value);
            }
        }
    }
}