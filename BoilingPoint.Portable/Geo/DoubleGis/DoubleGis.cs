﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using BoilingPoint.Portable.Net;
using BoilingPoint.Portable.Services;
using Newtonsoft.Json.Linq;

namespace BoilingPoint.Portable.Geo.DoubleGis
{
    public class DoubleGis
    {
        #region Fields

        public const string RUBRIC_BANKS = "Банки";

        private const string API_VERSION = "1.3";
        private const string BASE_API_URL = "http://catalog.api.2gis.ru";

        private static string _apiKey;

        #endregion

        #region Properties

        public static bool IsInitialized
        {
            get { return !string.IsNullOrEmpty(_apiKey); }
        }

        #endregion

        #region Methods

        public static void Initialize(string apiKey)
        {
            _apiKey = apiKey;
        }

        public async Task<V> SendRequestAsync<T, V>(T request)
            where T : BaseRequest
            where V : BaseResponse
        {
            Guard.ArgumentIsNotNull("ApiKey", _apiKey);

            request.AddParameter("version", API_VERSION);
            request.AddParameter("key", _apiKey);

            var parameters = new Dictionary<string, string>();
            foreach (var pair in request.Parameters)
            {
                if (pair.Value != null)
                {
                    parameters.Add(pair.Key, pair.Value.ToString());
                }
            }

            string queryString = WebUtility.BuildQueryString(parameters);

            var builder = new UriBuilder(BASE_API_URL) {Path = request.Path, Query = queryString};
            Uri requestUri = builder.Uri;

            V response = default(V);

            using (var httpClient = new HttpClient())
            {
                using (var requestMessage = new HttpRequestMessage(HttpMethod.Get, requestUri))
                {
                    try
                    {
                        using (HttpResponseMessage responseMessage = await httpClient.SendAsync(requestMessage))
                        {
                            if (responseMessage.IsSuccessStatusCode)
                            {
                                string s = await responseMessage.Content.ReadAsStringAsync();
                                if (!string.IsNullOrEmpty(s))
                                {
                                    JToken token = JToken.Parse(s);
                                    response = (V) Activator.CreateInstance(typeof (V), token);
                                }
                            }
                        }
                    }
                    catch (WebException)
                    {
                    }
                }
            }

            return response;
        }

        public async Task<List<Profile>> SearchProfilesAsync(IEnumerable<Filial> filials)
        {
            var profiles = new List<Profile>();
            foreach (Filial filial in filials)
            {
                var profileRequest = new ProfileRequest(filial.Id, filial.Hash);
                ProfileResponse response = await SendRequestAsync<ProfileRequest, ProfileResponse>(profileRequest);

                if (response == null || response.ResponseCode != ResponseCode.OK)
                    continue;

                Profile profile = response.Profile;
                profile.Filial = filial;
                profiles.Add(profile);
            }

            return profiles;
        }

        public async Task<List<Profile>>
            SearchProfilesAsync(GeoPoint geoPoint, int radiusInMeters = DataService.DEFAULT_RADIUS_IN_METERS,
                                string rubric = RUBRIC_BANKS)
        {
            List<Filial> filials =
                await SearchFilialsAsync(geoPoint, radiusInMeters, rubric);
            return await SearchProfilesAsync(filials);
        }

        public async Task<List<Filial>>
            SearchFilialsAsync(GeoPoint geoPoint, int radiusInMeters = DataService.DEFAULT_RADIUS_IN_METERS,
                               string rubric = RUBRIC_BANKS)
        {
            var filials = new List<Filial>();
            var searchRequest = new SearchRequest(rubric, geoPoint, radiusInMeters, SortBy.Distance);
            SearchResponse response =
                await SendRequestAsync<SearchRequest, SearchResponse>(searchRequest);
            if (response != null && response.ResponseCode == ResponseCode.OK)
            {
                filials.AddRange(response.Result);
            }
            return filials;
        }

        #endregion
    }
}