﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using BoilingPoint.Portable.Exceptions;
using Newtonsoft.Json.Linq;

namespace BoilingPoint.Portable.Geo.Google
{
    public class GoogleGeocoder : BaseGoogleService, IGoogleGeocoder
    {
        #region Fields

        private const string GEOCODE_SERVICE_URI =
            "http://maps.googleapis.com/maps/api/geocode/json?address={0}&sensor=false";

        private const string REVERSE_GEOCODE_SERVICE_URI =
            "http://maps.googleapis.com/maps/api/geocode/json?latlng={0},{1}&sensor=true";

        #endregion

        #region IGeocoder Members

        public async Task<string> GetCountryCodeAsync(double latitude, double longitude)
        {
            string countryCode = null;
            IList<GeoAddress> addresses = await ReverseGeocodeAsync(latitude, longitude);
            foreach (GoogleAddressComponent component in from googleAddress in addresses.OfType<GoogleAddress>()
                                                         where !googleAddress.IsPartialMatch
                                                         select googleAddress.Components.FirstOrDefault(
                                                             x => x.Types.Contains(GoogleAddressType.Country))
                                                         into component
                                                         where component != null
                                                         select component)
            {
                countryCode = component.ShortName.ToLower();
            }
            return countryCode;
        }

        public async Task<IList<GeoAddress>> GeocodeAsync(string address, string lang = "", string region = "")
        {
            try
            {
                string uriString = string.Format(GEOCODE_SERVICE_URI, Uri.EscapeDataString(address));
                if (!string.IsNullOrEmpty(region))
                {
                    uriString = string.Format("{0}&region={1}", uriString, region);
                }

                if (!string.IsNullOrEmpty(lang))
                {
                    uriString = string.Format("{0}&language={1}", uriString, lang);
                }

                var requestUri = new Uri(uriString);

                string s;
                using (var httpClient = new HttpClient())
                {
                    s = await httpClient.GetStringAsync(requestUri);
                }
                return ProcessResponse(s);
            }
            catch (WebException ex)
            {
                switch (ex.Status)
                {
                    case WebExceptionStatus.ConnectFailure:
                        throw new ConnectFailureException("The Google Maps geocoding service appears to be offline.", ex);
                    default:
                        throw;
                }
            }
        }

        public async Task<IList<GeoAddress>> ReverseGeocodeAsync(GeoPoint geoPoint, string lang = "")
        {
            return await ReverseGeocodeAsync(geoPoint.Latitude, geoPoint.Longitude, lang);
        }

        public async Task<IList<GeoAddress>>
            ReverseGeocodeAsync(double latitude, double longitude, string lang = "")
        {
            IList<GeoAddress> geoAddresses;
            try
            {
                string requestUri =
                    string.Format(REVERSE_GEOCODE_SERVICE_URI, latitude.ToString(CultureInfo.InvariantCulture),
                                  longitude.ToString(CultureInfo.InvariantCulture));

                if (!string.IsNullOrEmpty(lang))
                {
                    requestUri = string.Format("{0}&language={1}", requestUri, lang);
                }

                string s;
                using (var httpClient = new HttpClient())
                {
                    s = await httpClient.GetStringAsync(requestUri);
                }
                geoAddresses = ProcessResponse(s);
            }
            catch (WebException ex)
            {
                switch (ex.Status)
                {
                    case WebExceptionStatus.ConnectFailure:
                        throw new ConnectFailureException("The Google Maps geocoding service appears to be offline.", ex);
                    default:
                        throw;
                }
            }
            return geoAddresses;
        }

        #endregion

        private IList<GeoAddress> ProcessResponse(string s)
        {
            JObject jObject = JObject.Parse(s);
            GoogleStatus status = EvaluateStatus(jObject.Value<string>("status"));

            if (status != GoogleStatus.Ok && status != GoogleStatus.ZeroResults)
                throw new GoogleGeoCodingException(status);

            var results = (JArray) jObject["results"];

            IEnumerable<GoogleAddress> addresses =
                status == GoogleStatus.Ok ? ParseAddresses(results) : new GoogleAddress[] {};

            return addresses.Cast<GeoAddress>().ToList();
        }

        private IEnumerable<GoogleAddress> ParseAddresses(IEnumerable<JToken> tokens)
        {
            IList<GoogleAddress> addresses = new List<GoogleAddress>();
            foreach (JToken token in tokens)
            {
                string type;
                if (token.Value<JArray>("types").Count > 0)
                {
                    type = token.Value<JArray>("types")[0].ToString();
                }
                else
                {
                    type = "unknown";
                }

                GoogleAddressType addressType = EvaluateType(type);
                var formattedAddress = token.Value<string>("formatted_address");

                JToken addressComponentsToken = token.Value<JArray>("address_components");
                IEnumerable<GoogleAddressComponent> components = ParseComponents(addressComponentsToken);

                JToken locationToken =
                    token.SelectToken("geometry.location");

                var latitude = locationToken.Value<double>("lat");
                var longitude = locationToken.Value<double>("lng");
                var coordinates = new GeoPoint(latitude, longitude);

                var isPartialMatch = token.Value<bool>("partial_match");

                var address = new GoogleAddress(addressType, formattedAddress, components.ToArray(), coordinates,
                                                isPartialMatch);
                addresses.Add(address);
            }
            return addresses;
        }

        private IEnumerable<GoogleAddressComponent> ParseComponents(IEnumerable<JToken> tokens)
        {
            foreach (JToken token in tokens)
            {
                var longName = token.Value<string>("long_name");
                var shortName = token.Value<string>("short_name");

                GoogleAddressType[] types = ParseComponentTypes(token.Value<JArray>("types")).ToArray();

                yield return new GoogleAddressComponent(types, longName, shortName);
            }
        }

        private IEnumerable<GoogleAddressType> ParseComponentTypes(IEnumerable<JToken> tokens)
        {
            foreach (JToken token in tokens)
            {
                yield return EvaluateType(token.ToString());
            }
        }

        /// <remarks>
        ///     http://code.google.com/apis/maps/documentation/geocoding/#Types
        /// </remarks>
        private GoogleAddressType EvaluateType(string type)
        {
            switch (type)
            {
                case "street_address":
                    return GoogleAddressType.StreetAddress;
                case "route":
                    return GoogleAddressType.Route;
                case "intersection":
                    return GoogleAddressType.Intersection;
                case "political":
                    return GoogleAddressType.Political;
                case "country":
                    return GoogleAddressType.Country;
                case "administrative_area_level_1":
                    return GoogleAddressType.AdministrativeAreaLevel1;
                case "administrative_area_level_2":
                    return GoogleAddressType.AdministrativeAreaLevel2;
                case "administrative_area_level_3":
                    return GoogleAddressType.AdministrativeAreaLevel3;
                case "colloquial_area":
                    return GoogleAddressType.ColloquialArea;
                case "locality":
                    return GoogleAddressType.Locality;
                case "sublocality":
                    return GoogleAddressType.SubLocality;
                case "neighborhood":
                    return GoogleAddressType.Neighborhood;
                case "premise":
                    return GoogleAddressType.Premise;
                case "subpremise":
                    return GoogleAddressType.Subpremise;
                case "postal_code":
                    return GoogleAddressType.PostalCode;
                case "natural_feature":
                    return GoogleAddressType.NaturalFeature;
                case "airport":
                    return GoogleAddressType.Airport;
                case "park":
                    return GoogleAddressType.Park;
                case "point_of_interest":
                    return GoogleAddressType.PointOfInterest;
                case "post_box":
                    return GoogleAddressType.PostBox;
                case "street_number":
                    return GoogleAddressType.StreetNumber;
                case "floor":
                    return GoogleAddressType.Floor;
                case "room":
                    return GoogleAddressType.Room;
                case "stadium":
                    return GoogleAddressType.Stadium;
                case "establishment":
                    return GoogleAddressType.Establishment;
                default:
                    return GoogleAddressType.Unknown;
            }
        }
    }
}