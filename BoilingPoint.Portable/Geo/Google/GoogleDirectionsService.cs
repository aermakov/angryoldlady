﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using BoilingPoint.Portable.Exceptions;
using Newtonsoft.Json.Linq;

namespace BoilingPoint.Portable.Geo.Google
{
    public class GoogleDirectionsService : BaseGoogleService, IRouteService
    {
        private const string DirectionsServiceUri =
            "http://maps.googleapis.com/maps/api/directions/json?origin={0},{1}&destination={2},{3}&sensor=false&mode=driving";

        public async Task<IList<GeoPoint>> CalculateRouteAsync(GeoPoint origin, GeoPoint destination)
        {
            try
            {
                var requestUri =
                    new Uri(string.Format(DirectionsServiceUri,
                                          origin.Latitude.ToString(CultureInfo.InvariantCulture),
                                          origin.Longitude.ToString(CultureInfo.InvariantCulture),
                                          destination.Latitude.ToString(CultureInfo.InvariantCulture),
                                          destination.Longitude.ToString(CultureInfo.InvariantCulture)));

                string s;
                using (var httpClient = new HttpClient())
                {
                    s = await httpClient.GetStringAsync(requestUri);
                }

                JObject jObject = JObject.Parse(s);
                GoogleStatus status = EvaluateStatus(jObject.Value<string>("status"));
                IList<GeoPoint> route = new List<GeoPoint>();
                if (status == GoogleStatus.Ok)
                {
                    var encodedPoints = jObject.SelectToken("routes[0].overview_polyline.points").Value<string>();
                    route = DecodePolylinePoints(encodedPoints);
                }
                return route;
            }
            catch (WebException ex)
            {
                switch (ex.Status)
                {
                    case WebExceptionStatus.ConnectFailure:
                        throw new ConnectFailureException("The Google Maps directions service appears to be offline.",
                                                          ex);
                    default:
                        throw;
                }
            }
        }

        private List<GeoPoint> DecodePolylinePoints(string encodedPoints)
        {
            if (string.IsNullOrEmpty(encodedPoints))
                return null;

            var poly = new List<GeoPoint>();
            char[] polylinechars = encodedPoints.ToCharArray();
            int index = 0;

            int currentLat = 0;
            int currentLng = 0;

            while (index < polylinechars.Length)
            {
                // calculate next latitude
                int sum = 0;
                int shifter = 0;
                int next5Bits;
                do
                {
                    next5Bits = polylinechars[index++] - 63;
                    sum |= (next5Bits & 31) << shifter;
                    shifter += 5;
                } while (next5Bits >= 32 && index < polylinechars.Length);

                if (index >= polylinechars.Length)
                    break;

                currentLat += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                //calculate next longitude
                sum = 0;
                shifter = 0;
                do
                {
                    next5Bits = polylinechars[index++] - 63;
                    sum |= (next5Bits & 31) << shifter;
                    shifter += 5;
                } while (next5Bits >= 32 && index < polylinechars.Length);

                if (index >= polylinechars.Length && next5Bits >= 32)
                    break;

                currentLng += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);
                var point = new GeoPoint(Convert.ToDouble(currentLat)/100000.0, Convert.ToDouble(currentLng)/100000.0);
                poly.Add(point);
            }
            return poly;
        }
    }
}