﻿using System;

namespace BoilingPoint.Portable.Geo.Google
{
    public class GoogleAddress : GeoAddress
    {
        private readonly GoogleAddressComponent[] components;
        private readonly bool isPartialMatch;
        private readonly GoogleAddressType type;

        public GoogleAddress(GoogleAddressType type, string formattedAddress,
                             GoogleAddressComponent[] components, GeoPoint coordinates, bool isPartialMatch)
            : base(formattedAddress, coordinates)
        {
            if (components == null)
                throw new ArgumentNullException("components");

            if (components.Length < 1)
                throw new ArgumentException("Value cannot be empty.", "components");

            this.type = type;
            this.components = components;
            this.isPartialMatch = isPartialMatch;
        }

        public GoogleAddressType Type
        {
            get { return type; }
        }

        public GoogleAddressComponent[] Components
        {
            get { return components; }
        }

        public bool IsPartialMatch
        {
            get { return isPartialMatch; }
        }
    }
}