﻿using System;

namespace BoilingPoint.Portable.Geo.Yandex
{
    /// <summary>
    ///     Yandex address.
    /// </summary>
    public class YandexAddress : GeoAddress
    {
        #region Fields

        private readonly GeoKind _kind = GeoKind.Other;

        private readonly Precision _precision = Precision.Other;

        #endregion Fields

        #region Constructors

        public YandexAddress(GeoKind kind, String formattedAddress, GeoPoint coordinates, Precision precision)
            : base(formattedAddress, coordinates)
        {
            _kind = kind;
            _precision = precision;
        }

        #endregion Constructors

        #region Properties

        public GeoKind Kind
        {
            get { return _kind; }
        }

        public Precision Precision
        {
            get { return _precision; }
        }

        public CountryDetails Country { get; set; }

        #endregion Properties
    }
}