﻿using System;

namespace BoilingPoint.Portable
{
    public interface ICache
    {
        void Add(string key, object value, DateTime absoluteExpiration, TimeSpan slidingExpiration);
        bool Contains(string key);
        void Remove(string key);
        T Get<T>(string key);
    }
}