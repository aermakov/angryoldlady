﻿namespace BoilingPoint.Portable.Enums
{
    public enum GeolocationStatus
    {
        Initializing,
        Ready,
        Disabled,
        NoData,
        Disallowed,
        NotInitialized,
        NotAvailable,
    }
}