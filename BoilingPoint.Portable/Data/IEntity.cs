﻿using System;

namespace BoilingPoint.Portable.Data
{
    public interface IEntity
    {
        string objectId { get; set; }
        DateTime? updatedAt { get; set; }
        DateTime? createdAt { get; set; }
    }
}