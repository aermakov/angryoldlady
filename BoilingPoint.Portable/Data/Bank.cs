﻿using BoilingPoint.Portable.Attributes;
using BoilingPoint.Portable.Geo.DoubleGis;
using Newtonsoft.Json;

namespace BoilingPoint.Portable.Data
{
    public class Bank : BaseEntity
    {
        #region Fields

        private string _countryCode;

        #endregion

        #region Properties

        public string Title { get; set; }
        public string LowercaseTitle { get; set; }
        public string LowercaseHashtagTitle { get; set; }
        public string Address { get; set; }
        public string WebSite { get; set; }
        public string TwitterUsername { get; set; }
        public string License { get; set; }
        public string PhoneNumber { get; set; }
        public string InternationalPhoneNumber { get; set; }
        public string Country { get; set; }

        public string CountryCode
        {
            get
            {
                if (_countryCode == null && !string.IsNullOrEmpty(Country))
                {
                    _countryCode = Utils.GetCountryCode(Country);
                }
                return _countryCode;
            }
            set { _countryCode = value; }
        }

        [Ignore]
        [JsonIgnore]
        public Profile Profile { get; set; }

        #endregion

        public override string ToString()
        {
            return Title;
        }

        protected bool Equals(Bank other)
        {
            return string.Equals(Title, other.Title) && string.Equals(Country, other.Country);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Bank) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Title != null ? Title.GetHashCode() : 0)*397) ^ (Country != null ? Country.GetHashCode() : 0);
            }
        }
    }
}