﻿using System;
using System.Collections.Generic;

namespace BoilingPoint.Portable.Data.TweetEntities
{
    public class MediaEntity
    {
        public UInt64 ID { get; set; }

        public String MediaUrl { get; set; }

        public List<PhotoSize> Sizes { get; set; }

        public String Type { get; set; }
    }
}