﻿using System;
using Newtonsoft.Json;

namespace BoilingPoint.Portable.Data
{
    public class BaseEntity : PropertyChangedBase, IEntity
    {
        [JsonIgnore]
        public string objectId { get; set; }

        [JsonIgnore]
        public DateTime? updatedAt { get; set; }

        [JsonIgnore]
        public DateTime? createdAt { get; set; }
    }
}