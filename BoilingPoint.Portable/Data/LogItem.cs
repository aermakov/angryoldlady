﻿namespace BoilingPoint.Portable.Data
{
    public class LogItem : BaseEntity
    {
        public LogItem()
        {
        }

        public LogItem(string message)
        {
            Message = message;
        }

        public string Message { get; set; }
    }
}