﻿namespace BoilingPoint.Portable.Data
{
    public class Config : BaseEntity
    {
        public string AppBaseUrl { get; set; }
        public string DoubleGisApiKey { get; set; }
        public string TCSReferralUrl { get; set; }
    }
}