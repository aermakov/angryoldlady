﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using BoilingPoint.Portable.Extensions;

namespace BoilingPoint.Portable.Data
{
    [Flags]
    public enum StatusBuildSettings
    {
        Bank = 1,
        UserTags = 2,
        Users = 4,
        HateToWaitTag = 8,
        ServiceTags = 16,
        Comment = 32,
        All = Bank | UserTags | Users | HateToWaitTag | ServiceTags | Comment
    }

    public static class StatusBuilder
    {
        public static string Build(Tweet tweet,
                                   StatusBuildSettings buildSettings = StatusBuildSettings.All)
        {
            if (tweet.ID != 0ul && !string.IsNullOrEmpty(tweet.Text))
                return tweet.Text;

            var sb = new StringBuilder();
            if (buildSettings.HasFlag(StatusBuildSettings.Bank))
            {
                AppendBank(sb, tweet);
            }

            if (buildSettings.HasFlag(StatusBuildSettings.Users))
            {
                foreach (User user in tweet.Users)
                {
                    sb.AppendFormat("@{0} ", user.ScreenName);
                }
            }

            if (buildSettings.HasFlag(StatusBuildSettings.ServiceTags))
            {
                AppendServiceTags(sb, tweet);
            }

            if (buildSettings.HasFlag(StatusBuildSettings.UserTags))
            {
                foreach (string tag in tweet.Tags)
                {
                    sb.AppendFormat("#{0} ", tag);
                }
            }

            if (buildSettings.HasFlag(StatusBuildSettings.Comment))
            {
                if (!string.IsNullOrEmpty(tweet.Comment))
                {
                    sb.AppendFormat(tweet.Comment.Trim());
                }
            }

            if (buildSettings.HasFlag(StatusBuildSettings.HateToWaitTag))
            {
                sb.AppendFormat("{0} ", tweet.HateToWaitTag);
            }

            return sb.ToString();
        }

        public static string BuildPartial(Tweet tweet)
        {
            string text = Build(tweet,
                                StatusBuildSettings.Bank |
                                StatusBuildSettings.Comment |
                                StatusBuildSettings.Users |
                                StatusBuildSettings.UserTags);
            if (tweet.ID != 0ul)
            {
                var sb = new StringBuilder(text);
                Utils.ReplaceIgnoreCase(sb, tweet.HateToWaitTag, string.Empty);
                Utils.ReplaceIgnoreCase(sb, tweet.PeopleInlineTag, string.Empty);
                Utils.ReplaceIgnoreCase(sb, tweet.WaitTimeTag, string.Empty);
                Utils.ReplaceIgnoreCase(sb, tweet.WaitTimeTag, string.Empty);
                Utils.ReplaceIgnoreCase(sb, tweet.CashBoxAvailableTag, string.Empty);
                Utils.ReplaceIgnoreCase(sb, tweet.CashBoxClosedTag, string.Empty);

                var regex = new Regex(@"[ ]{2,}", RegexOptions.None);
                text = regex.Replace(sb.ToString(), @" ");
            }
            return text;
        }

        public static string FormatBank(Bank bank)
        {
            if (bank != null && !string.IsNullOrEmpty(bank.TwitterUsername))
                return string.Format("@{0}", bank.TwitterUsername);
            if (bank != null) return string.Format("#{0}", Utils.GetTitleCaseHashtag(bank.Title));
            return string.Empty;
        }

        private static void AppendBank(StringBuilder sb, Tweet tweet)
        {
            if (tweet.Bank == null) return;
            sb.AppendFormat("{0} ", FormatBank(tweet.Bank));
        }

        private static void AppendServiceTags(StringBuilder sb, Tweet tweet)
        {
            if (tweet.PeopleInline > 0)
            {
                sb.AppendFormat("{0} ", tweet.PeopleInlineTag);
            }

            if (tweet.WaitTime > 0)
            {
                sb.AppendFormat("{0} ", tweet.WaitTimeTag);
            }

            if (tweet.CashBoxAvailable > 0)
            {
                sb.AppendFormat("{0} ", tweet.CashBoxAvailableTag);
            }

            if (tweet.CashBoxClosed > 0)
            {
                sb.AppendFormat("{0} ", tweet.CashBoxClosedTag);
            }
        }
    }
}