﻿using System.Collections.Generic;
using BoilingPoint.Portable.Geo;

namespace BoilingPoint.Portable.Data
{
    public class TweetGrouping : List<Tweet>
    {
        private readonly Distance _radiusInKilometers;

        public TweetGrouping(double radiusInMeters)
        {
            _radiusInKilometers = Distance.ToKilometers(radiusInMeters);
        }

        public bool CanAdd(double latitude, double longitude)
        {
            if (Count == 0)
                return true;

            Tweet tweet = this[0];
            var geoPoint = new GeoPoint(tweet.Latitude, tweet.Longitude);
            Distance distance =
                geoPoint.DistanceBetween(new GeoPoint(latitude, longitude)).ToKilometers();
            return _radiusInKilometers >= distance;
        }

        public bool CanAdd(Tweet tweet)
        {
            return CanAdd(tweet.Latitude, tweet.Longitude);
        }
    }
}