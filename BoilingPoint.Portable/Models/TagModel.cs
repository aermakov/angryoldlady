﻿using BoilingPoint.Portable.Data;

namespace BoilingPoint.Portable.Models
{
    public class TagModel : PropertyChangedBase
    {
        public TagModel(Tag tag)
        {
            Name = string.Format("#{0}", tag.Name);
        }

        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}