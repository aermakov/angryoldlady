﻿using System;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.DoubleGis;

namespace BoilingPoint.Portable.Models
{
    public class FilialModel
    {
        #region Constructors

        public FilialModel(Filial filial)
        {
            Id = filial.Id;
            Name = filial.Name;
            Comment = filial.Comment;
            Rubrics = String.Join(", ", filial.Rubrics);
            Distance = filial.Distance;
            Latitude = filial.Latitude;
            Longitude = filial.Longitude;

            Address = String.Format("{0}, {1}", filial.CityName, filial.Address);
        }

        #endregion Constructors

        #region Properties

        public String Id { get; set; }

        public String Name { get; set; }

        public Int32 Distance { get; set; }

        public String DistanceFormatted
        {
            get
            {
                String val = null;

                if (Distance < Geo.Distance.MetersInKilometers)
                {
                    val = string.Format("{0} м.", Distance);
                }
                else
                {
                    val = string.Format("{0} км.", Geo.Distance.ToKilometers(Distance).Value);
                }

                return val;
            }
        }

        public String Address { get; set; }

        public String Comment { get; set; }

        public String Rubrics { get; set; }

        public Double Latitude { get; set; }

        public Double Longitude { get; set; }

        public GeoPoint Location
        {
            get { return new GeoPoint(Latitude, Longitude); }
        }

        #endregion Properties
    }
}