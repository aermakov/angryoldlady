﻿using System;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Geo.DoubleGis;

namespace BoilingPoint.Portable.Models
{
    public class ContactModel : PropertyChangedBase
    {
        public ContactModel(Contact contact)
        {
            Comment = contact.Comment;
            Value = contact.Alias ?? contact.Value;
            Type = (ContactType) Enum.Parse(typeof (ContactType), contact.Type, true);
            switch (Type)
            {
                case ContactType.Email:
                    TypeString = "e-mail";
                    break;
                case ContactType.Fax:
                    TypeString = "факс";
                    break;
                case ContactType.Icq:
                    TypeString = "icq";
                    break;
                case ContactType.Jabber:
                    TypeString = "jabber";
                    break;
                case ContactType.Phone:
                    TypeString = "телефон";
                    break;
                case ContactType.Skype:
                    TypeString = "skype";
                    break;
                case ContactType.WebSite:
                    TypeString = "сайт";
                    break;
            }
        }

        public ContactType Type { get; set; }

        public string TypeString { get; set; }

        public string Value { get; set; }

        public string Comment { get; set; }

        public bool CommentVisible
        {
            get { return Comment != null; }
        }
    }
}