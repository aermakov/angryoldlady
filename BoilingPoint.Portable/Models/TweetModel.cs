﻿using System;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Extensions;

namespace BoilingPoint.Portable.Models
{
    public class TweetModel : PropertyChangedBase
    {
        #region Events

        public event EventHandler ContextMenuOpening;

        public virtual void OnContextMenuOpening(object sender, EventArgs e)
        {
            if (ContextMenuOpening != null)
                ContextMenuOpening(sender, e);
        }

        public event EventHandler ShowOnMapTap;

        protected virtual void OnShowOnMapTapped(object sender, EventArgs e)
        {
            if (ShowOnMapTap != null)
                ShowOnMapTap(sender, e);
        }

        public event EventHandler ShowTweetsInFilialTap;

        protected virtual void OnShowTweetsInFilialTapped(object sender, EventArgs e)
        {
            if (ShowTweetsInFilialTap != null)
                ShowTweetsInFilialTap(sender, e);
        }

        public event EventHandler ShowTweetsInBankTap;

        protected virtual void OnShowTweetsInBankTapped(object sender, EventArgs e)
        {
            if (ShowTweetsInBankTap != null)
                ShowTweetsInBankTap(sender, e);
        }

        #endregion

        #region Fields

        private string _address;
        private string _bankTitle;
        private string _createdAtShortTimeago;
        private string _createdAtTimeago;
        private string _createdAtUtcFormatted;
        private string _profileImageUrl;

        #endregion

        #region Ctors

        public TweetModel()
        {
        }

        public TweetModel(Tweet tweet)
        {
            Initialize(tweet);
        }

        protected void Initialize(Tweet tweet)
        {
            ID = tweet.ID;
            ProfileImageUrl = tweet.ProfileImageUrl;
            UserName = tweet.UserName;
            UserScreenName = tweet.UserScreenName;
            Address = tweet.Address;
            Latitude = tweet.Latitude;
            Longitude = tweet.Longitude;

            CreatedAtUtc = Utils.UnixTimeStampToDateTime(tweet.CreatedAtTimestamp);
            CreatedAtLocal = CreatedAtUtc.ToLocalTime();

            UpdateCreatedAtTimeago();

            BankTitle = tweet.BankTitle;
            PeopleInline = tweet.PeopleInline;
            WaitTime = TimeSpan.FromMinutes(tweet.WaitTime);
            WaitTimeFormatted = WaitTime.ToString(@"hh\:mm");
            CashBoxAvailable = tweet.CashBoxAvailable;
            CashBoxClosed = tweet.CashBoxClosed;
            Status = StatusBuilder.BuildPartial(tweet);
            ProfileId = tweet.ProfileId;
            BankId = tweet.BankId;
        }

        public void UpdateCreatedAtTimeago()
        {
            CreatedAtTimeago = CreatedAtUtc.ToTimeago();
            CreatedAtShortTimeago = CreatedAtUtc.ToShortTimeago();

            UpdateCreatedAtUtcFormatted();
        }

        public void UpdateCreatedAtUtcFormatted()
        {
            if (DateTime.UtcNow.Month != CreatedAtUtc.Month)
            {
                CreatedAtUtcFormatted = CreatedAtUtc.ToString("d MMM");
                if (DateTime.Today.Year != CreatedAtLocal.Year)
                {
                    CreatedAtUtcFormatted = CreatedAtUtc.ToString("d MMM yy");
                }
            }
            else
            {
                CreatedAtUtcFormatted = CreatedAtShortTimeago;
            }
        }

        #endregion

        #region Properties

        public TimeSpan WaitTime { get; set; }
        public ulong ID { get; set; }

        public string ProfileImageUrl
        {
            get { return _profileImageUrl; }
            set
            {
                if (Equals(_profileImageUrl, value)) return;
                _profileImageUrl = value;
                NotifyOfPropertyChange();
            }
        }

        public string UserName { get; set; }
        public string UserScreenName { get; set; }
        public string Status { get; set; }

        public bool StatusVisible
        {
            get { return !string.IsNullOrEmpty(Status); }
        }

        public int PeopleInline { get; set; }
        public string WaitTimeFormatted { get; set; }
        public int CashBoxAvailable { get; set; }
        public int CashBoxClosed { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public string BankId { get; set; }

        public bool ShowTweetsInBankVisible
        {
            get { return BankId != null; }
        }

        public bool ShowTweetsInFilialVisible
        {
            get { return !Equals(Latitude, 0d) && !Equals(Longitude, 0d) && !string.IsNullOrEmpty(ProfileId); }
        }

        public bool ShowOnMapVisible
        {
            get { return !Equals(Latitude, 0d) && !Equals(Longitude, 0d); }
        }

        public string ProfileId { get; set; }

        public bool ServiceTagsAvailable
        {
            get { return PeopleInline > 0 || !Equals(WaitTime, TimeSpan.Zero) || CashBoxAvailable > 0 || CashBoxClosed > 0; }
        }

        public DateTime CreatedAtLocal { get; set; }
        public DateTime CreatedAtUtc { get; set; }

        public string CreatedAtUtcFormatted
        {
            get { return _createdAtUtcFormatted; }
            set
            {
                _createdAtUtcFormatted = value;
                NotifyOfPropertyChange();
            }
        }

        public string CreatedAtTimeago
        {
            get { return _createdAtTimeago; }
            set
            {
                _createdAtTimeago = value;
                NotifyOfPropertyChange();
            }
        }

        public string CreatedAtShortTimeago
        {
            get { return _createdAtShortTimeago; }
            set
            {
                _createdAtShortTimeago = value;
                NotifyOfPropertyChange();
            }
        }

        public string Address
        {
            get { return _address; }
            set
            {
                _address = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange("AddressVisible");
            }
        }

        public bool PeopleInlineVisible
        {
            get { return PeopleInline != 0; }
        }

        public bool WaitTimeVisible
        {
            get { return !Equals(WaitTime, TimeSpan.Zero); }
        }

        public bool CashBoxAvailableVisible
        {
            get { return CashBoxAvailable != 0; }
        }

        public bool CashBoxClosedVisible
        {
            get { return CashBoxClosed != 0; }
        }

        public bool AddressVisible
        {
            get { return !string.IsNullOrEmpty(Address); }
        }

        public string BankTitle
        {
            get { return _bankTitle; }
            set
            {
                _bankTitle = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange("BankTitleVisible");
            }
        }

        public bool BankTitleVisible
        {
            get { return !string.IsNullOrEmpty(BankTitle); }
        }

        #endregion

        #region Methods

        public void OnShowOnMapTapped()
        {
            OnShowOnMapTapped(this, EventArgs.Empty);
        }

        public void OnShowTweetsInFilialTapped()
        {
            OnShowTweetsInFilialTapped(this, EventArgs.Empty);
        }

        public void OnShowTweetsInBankTapped()
        {
            OnShowTweetsInBankTapped(this, EventArgs.Empty);
        }

        protected bool Equals(TweetModel other)
        {
            return ID == other.ID;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((TweetModel) obj);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode();
        }

        public override string ToString()
        {
            return Status;
        }

        #endregion
    }
}