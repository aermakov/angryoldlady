﻿using BoilingPoint.Portable.Data;

namespace BoilingPoint.Portable.Models
{
    public class BankModel : PropertyChangedBase
    {
        public BankModel(Bank bank)
        {
            Bank = bank;
            Title = bank.Title.Trim();
            TwitterUsername = string.Format("@{0}", (bank.TwitterUsername ?? string.Empty).Trim());
            TwitterUsernameVisible = !string.IsNullOrWhiteSpace(bank.TwitterUsername);
        }

        public Bank Bank { get; set; }
        public string TwitterUsername { get; set; }
        public bool TwitterUsernameVisible { get; set; }
        public string Title { get; set; }

        public override string ToString()
        {
            return Title;
        }
    }
}