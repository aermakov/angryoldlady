﻿using System;
using Newtonsoft.Json.Linq;

namespace BoilingPoint.Portable.Extensions
{
    public static class JTokenExtensions
    {
        public static T GetValueOrDefault<T>(this JToken token, string key = null)
        {
            try
            {
                return token != null ? token.Value<T>(key) : token.Value<T>();
            }
            catch (Exception)
            {
                return default(T);
            }
        }
    }
}