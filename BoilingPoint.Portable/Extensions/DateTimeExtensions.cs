﻿using System;

namespace BoilingPoint.Portable.Extensions
{
    public static class DateTimeExtensions
    {
        private const int SECOND = 1;
        private const int MINUTE = 60*SECOND;
        private const int HOUR = 60*MINUTE;
        private const int DAY = 24*HOUR;
        private const int MONTH = 30*DAY;

        public static string ToTimeago(this DateTime dateTime)
        {
            var ts = new TimeSpan(DateTime.UtcNow.Ticks - dateTime.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);
            if (delta < 1*MINUTE)
            {
                return ts.Seconds == 1 ? "секунду назад" : ts.Seconds + " секунд назад";
            }
            if (delta < 2*MINUTE)
            {
                return "минуту назад";
            }
            if (delta < 45*MINUTE)
            {
                return string.Format("{0} {1} назад", ts.Minutes,
                                     Utils.Plural(ts.Minutes, new[] {"минуту", "минуты", "минут"}));
            }
            if (delta < 90*MINUTE)
            {
                return "час назад";
            }
            if (delta < 24*HOUR)
            {
                return string.Format("{0} {1} назад", ts.Hours, Utils.Plural(ts.Hours, new[] {"час", "часа", "часов"}));
            }
            if (delta < 48*HOUR)
            {
                return "вчера";
            }
            if (delta < 30*DAY)
            {
                return string.Format("{0} {1} назад", ts.Days, Utils.Plural(ts.Days, new[] {"день", "дня", "дней"}));
            }
            if (delta < 12*MONTH)
            {
                int months = Convert.ToInt32(Math.Floor((double) ts.Days/30));
                if (months <= 1) return "месяц назад";
                return string.Format("{0} {1} назад", months, Utils.Plural(months, new[] {"месяц", "месяца", "месяцев"}));
            }

            int years = Convert.ToInt32(Math.Floor((double) ts.Days/365));
            if (years <= 1) return "год назад";
            return string.Format("{0} {1} назад", years, Utils.Plural(years, new[] {"год", "года", "лет"}));
        }

        public static string ToShortTimeago(this DateTime dateTime)
        {
            var ts = new TimeSpan(DateTime.UtcNow.Ticks - dateTime.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);
            if (delta < 1*MINUTE)
            {
                return ts.Seconds == 1 ? "1с" : ts.Seconds + "с";
            }
            if (delta < 2*MINUTE)
            {
                return "1м";
            }
            if (delta < 45*MINUTE)
            {
                return string.Format("{0}м", ts.Minutes);
            }
            if (delta < 90*MINUTE)
            {
                return "1ч";
            }
            if (delta < 24*HOUR)
            {
                return string.Format("{0}ч", ts.Hours);
            }
            if (delta < 48*HOUR)
            {
                return "1д";
            }
            if (delta < 30*DAY)
            {
                return string.Format("{0}д", ts.Days);
            }
            if (delta < 12*MONTH)
            {
                int months = Convert.ToInt32(Math.Floor((double) ts.Days/30));
                if (months <= 1) months = 1;
                return string.Format("{0}мес", months);
            }

            int years = Convert.ToInt32(Math.Floor((double) ts.Days/365));
            if (years <= 1) return "1г";
            return string.Format("{0}{1}", years, Utils.Plural(years, new[] {"г", "г", "л"}));
        }
    }
}