namespace BoilingPoint.Portable
{
    /// <summary>
    ///     An invokable action that should be executed during the
    ///     next processing iteration of the host application
    /// </summary>
    public interface IDeferredAction
    {
        /// <summary>
        ///     Invoke the encapsulated action
        /// </summary>
        void Invoke();
    }
}