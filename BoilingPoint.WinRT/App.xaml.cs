﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using BoilingPoint.Portable.Geo.Google;
using BoilingPoint.Portable.Geo.Yandex;
using BoilingPoint.Portable.Services;
using BoilingPoint.WinRT.Services;
using BoilingPoint.WinRT.ViewModels;
using BoilingPoint.WinRT.Views;
using BugSense;
using Caliburn.Micro;
using Parse;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using DataService = BoilingPoint.WinRT.Services.DataService;
using TwitterService = BoilingPoint.WinRT.Services.TwitterService;

// The Grid App template is documented at http://go.microsoft.com/fwlink/?LinkId=234226

namespace BoilingPoint.WinRT
{
    /// <summary>
    ///     Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App
    {
        #region Fields

        private WinRTContainer _container;

        #endregion

        /// <summary>
        ///     Initializes the singleton Application Object.  This is the first line of authored code
        ///     executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            InitializeComponent();

            Suspending += OnSuspending;
        }

        public static String GetResource(String name)
        {
            var resourceLoader = new ResourceLoader();

            return resourceLoader.GetString(name);
        }

        protected override void Configure()
        {
            _container = new WinRTContainer();
            _container.RegisterWinRTServices();

            _container.PerRequest<MainViewModel>();
            _container.PerRequest<ProfileViewModel>();
            _container.PerRequest<MapViewModel>();

            _container.RegisterSingleton(typeof (IGeocodeService), null, typeof (GeocodeService));
            _container.RegisterSingleton(typeof (IYandexGeocoder), null, typeof (YandexGeocoder));
            _container.RegisterSingleton(typeof (IGoogleGeocoder), null, typeof (GoogleGeocoder));
            _container.RegisterSingleton(typeof (ILogService), null, typeof (LogService));
            _container.RegisterSingleton(typeof (ITwitterService), null, typeof (TwitterService));
            _container.RegisterSingleton(typeof (IDataService), null, typeof (DataService));
            _container.RegisterSingleton(typeof (IGeolocationService), null, typeof (GeolocationService));
            _container.RegisterSingleton(typeof (INotificationService), null, typeof (NotificationService));
            _container.RegisterSingleton(typeof (IAnonymousTwitterService), null, typeof (AnonymousTwitterService));

            ParseClient.Initialize(Settings.PARSE_APPLICATION_ID, Settings.PARSE_WINDOWS_KEY);
            BugSenseHandler.Instance.Init(this, Settings.BUGSENSE_API_KEY);
        }

        protected override void OnUnhandledException(Object sender, UnhandledExceptionEventArgs e)
        {
            var logService = (ILogService) _container.GetInstance(typeof (ILogService), null);
            if (logService != null)
            {
                logService.Error(e.Exception);
            }

            if (Debugger.IsAttached)
            {
                Debugger.Break();
                e.Handled = true;
            }
            else
            {
                e.Handled = true;
            }

            base.OnUnhandledException(sender, e);
        }

        protected override Object GetInstance(Type service, String key)
        {
            return _container.GetInstance(service, key);
        }

        protected override IEnumerable<Object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(Object instance)
        {
            _container.BuildUp(instance);
        }

        protected override void PrepareViewFirst(Frame rootFrame)
        {
            _container.RegisterNavigationService(rootFrame);
        }

        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            if (Equals(args.TileId, "App"))
            {
                DisplayRootView<MainView>();
            }
        }
    }
}