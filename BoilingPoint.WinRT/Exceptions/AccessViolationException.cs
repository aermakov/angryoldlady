﻿using System;

namespace BoilingPoint.WinRT.Exceptions
{
    public class AccessViolationException : Exception
    {
        public AccessViolationException()
        {
        }

        public AccessViolationException(string message)
            : base(message)
        {
        }

        public AccessViolationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}