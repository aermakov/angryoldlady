﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace BoilingPoint.WinRT.Converters
{
    public class NullOrEmptyToVisibilityConverter : IValueConverter
    {
        public Object Convert(Object value, Type targetType, Object parameter, String language)
        {
            Boolean isVisible = false;

            if (value is String)
            {
                isVisible = !String.IsNullOrEmpty(value as String);
            }
            else
            {
                isVisible = value != null;
            }

            return isVisible ? Visibility.Visible : Visibility.Collapsed;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, String language)
        {
            Boolean isVisible = false;

            if (value is Visibility)
            {
                var visibility = (Visibility) value;
                isVisible = visibility == Visibility.Visible;
            }

            return isVisible ? 1 : 0;
        }
    }
}