﻿using System;
using System.Linq;
using BoilingPoint.Portable;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Extensions;
using Windows.UI.Xaml;
using Yandex.Positioning;

namespace BoilingPoint.WinRT.Models
{
    public class TweetPushPinModel : PushPinModel
    {
        #region Fields

        private string _address;
        private string _bankTitle;
        private string _createdAtTimeago;
        private Tweet _tweet;

        #endregion

        #region Ctors

        public TweetPushPinModel(TweetGrouping grouping)
        {
            Grouping = grouping;
            Visibility = Visibility.Visible;
            ContentVisibility = Visibility.Collapsed;

            Tweet = GetLastTweet();

            Initialize(Tweet);
        }

        #endregion

        #region Properties

        public TimeSpan WaitTime { get; set; }
        public ulong ID { get; set; }
        public string ProfileImageUrl { get; set; }
        public string UserName { get; set; }
        public string UserScreenName { get; set; }
        public string ShortText { get; set; }
        public int PeopleInline { get; set; }
        public string WaitTimeFormatted { get; set; }
        public int CashBoxAvailable { get; set; }
        public int CashBoxClosed { get; set; }
        public string BankId { get; set; }

        public bool BankVisible
        {
            get { return BankId != null; }
        }

        public string ProfileId { get; set; }

        public bool ServiceTagsAvailable
        {
            get
            {
                return PeopleInline > 0 ||
                       !Equals(WaitTime, TimeSpan.Zero) ||
                       CashBoxAvailable > 0 ||
                       CashBoxClosed > 0;
            }
        }

        public DateTime CreatedAtUtc { get; set; }
        public DateTime CreatedAtLocal { get; set; }

        public string CreatedAtTimeago
        {
            get { return _createdAtTimeago; }
            set
            {
                _createdAtTimeago = value;
                NotifyOfPropertyChange();
            }
        }

        public string Address
        {
            get { return _address; }
            set
            {
                _address = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange("AddressVisible");
            }
        }

        public bool PeopleInlineVisible
        {
            get { return PeopleInline != 0; }
        }

        public bool WaitTimeVisible
        {
            get { return !Equals(WaitTime, TimeSpan.Zero); }
        }

        public bool CashBoxAvailableVisible
        {
            get { return CashBoxAvailable != 0; }
        }

        public bool CashBoxClosedVisible
        {
            get { return CashBoxClosed != 0; }
        }

        public bool AddressVisible
        {
            get { return !string.IsNullOrEmpty(Address); }
        }

        public string BankTitle
        {
            get { return _bankTitle; }
            set
            {
                _bankTitle = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange("BankTitleVisible");
            }
        }

        public bool BankTitleVisible
        {
            get { return !string.IsNullOrEmpty(BankTitle); }
        }

        public Tweet Tweet
        {
            get { return _tweet; }
            set
            {
                if (Equals(_tweet, value)) return;
                _tweet = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange("Position");
                NotifyOfPropertyChange("Title");
            }
        }

        public override PushpinType Type
        {
            get { return PushpinType.Tweet; }
        }

        public override GeoCoordinate Location
        {
            get { return new GeoCoordinate(Tweet.Latitude, Tweet.Longitude); }
        }

        public override string Title
        {
            get { return StatusBuilder.BuildPartial(Tweet); }
        }

        public TweetGrouping Grouping { get; private set; }

        #endregion

        #region Methods

        protected void Initialize(Tweet tweet)
        {
            ID = tweet.ID;
            ProfileImageUrl = tweet.ProfileImageUrl;
            UserName = tweet.UserName;
            UserScreenName = tweet.UserScreenName;
            Address = tweet.Address;
            Location = new GeoCoordinate(tweet.Latitude, tweet.Longitude);

            CreatedAtUtc = Utils.UnixTimeStampToDateTime(tweet.CreatedAtTimestamp);
            CreatedAtLocal = CreatedAtUtc.ToLocalTime();
            CreatedAtTimeago = CreatedAtUtc.ToTimeago();

            BankTitle = tweet.BankTitle;
            PeopleInline = tweet.PeopleInline;
            WaitTime = TimeSpan.FromMinutes(tweet.WaitTime);
            WaitTimeFormatted = WaitTime.ToString(@"hh\:mm");
            CashBoxAvailable = tweet.CashBoxAvailable;
            CashBoxClosed = tweet.CashBoxClosed;
            ShortText = StatusBuilder.BuildPartial(tweet);
            ProfileId = tweet.ProfileId;
            BankId = tweet.BankId;
        }

        public void AddTweetToGrouping(Tweet tweet)
        {
            Grouping.Add(tweet);

            Tweet = GetLastTweet();

            Initialize(Tweet);
        }

        private Tweet GetLastTweet()
        {
            return Grouping.OrderBy(tweet => tweet.CreatedAt).Last();
        }

        #endregion
    }
}