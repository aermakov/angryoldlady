﻿using System;
using BoilingPoint.Portable.Geo.DoubleGis;
using Yandex.Maps;
using Yandex.Positioning;

namespace BoilingPoint.WinRT.Models
{
    public class ProfilePushpinModel : PushPinModel
    {
        #region Fields

        private readonly Profile _profile;
        private Uri _imageUri;

        #endregion

        #region Ctors

        public ProfilePushpinModel()
        {
        }

        public ProfilePushpinModel(Profile profile)
        {
            _profile = profile;

            ImageUri = profile.ImageUri;

            State = PushPinState.Collapsed;
        }

        #endregion

        #region Properties

        public string Address
        {
            get { return Profile.Address; }
        }

        public Uri ImageUri
        {
            get { return _imageUri; }
            set
            {
                if (Equals(_imageUri, value)) return;
                _imageUri = value;
                NotifyOfPropertyChange();
            }
        }

        public Profile Profile
        {
            get { return _profile; }
        }

        public string Name
        {
            get { return Profile.Name; }
        }

        public override GeoCoordinate Location
        {
            get { return new GeoCoordinate(Profile.Latitude, Profile.Longitude); }
        }

        public override PushpinType Type
        {
            get { return PushpinType.Profile; }
        }

        public string Id
        {
            get { return Profile.Id; }
        }

        #endregion
    }
}