﻿using BoilingPoint.Portable.Data;
using Windows.UI.Xaml;
using Yandex.Maps;
using Yandex.Positioning;

namespace BoilingPoint.WinRT.Models
{
    public abstract class PushPinModel : PropertyChangedBase
    {
        #region Fields

        protected Visibility _contentVisibility;
        protected GeoCoordinate _location;
        protected PushPinState _state;
        protected string _title;
        protected Visibility _visibility;
        protected int _zIndex;

        #endregion

        #region Ctors

        protected PushPinModel()
        {
            Visibility = Visibility.Visible;
            ContentVisibility = Visibility.Collapsed;
            State = PushPinState.Expanded;
        }

        #endregion

        #region Properties

        public abstract PushpinType Type { get; }

        public virtual GeoCoordinate Location
        {
            get { return _location; }
            set
            {
                if (Equals(_location, value)) return;
                _location = value;
                NotifyOfPropertyChange();
            }
        }

        public virtual string Title
        {
            get { return _title; }
            set
            {
                if (Equals(_title, value)) return;
                _title = value;
                NotifyOfPropertyChange();
            }
        }

        public Visibility Visibility
        {
            get { return _visibility; }
            set
            {
                if (Equals(_visibility, value)) return;
                _visibility = value;
                NotifyOfPropertyChange();
            }
        }

        public Visibility ContentVisibility
        {
            get { return _contentVisibility; }
            set
            {
                if (Equals(_contentVisibility, value)) return;
                _contentVisibility = value;
                NotifyOfPropertyChange();
            }
        }

        public PushPinState State
        {
            get { return _state; }
            set
            {
                if (Equals(_state, value)) return;
                _state = value;
                NotifyOfPropertyChange();
            }
        }

        public int ZIndex
        {
            get { return _zIndex; }
            set
            {
                if (_zIndex == value) return;
                _zIndex = value;
                NotifyOfPropertyChange();
            }
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            return Title;
        }

        #endregion
    }
}