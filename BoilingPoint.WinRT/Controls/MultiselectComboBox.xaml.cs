﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;

namespace BoilingPoint.WinRT.Controls
{
    public sealed partial class MultiselectComboBox : UserControl
    {
        public MultiselectComboBox()
        {
            InitializeComponent();

            ComboBox.ItemsSource = new[] {"one", "two", "three"};
        }

        public IList<Object> SelectedItems { get; private set; }
    }
}