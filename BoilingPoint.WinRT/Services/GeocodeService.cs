﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.Google;
using BoilingPoint.Portable.Geo.Yandex;
using BoilingPoint.Portable.Services;

namespace BoilingPoint.WinRT.Services
{
    public class GeocodeService : IGeocodeService
    {
        private readonly IGoogleGeocoder _googleGeocoder;
        private readonly IYandexGeocoder _yandexGeocoder;

        public GeocodeService(IGoogleGeocoder googleGeocoder, IYandexGeocoder yandexGeocoder)
        {
            _googleGeocoder = googleGeocoder;
            _yandexGeocoder = yandexGeocoder;
        }

        public Task<IList<GeoAddress>> GeocodeAsync(string address, string lang = "", string region = "")
        {
            throw new NotImplementedException();
        }

        public Task<IList<GeoAddress>> ReverseGeocodeAsync(GeoPoint geoPoint, string lang = "")
        {
            throw new NotImplementedException();
        }

        public Task<IList<GeoAddress>> ReverseGeocodeAsync(double latitude, double longitude, string lang = "")
        {
            throw new NotImplementedException();
        }

        public Task<string> GetCountryCodeAsync(double latitude, double longitude)
        {
            throw new NotImplementedException();
        }
    }
}