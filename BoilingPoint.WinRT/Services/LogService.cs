﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using BoilingPoint.Portable.Services;
using BugSense;
using Newtonsoft.Json;

namespace BoilingPoint.WinRT.Services
{
    public class LogService : ILogService
    {
        public void Debug(string format, params object[] args)
        {
            System.Diagnostics.Debug.WriteLine(format, args);
        }

        public void Error(Exception ex, Dictionary<string, string> logExtra = null)
        {
            BugSenseHandler.Instance.LogException(ex, logExtra);
        }

        public void Debug(object value)
        {
            if (value == null) return;
            var sb = new StringBuilder();
            using (var writer = new StringWriter(sb))
            {
                var serializer = new JsonSerializer();
                serializer.Serialize(writer, value);
                System.Diagnostics.Debug.WriteLine(sb.ToString());
            }
        }
    }
}