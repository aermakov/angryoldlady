﻿using System;
using System.Threading.Tasks;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Events;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Services;
using Windows.Devices.Geolocation;
using PositionChangedEventArgs = Windows.Devices.Geolocation.PositionChangedEventArgs;

namespace BoilingPoint.WinRT.Services
{
    public class GeolocationService : BaseGeolocationService
    {
        #region Fields

        private Geolocator _locator;

        private GeolocationStatus _status = GeolocationStatus.Disabled;

        #endregion Fields

        #region Constructors

        public GeolocationService()
        {
            Init();
        }

        #endregion Constructors

        #region Properties

        public override GeolocationStatus Status
        {
            get { return _status; }
        }

        public override bool Started
        {
            get { return true; }
        }

        #endregion Properties

        #region Methods

        public override async Task<GeoPoint> GetGeoPointAsync()
        {
            GeoPoint geoPoint = null;

            Geoposition geoposition = await _locator.GetGeopositionAsync();
            if (geoposition != null)
            {
                double latitude = geoposition.Coordinate.Latitude;
                double longitude = geoposition.Coordinate.Longitude;
                geoPoint = new GeoPoint(latitude, longitude);
            }
            return geoPoint;
        }

        public override void Start()
        {
        }

        public override void Stop()
        {
        }

        private void Init()
        {
            _locator = new Geolocator();

            _locator.PositionChanged += OnLocatorPositionChanged;
            _locator.StatusChanged += OnLocatorStatusChanged;
        }

        private void OnLocatorPositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            var point = new GeoPoint(args.Position.Coordinate.Latitude, args.Position.Coordinate.Longitude);

            var e = new Portable.Events.PositionChangedEventArgs
                        {
                            Point = point
                        };

            OnPositionChanged(sender, e);
        }

        private void OnLocatorStatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            _status =
                (GeolocationStatus) Enum.Parse(typeof (GeolocationStatus), args.Status.ToString());

            var e = new GeolocationStatusChangedEventArgs
                        {
                            Status = _status
                        };

            OnStatusChanged(sender, e);
        }

        #endregion Methods
    }
}