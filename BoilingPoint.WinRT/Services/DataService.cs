﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoilingPoint.Portable;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.DoubleGis;
using BoilingPoint.Portable.PhoneNumbers;
using BoilingPoint.Portable.Services;
using BoilingPoint.WinRT.Helpers;
using Parse;

namespace BoilingPoint.WinRT.Services
{
    public class DataService : Portable.Services.DataService
    {
        public DataService(IGeolocationService geolocationService)
            : base(geolocationService)
        {
        }

        public override async Task<List<Tag>> GetPredefinedTagsAsync()
        {
            return (await ParseObject.GetQuery("Tag").FindAsync()).Select(ParseHelper.GetObject<Tag>).ToList();
        }

        public override async Task<Bank> GetBankByIdAsync(string bankId)
        {
            var parseObject = new ParseObject("Bank") {ObjectId = bankId};
            await parseObject.FetchAsync();
            return ParseHelper.GetObject<Bank>(parseObject);
        }

        public override async Task<int> GetBanksCountAsync(string countryCode = null)
        {
            ParseQuery<ParseObject> query = ParseObject.GetQuery("Bank");
            if (countryCode != null)
            {
                countryCode = countryCode.ToLower();
                query = query.WhereEqualTo("CountryCode", countryCode);
            }

            return await query.CountAsync();
        }

        public override async Task<Config> GetConfigAsync()
        {
            Config config = null;
            var query = new ParseQuery<ParseObject>("Config");
            ParseObject parseObject = await query.WhereNotEqualTo("AppBaseUrl", null).FirstOrDefaultAsync();

            if (parseObject != null)
            {
                config = ParseHelper.GetObject<Config>(parseObject);
            }
            return config;
        }

        protected override async Task<Bank> GetBankByProfileAsync(Profile profile)
        {
            Bank bank = null;

            var queries = new List<ParseQuery<ParseObject>>();

            string webSite = profile.GetWebSite();
            if (webSite != null)
            {
                queries.Add(new ParseQuery<ParseObject>("Bank")
                                .WhereEqualTo("WebSite", Utils.NormalizeWebSite(webSite)));
            }

            IEnumerable<string> phones =
                profile.Contacts.Where(x => x.Type == "phone").Select(x => x.Alias ?? x.Value);
            foreach (string phone in phones)
            {
                ParseQuery<ParseObject> phoneQuery =
                    new ParseQuery<ParseObject>("Bank")
                        .WhereEqualTo("PhoneNumber", PhoneNumberUtil.Normalize(phone));
                queries.Add(phoneQuery);
            }

            ParseObject parseObject = await ParseQuery<ParseObject>.Or(queries).FirstOrDefaultAsync();
            if (parseObject != null)
            {
                bank = ParseHelper.GetObject<Bank>(parseObject);
            }

            return bank;
        }

        public override async Task<List<Bank>> GetBanksAsync(int page, int pageSize, string countryCode = null)
        {
            ParseQuery<ParseObject> query =
                ParseObject.GetQuery("Bank").OrderBy("Title").Skip(page*pageSize).Limit(pageSize);
            if (countryCode != null)
            {
                countryCode = countryCode.ToLower();
                query = query.WhereEqualTo("CountryCode", countryCode);
            }

            return (await query.FindAsync()).Select(ParseHelper.GetObject<Bank>).ToList();
        }

        public override async Task<List<Bank>> SearchBanksByTitleAsync(string title, string countryCode = null)
        {
            string lowercaseHashtagTitle = Utils.GetLowerCaseHashtag(title);
            ParseQuery<ParseObject> query =
                ParseObject.GetQuery("Bank").OrderBy("Title")
                           .Limit(1000)
                           .WhereContains("LowercaseHashtagTitle", lowercaseHashtagTitle);
            if (countryCode != null)
            {
                countryCode = countryCode.ToLower();
                query = query.WhereEqualTo("CountryCode", countryCode);
            }

            return (await query.FindAsync()).Select(ParseHelper.GetObject<Bank>).ToList();
        }

        public override async Task<List<Bank>> SearchBanksByHashtagTitleAsync(string hashtagTitle)
        {
            Guard.ArgumentIsNotNull(hashtagTitle, "hashtagTitle");

            string lowercaseHashtagTitle = Utils.GetLowerCaseHashtag(hashtagTitle);
            ParseQuery<ParseObject> query =
                ParseObject.GetQuery("Bank").WhereEqualTo("LowercaseHashtagTitle", lowercaseHashtagTitle);
            return (await query.FindAsync()).Select(ParseHelper.GetObject<Bank>).ToList();
        }

        public override async Task<List<Bank>> SearchBanksByTwitterUsernameAsync(string twitterUsername)
        {
            Guard.ArgumentIsNotNull(twitterUsername, "twitterUsername");

            twitterUsername = twitterUsername.ToLower().Trim();
            ParseQuery<ParseObject> query =
                ParseObject.GetQuery("Bank").WhereEqualTo("TwitterUsername", twitterUsername);
            return (await query.FindAsync()).Select(ParseHelper.GetObject<Bank>).ToList();
        }

        public override async Task<List<Tweet>>
            GetTweetsAsync(GeoPoint geoPoint = null, int radiusInMeters = 0, ulong sinceID = 0, ulong maxID = 0)
        {
            ParseQuery<ParseObject> query = ParseObject.GetQuery("Tweet");

            if (sinceID != 0)
            {
                query = query.WhereGreaterThan("ID", sinceID);
            }
            else if (maxID != 0)
            {
                query = query.WhereLessThan("ID", maxID);
            }

            if (geoPoint != null && radiusInMeters != 0)
            {
                var parseGeoPoint = new ParseGeoPoint(geoPoint.Latitude, geoPoint.Longitude);
                Distance distance = Distance.ToKilometers(radiusInMeters);
                ParseGeoDistance parseGeoDistance = ParseGeoDistance.FromKilometers(distance.Value);
                query = query.WhereWithinDistance("Location", parseGeoPoint, parseGeoDistance);
            }

            List<Tweet> tweets =
                (await query.OrderByDescending("CreatedAt").FindAsync()).Select(ParseHelper.GetObject<Tweet>).ToList();

            return tweets;
        }

        public override async Task<List<Tweet>> GetTweetsByBankIdAsync(string bankId)
        {
            ParseQuery<ParseObject> query = ParseObject.GetQuery("Tweet");

            return
                (await query.WhereEqualTo("BankId", bankId)
                            .OrderByDescending("CreatedAt")
                            .FindAsync()).Select(ParseHelper.GetObject<Tweet>).ToList();
        }

        public override async Task<List<Tweet>> GetTweetsByProfileIdAsync(string profileId)
        {
            ParseQuery<ParseObject> query = ParseObject.GetQuery("Tweet");

            return
                (await query.WhereEqualTo("ProfileId", profileId)
                            .OrderByDescending("CreatedAt")
                            .FindAsync()).Select(ParseHelper.GetObject<Tweet>).ToList();
        }
    }
}