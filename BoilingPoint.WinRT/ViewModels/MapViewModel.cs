﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.DoubleGis;
using BoilingPoint.Portable.Models;
using BoilingPoint.Portable.Services;
using BoilingPoint.WinRT.Models;
using Caliburn.Micro;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Yandex.Maps;
using Yandex.Positioning;

namespace BoilingPoint.WinRT.ViewModels
{
    public class MapViewModel : ViewModelBase
    {
        #region Fields

        private const int SEARCH_RADIUS_IN_METERS = 1000;
        private readonly IAnonymousTwitterService _anonymousTwitterService;

        private readonly IDataService _dataService;
        private readonly IGeolocationService _geolocationService;
        private readonly ITwitterService _twitterService;
        private DataTemplate _profilePushpinTemplate;
        private BindableCollection<PushPinModel> _pushPins;

        private MapLayer _pushPinsLayer;
        private DataTemplate _tweetPushpinTemplate;
        private BindableCollection<TweetModel> _tweets;

        #endregion Fields

        #region Constructors

        public MapViewModel(INavigationService navigationService,
                            IDataService dataService,
                            IGeolocationService geolocationService,
                            ITwitterService twitterService,
                            IAnonymousTwitterService anonymousTwitterService)
            : base(navigationService)
        {
            _dataService = dataService;
            _geolocationService = geolocationService;
            _twitterService = twitterService;
            _anonymousTwitterService = anonymousTwitterService;

            _tweets = new BindableCollection<TweetModel>();
            _pushPins = new BindableCollection<PushPinModel>();
        }

        #endregion Constructors

        #region Properties

        public UInt64 TweetID { get; set; }

        public String ProfileID { get; set; }

        public GeoPoint CurrentLocation { get; set; }

        public UInt64 SinceID
        {
            get
            {
                UInt64 sinceID = 0;
                TweetModel lastTweet = Tweets.FirstOrDefault();
                if (lastTweet != null)
                {
                    sinceID = lastTweet.ID;
                }

                return sinceID;
            }
        }

        public BindableCollection<TweetModel> Tweets
        {
            get { return _tweets; }
            set
            {
                if (Equals(_tweets, value))
                {
                    return;
                }

                _tweets = value;
                NotifyOfPropertyChange();
            }
        }

        public BindableCollection<PushPinModel> PushPins
        {
            get { return _pushPins; }
            set
            {
                if (Equals(_pushPins, value))
                {
                    return;
                }

                _pushPins = value;
                NotifyOfPropertyChange();
            }
        }

        public Map Map
        {
            get { return FindControl<Map>("MapControl"); }
        }

        #endregion Properties

        #region Methods

        protected override async void OnViewLoaded(Object view)
        {
            _tweetPushpinTemplate = (DataTemplate) Application.Current.Resources["TweetPushpinTemplate"];
            _profilePushpinTemplate = (DataTemplate) Application.Current.Resources["ProfilePushpinTemplate"];
            _pushPinsLayer = (MapLayer) Map.FindName("PushpinsLayer");

            if (CurrentLocation == null && _geolocationService.Status == GeolocationStatus.Ready)
            {
                CurrentLocation = await _geolocationService.GetGeoPointAsync();
            }

            LoadTweets();

            await LoadProfilesAsync();

            if (TweetID != 0)
            {
                JumpToTweet();
            }
            else if (!string.IsNullOrEmpty(ProfileID))
            {
                JumpToProfile();
            }
            else
            {
                if (CurrentLocation != null)
                {
                    JumpToPosition(CurrentLocation);
                }
            }
        }

        private void LoadTweets()
        {
            List<TweetGrouping> groupings =
                _twitterService.GroupTweetsByDistance(_anonymousTwitterService.Tweets);

            IEnumerable<TweetPushPinModel> pushpins =
                groupings.Select(grouping => new TweetPushPinModel(grouping)).ToList();

            foreach (TweetPushPinModel pushpinModel in pushpins)
            {
                AddPushpin(pushpinModel, _tweetPushpinTemplate);
            }

            PushPins.AddRange(pushpins.Cast<PushPinModel>());
        }

        private async Task LoadProfilesAsync()
        {
            if (_geolocationService.Status == GeolocationStatus.Ready)
            {
                GeoPoint geoPoint = await _geolocationService.GetGeoPointAsync();
                double latitude = geoPoint.Latitude;
                double longitude = geoPoint.Longitude;

                var profilePushpins = new List<ProfilePushpinModel>();
                List<Profile> profiles =
                    await _dataService.SearchProfilesAsync(latitude, longitude, SEARCH_RADIUS_IN_METERS);
                foreach (
                    ProfilePushpinModel pushpinModel in profiles.Select(profile => new ProfilePushpinModel(profile)))
                {
                    AddPushpin(pushpinModel, _profilePushpinTemplate);

                    profilePushpins.Add(pushpinModel);
                    PushPins.Add(pushpinModel);
                }

                List<Bank> banks = await _dataService.SearchBanksAsync(profiles);

                List<Profile> profilesWithImages = await GetProfilesWithImagesAsync(banks);
                foreach (Profile profile in profilesWithImages)
                {
                    ProfilePushpinModel pushpinModel =
                        profilePushpins.First(x => x.Id == profile.Id);
                    if (pushpinModel != null)
                    {
                        pushpinModel.ImageUri = profile.ImageUri;
                    }
                }
            }
        }

        private async Task<List<Profile>> GetProfilesWithImagesAsync(List<Bank> banks)
        {
            var profilesWithImages = new List<Profile>();
            var userScreenNames = new List<string>();
            if (banks != null)
            {
                foreach (Bank bank in banks.Where(x => !string.IsNullOrEmpty(x.TwitterUsername) &&
                                                       !userScreenNames.Contains(x.TwitterUsername)))
                {
                    userScreenNames.Add(bank.TwitterUsername);
                }

                List<User> users = await _anonymousTwitterService.GetUsersAsync(string.Join(",", userScreenNames));

                foreach (User user in users)
                {
                    string fileName = Path.GetFileName(user.ProfileImageUrl);
                    if (fileName == null || fileName.StartsWith("default_profile")) continue;
                    Bank bank = banks.First(x => Equals(x.TwitterUsername, user.ScreenName));
                    Profile profile = bank.Profile;
                    profile.ImageUri = new Uri(user.ProfileImageUrl);
                    profilesWithImages.Add(profile);
                }
            }

            return profilesWithImages;
        }

        private void AddPushpin(PushPinModel pushpinModel, DataTemplate template)
        {
            var pushPin =
                new PushPin
                    {
                        DataContext = pushpinModel,
                        ContentTemplate = template,
                        State = pushpinModel.State,
                        ContentVisibility = pushpinModel.ContentVisibility,
                        Visibility = pushpinModel.Visibility
                    };

            MapLayer.SetLocation(pushPin, pushpinModel.Location);
            MapLayer.SetAlignment(pushPin, Alignment.BottomCenter);

            Canvas.SetZIndex(pushPin, pushpinModel.ZIndex);

            _pushPinsLayer.Children.Add(pushPin);
        }

        private void JumpToProfile()
        {
            if (PushPins != null)
            {
                foreach (PushPinModel pushpinModel in PushPins)
                {
                    if (!(pushpinModel is ProfilePushpinModel)) continue;
                    Profile profile = ((ProfilePushpinModel) pushpinModel).Profile;
                    if (profile.Id != ProfileID) continue;
                    JumpToPosition(profile.Latitude, profile.Longitude);
                    pushpinModel.Visibility = Visibility.Visible;
                    break;
                }
            }
        }

        private void JumpToTweet()
        {
            if (PushPins != null)
            {
                foreach (PushPinModel pushpinModel in PushPins)
                {
                    if (!(pushpinModel is TweetPushPinModel)) continue;
                    Tweet tweet = ((TweetPushPinModel) pushpinModel).Grouping.FirstOrDefault(t => t.ID == TweetID);
                    if (tweet == null) continue;
                    JumpToPosition(tweet.Latitude, tweet.Longitude);
                    pushpinModel.Visibility = Visibility.Visible;
                    break;
                }
            }
        }

        private void JumpToPosition(GeoPoint geoPoint)
        {
            if (geoPoint != null)
            {
                Map.Center = new GeoCoordinate(geoPoint.Latitude, geoPoint.Longitude);
            }
        }

        private void JumpToPosition(Double latitude, Double longitude)
        {
            Map.Center = new GeoCoordinate(latitude, longitude);
        }

        private void MapZoomIn()
        {
            Map.ZoomIn();
        }

        private void MapZoomOut()
        {
            Map.ZoomOut();
        }

        private async void MapFindMe()
        {
            GeoPositionStatus status = Map.JumpToCurrentLocation();
            switch (status)
            {
                case GeoPositionStatus.Disabled:
                    var alert = new MessageDialog(App.GetResource("LocationIsDisabled"));
                    await alert.ShowAsync();
                    break;
            }
        }

        private void CollapseAllPushPins()
        {
            Map.PushPinManager.CollapseAllPushpins();
        }

        private void GoBack()
        {
            if (_navigationService.CanGoBack)
            {
                _navigationService.GoBack();
            }
        }

        #endregion Methods
    }
}