﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Enums;
using BoilingPoint.Portable.Extensions;
using BoilingPoint.Portable.Geo;
using BoilingPoint.Portable.Geo.DoubleGis;
using BoilingPoint.Portable.Models;
using BoilingPoint.Portable.Services;
using BoilingPoint.WinRT.Models;
using Caliburn.Micro;
using Telerik.UI.Xaml.Controls.Input;
using Telerik.UI.Xaml.Controls.Input.AutoCompleteBox;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace BoilingPoint.WinRT.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        #region Fields

        private const String QUERY = "#ihatetowait";
        private const Int32 SEARCH_RADIUS_IN_METERS = 2000;
        private readonly IAnonymousTwitterService _anonymousTwitterService;

        private readonly IDataService _dataService;
        private readonly DateTime _defaultDateTime = new DateTime(2000, 1, 1, 0, 0, 0);
        private readonly IGeocodeService _geocodeService;
        private readonly IGeolocationService _geolocationService;
        private readonly DispatcherTimer _timer;
        private readonly ITwitterService _twitterService;

        private String _bank = String.Empty;
        private Int32 _cashBoxAvailable;
        private Int32 _cashBoxClosed;
        private String _countryCode;
        private GeoPoint _currentGeoPoint;

        private Boolean _isBanksLoading;
        private Boolean _isFilialsLoading;

        private Boolean _isLocationEnabled = false;
        private Boolean _isTweetsLoading;
        private String _message;
        private Boolean _panelVisible;
        private Int32 _peopleInline;
        private BindableCollection<TweetPushPinModel> _pins;
        private List<String> _selectedTags;
        private BindableCollection<TagModel> _tags;
        private BindableCollection<TweetModel> _tweetModels;
        private DateTime _waitTime = DateTime.MinValue;
        private WebServiceTextSearchProvider _webSearchProvider;
        private BindableCollection<FilialModel> _filials { get; set; }

        #endregion Fields

        #region Constructors

        public MainViewModel(INavigationService navigationService,
                             ITwitterService twitterService,
                             IAnonymousTwitterService anonymousTwitterService,
                             IGeolocationService geolocationService,
                             IGeocodeService geocodeService,
                             IDataService dataService)
            : base(navigationService)
        {
            _twitterService = twitterService;
            _anonymousTwitterService = anonymousTwitterService;
            _geolocationService = geolocationService;
            _geocodeService = geocodeService;
            _dataService = dataService;

            _tweetModels = new BindableCollection<TweetModel>();
            _pins = new BindableCollection<TweetPushPinModel>();
            _tags = new BindableCollection<TagModel>();
            _filials = new BindableCollection<FilialModel>();

            _selectedTags = new List<String>();

            _timer = new DispatcherTimer();
        }

        #endregion Constructors

        #region Properties

        public UInt64 SinceID
        {
            get
            {
                UInt64 sinceID = 0;
                TweetModel lastTweet = Tweets.FirstOrDefault();
                if (lastTweet != null)
                {
                    sinceID = lastTweet.ID;
                }

                return sinceID;
            }
        }

        public BindableCollection<TweetPushPinModel> Pins
        {
            get { return _pins; }
            set
            {
                if (Equals(value, _pins))
                {
                    return;
                }

                _pins = value;
                NotifyOfPropertyChange();
            }
        }

        public BindableCollection<TweetModel> Tweets
        {
            get { return _tweetModels; }
            set
            {
                if (Equals(value, _tweetModels))
                {
                    return;
                }

                _tweetModels = value;
                NotifyOfPropertyChange();
            }
        }

        public BindableCollection<FilialModel> Filials
        {
            get { return _filials; }
            set
            {
                if (Equals(_filials, value))
                {
                    return;
                }

                _filials = value;
                NotifyOfPropertyChange();
            }
        }

        public BindableCollection<TagModel> Tags
        {
            get { return _tags; }
            set
            {
                if (Equals(value, _tags))
                {
                    return;
                }

                _tags = value;
                NotifyOfPropertyChange();
            }
        }

        public Boolean IsBanksLoading
        {
            get { return _isBanksLoading; }
            set
            {
                _isBanksLoading = value;
                NotifyOfPropertyChange();
            }
        }

        public Boolean IsTweetsLoading
        {
            get { return _isTweetsLoading; }
            set
            {
                _isTweetsLoading = value;
                NotifyOfPropertyChange();
            }
        }

        public Boolean IsFilialsLoading
        {
            get { return _isFilialsLoading; }
            set
            {
                _isFilialsLoading = value;
                NotifyOfPropertyChange();
            }
        }

        public String Bank
        {
            get { return _bank; }
            set
            {
                _bank = value;
                NotifyOfPropertyChange(() => Bank);
            }
        }

        public Int32 PeopleInline
        {
            get { return _peopleInline; }
            set
            {
                _peopleInline = value;
                NotifyOfPropertyChange(() => PeopleInline);
            }
        }

        public DateTime WaitTime
        {
            get { return _waitTime; }
            set
            {
                _waitTime = value;
                NotifyOfPropertyChange(() => WaitTime);
            }
        }

        public Int32 CashBoxAvailable
        {
            get { return _cashBoxAvailable; }
            set
            {
                _cashBoxAvailable = value;
                NotifyOfPropertyChange(() => CashBoxAvailable);
            }
        }

        public Int32 CashBoxClosed
        {
            get { return _cashBoxClosed; }
            set
            {
                _cashBoxClosed = value;
                NotifyOfPropertyChange(() => CashBoxClosed);
            }
        }

        public String Message
        {
            get { return _message; }
            set
            {
                _message = value;
                NotifyOfPropertyChange(() => Message);
            }
        }

        public Boolean PanelVisible
        {
            get { return _panelVisible; }
            set
            {
                _panelVisible = value;
                NotifyOfPropertyChange(() => PanelVisible);
                NotifyOfPropertyChange(() => ShowPanelButtonVisible);
            }
        }

        public Boolean ShowPanelButtonVisible
        {
            get { return !_panelVisible; }
        }

        public List<String> SelectedTags
        {
            get
            {
                if (_selectedTags == null)
                    _selectedTags = new List<String>();
                return _selectedTags;
            }
            set
            {
                if (Equals(_selectedTags, value))
                {
                    return;
                }

                _selectedTags = value;
                NotifyOfPropertyChange();
                NotifyOfPropertyChange("SelectedTagsString");
            }
        }

        public String SelectedTagsString
        {
            get { return String.Join(" ", SelectedTags); }
        }

        public RadAutoCompleteBox BanksAutocompleteBox
        {
            get { return FindControl<RadAutoCompleteBox>("Banks"); }
        }

        public RadTimePicker WaitTimePicker
        {
            get { return FindControl<RadTimePicker>("WaitTimeControl"); }
        }

        public VisualState ShowPanelState
        {
            get { return FindControl<VisualState>("ShowPanelAnimation"); }
        }

        public VisualState HidePanelState
        {
            get { return FindControl<VisualState>("HidePanelAnimation"); }
        }

        #endregion Properties

        #region Methods

        protected override async void OnViewLoaded(Object view)
        {
            if (_geolocationService.Status == GeolocationStatus.Ready)
            {
                _currentGeoPoint = await _geolocationService.GetGeoPointAsync();
            }

            await InitializeDoubleGisAsync();

            await LoadTweetsAsync();

            await InitializeControlsAsync();

            await LoadFilialsAsync();

            _timer.Interval = TimeSpan.FromSeconds(60);
            _timer.Tick += OnTimerTick;
            _timer.Start();
        }

        private async Task InitializeDoubleGisAsync()
        {
            Config config = await _dataService.GetConfigAsync();

            DoubleGis.Initialize(config.DoubleGisApiKey);
        }

        protected async Task SendTweet()
        {
            if (!_twitterService.IsAuthorized)
            {
                await _twitterService.AuthorizeAsync();
            }

            Tweet tweet = GetTweet();

            GeoPoint geoPoint = await _geolocationService.GetGeoPointAsync();
            if (geoPoint != null)
            {
                tweet.Latitude = geoPoint.Latitude;
                tweet.Longitude = geoPoint.Longitude;
            }

            await _twitterService.SendTweetAsync(tweet);

            ClearNewTweetControls();

            HidePanel();

            await LoadTweetsAsync(SinceID);
        }

        private async Task InitializeControlsAsync()
        {
            WaitTime = _defaultDateTime;

            _webSearchProvider = new WebServiceTextSearchProvider();
            _webSearchProvider.InputChanged += OnSearchProviderInputChanged;

            BanksAutocompleteBox.InitializeSuggestionsProvider(_webSearchProvider);

            List<Tag> tags = await _dataService.GetPredefinedTagsAsync();
            Tags.AddRange(tags.Select(q => new TagModel(q)));

            var cbxTags = FindControl<ComboBox>("TagsComboBox");
            cbxTags.ItemsSource = new[] {Tags};
        }

        private async void OnSearchProviderInputChanged(Object sender, EventArgs e)
        {
            IsBanksLoading = true;

            var provider = sender as WebServiceTextSearchProvider;
            if (provider == null)
            {
                return;
            }

            String inputString = provider.InputString;

            String countryCode = await GetCountryCodeAsync();
            if (!String.IsNullOrEmpty(countryCode))
            {
                List<Bank> banks = await _dataService.SearchBanksByTitleAsync(inputString, countryCode);
                provider.LoadItems(banks);
            }

            IsBanksLoading = false;
        }

        private async Task LoadTweetsAsync(UInt64 sinceID = 0)
        {
            IsTweetsLoading = true;

            IEnumerable<Tweet> tweets = await _anonymousTwitterService.SearchAsync(QUERY, sinceID);

            IEnumerable<Tweet> enumerable = tweets.ToArray();
            IEnumerable<TweetModel> tweetModels = enumerable.Select(q => new TweetModel(q))
                                                            .OrderBy(q => q.CreatedAtUtc);

            foreach (TweetModel tweetModel in tweetModels)
            {
                Tweets.Insert(0, tweetModel);
            }

            IsTweetsLoading = false;

            InitializeMapPins(enumerable);
        }

        private async Task LoadFilialsAsync(Int32 radiusInMeters = SEARCH_RADIUS_IN_METERS)
        {
            IsFilialsLoading = true;

            GeoPoint geoPoint = _currentGeoPoint;

            if (geoPoint != null)
            {
                List<Filial> filials =
                    (await _dataService.SearchFilialsAsync(geoPoint.Latitude, geoPoint.Longitude, radiusInMeters))
                        .OrderBy(q => q.Distance)
                        .ToList();

                String countryCode = await GetCountryCodeAsync();
                if (!String.IsNullOrEmpty(countryCode))
                {
                    List<Bank> banks = await _dataService.GetBanksAsync(countryCode);
                }

                Filials.AddRange(filials.Select(q => new FilialModel(q)));
            }

            IsFilialsLoading = false;
        }

        private void InitializeMapPins(IEnumerable<Tweet> tweets)
        {
            foreach (Tweet tweet in tweets)
            {
                TweetPushPinModel pushPin = Pins.FirstOrDefault(q => q.Grouping.CanAdd(tweet));
                if (pushPin != null)
                {
                    pushPin.AddTweetToGrouping(tweet);
                }
                else
                {
                    var grouping = new TweetGrouping(DataService.DEFAULT_RADIUS_IN_METERS) {tweet};
                    pushPin = new TweetPushPinModel(grouping);
                    Pins.Add(pushPin);
                }
            }
        }

        private Tweet GetTweet()
        {
            var tweet = new Tweet
                            {
                                Bank = new Bank {Title = Bank},
                                PeopleInline = PeopleInline,
                                WaitTime = (WaitTime - _defaultDateTime).TotalMinutes,
                                CashBoxAvailable = CashBoxAvailable,
                                CashBoxClosed = CashBoxClosed,
                                Comment = Message,
                                Tags = SelectedTags.Select(q => q.Substring(1)).ToList()
                            };

            return tweet;
        }

        private async Task<String> GetCountryCodeAsync()
        {
            if (_countryCode == null)
            {
                String countryCode = null;

                if (_currentGeoPoint != null)
                {
                    countryCode = await _geocodeService.GetCountryCodeAsync(
                        _currentGeoPoint.Latitude, _currentGeoPoint.Longitude);
                }

                _countryCode = countryCode;
            }

            return _countryCode;
        }

        private void ClearNewTweetControls()
        {
            Bank = String.Empty;
            PeopleInline = 0;
            WaitTime = _defaultDateTime;
            CashBoxAvailable = 0;
            CashBoxClosed = 0;
            Message = String.Empty;
        }

        private async void OnTimerTick(Object sender, Object e)
        {
            foreach (TweetModel tweetModel in Tweets)
            {
                tweetModel.CreatedAtTimeago = tweetModel.CreatedAtUtc.ToTimeago();
            }

            await LoadTweetsAsync(SinceID);
        }

        private void ShowPanel()
        {
            ShowPanelState.Storyboard.Begin();
            PanelVisible = true;
        }

        private void HidePanel()
        {
            HidePanelState.Storyboard.Begin();
            PanelVisible = false;
        }

        public void ShowHidePanel()
        {
            if (PanelVisible)
            {
                HidePanel();
            }
            else
            {
                ShowPanel();
            }
        }

        public void CancelTweet()
        {
            HidePanel();
            ClearNewTweetControls();
        }

        public void OnSelectTagsTapped(ComboBox tagsComboBox, ListBox tagsListBox)
        {
            SelectedTags = tagsListBox.SelectedItems.Cast<TagModel>()
                                      .Select(q => q.Name).ToList();

            tagsComboBox.IsDropDownOpen = false;
        }

        public void OnCancelTagsTapped(ComboBox tagsComboBox)
        {
            tagsComboBox.IsDropDownOpen = false;
        }

        public async void OnFilialTapped(FrameworkElement sender, FilialModel filial)
        {
            _navigationService.UriFor<ProfileViewModel>()
                              .WithParam(q => q.ProfileId, filial.Id)
                              .Navigate();
        }

        public void GoToMap()
        {
            _navigationService.UriFor<MapViewModel>()
                              .WithParam(q => q.CurrentLocation, _currentGeoPoint)
                              .Navigate();
        }

        #endregion Methods
    }
}