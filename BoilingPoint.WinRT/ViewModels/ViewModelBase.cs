﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using Windows.UI.Xaml;

namespace BoilingPoint.WinRT.ViewModels
{
    public abstract class ViewModelBase : Screen
    {
        protected readonly Dictionary<String, Object> _controlsCache = new Dictionary<String, Object>();
        protected readonly INavigationService _navigationService;

        protected ViewModelBase(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public Boolean CanGoBack
        {
            get { return _navigationService.CanGoBack; }
        }

        public void GoBack()
        {
            _navigationService.GoBack();
        }

        public T FindControl<T>(String name)
        {
            Object control = null;

            if (!_controlsCache.TryGetValue(name, out control))
            {
                control = ((FrameworkElement) GetView()).FindName(name);
                _controlsCache.Add(name, control);
            }

            return (T) control;
        }
    }
}