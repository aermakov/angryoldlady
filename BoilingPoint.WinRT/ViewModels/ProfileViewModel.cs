﻿using System;
using System.Collections.Generic;
using System.Linq;
using BoilingPoint.Portable.Data;
using BoilingPoint.Portable.Geo.DoubleGis;
using BoilingPoint.Portable.Models;
using BoilingPoint.Portable.Services;
using Caliburn.Micro;

namespace BoilingPoint.WinRT.ViewModels
{
    /// <summary>
    ///     Profile view model.
    /// </summary>
    public class ProfileViewModel : ViewModelBase
    {
        #region Fields

        private readonly IAnonymousTwitterService _anonymousTwitterService;
        private readonly IDataService _dataService;
        private BindableCollection<ContactModel> _contacts;

        private FilialModel _filial;
        private ProfileModel _profile;
        private BindableCollection<TweetModel> _tweets;

        #endregion Fields

        #region Constructors

        public ProfileViewModel(INavigationService navigationService,
                                IDataService dataService,
                                IAnonymousTwitterService anonymousTwitterService)
            : base(navigationService)
        {
            _dataService = dataService;
            _anonymousTwitterService = anonymousTwitterService;

            _tweets = new BindableCollection<TweetModel>();
            _contacts = new BindableCollection<ContactModel>();
        }

        #endregion Constructors

        #region Properties

        public String ProfileId { get; set; }

        public ProfileModel Profile
        {
            get { return _profile; }
            set
            {
                if (Equals(_profile, value))
                {
                    return;
                }

                _profile = value;
                NotifyOfPropertyChange();
            }
        }

        public FilialModel Filial
        {
            get { return _filial; }
            set
            {
                if (Equals(_filial, value))
                {
                    return;
                }

                _filial = value;
                NotifyOfPropertyChange();
            }
        }

        public BindableCollection<TweetModel> Tweets
        {
            get { return _tweets; }
            set
            {
                if (Equals(_tweets, value))
                {
                    return;
                }

                _tweets = value;
                NotifyOfPropertyChange();
            }
        }

        public BindableCollection<ContactModel> Contacts
        {
            get { return _contacts; }
            set
            {
                if (Equals(_contacts, value))
                {
                    return;
                }

                _contacts = value;
                NotifyOfPropertyChange();
            }
        }

        #endregion Properties

        #region Methods

        protected override async void OnViewLoaded(Object view)
        {
            if (!String.IsNullOrEmpty(ProfileId))
            {
                Profile profile = await _dataService.GetProfileByIdAsync(ProfileId);
                if (profile != null)
                {
                    Profile = new ProfileModel(profile);

                    Contacts.AddRange(profile.Contacts.Select(q => new ContactModel(q)));
                }

                List<Tweet> tweets = await _dataService.GetTweetsByProfileIdAsync(ProfileId);

                Tweets.AddRange(tweets.Select(q => new TweetModel(q)));
            }
        }

        private void GoBack()
        {
            if (_navigationService.CanGoBack)
            {
                _navigationService.GoBack();
            }
        }

        #endregion Methods
    }
}